## Orbital Taxi Publisher
This is a simple command line application
which extracts the Taxi schemas that are built into Orbital
source code, and publishes them as a taxi project.

It's intended as an internal tool within Orbital CI/CD pipelines
so that Orbital taxi code can be used in standalone tooling, like VS Code

## Usage
