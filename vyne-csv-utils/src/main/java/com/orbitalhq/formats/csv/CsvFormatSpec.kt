package com.orbitalhq.formats.csv

import com.google.common.net.MediaType
import com.orbitalhq.VyneTypes
import com.orbitalhq.annotations.AnnotationWrapper
import com.orbitalhq.models.format.ModelFormatSpec
import com.orbitalhq.schemas.Metadata
import com.orbitalhq.schemas.QualifiedName
import com.orbitalhq.schemas.fqn
import lang.taxi.TaxiDocument
import lang.taxi.types.Annotation
import java.io.Serializable

object CsvAnnotationSpec {
   val NAME = "${VyneTypes.NAMESPACE}.formats.Csv".fqn()
   val taxi = """
      namespace ${NAME.namespace} {

         annotation ${NAME.name} {
            delimiter : String = ","
            firstRecordAsHeader : Boolean = true
            nullValue : String?
            containsTrailingDelimiters : Boolean = false
            useFieldNamesAsColumnNames: Boolean = true
            quoteChar: String = '"'
            recordSeparator: String = "\r\n"
         }

      }
   """.trimIndent()
}

class CsvFormatSpecAnnotation(
   val delimiter: Char = ',',
   val firstRecordAsHeader: Boolean = true,
   val nullValue: String? = null,
   val containsTrailingDelimiters: Boolean = false,
   val ignoreContentBefore: String? = null,
   /**
    * Indicates if the field names should be used for column names.
    *
    * Historically, only fields with "by column(..) accessors defined were used for column names.
    * As part of a migration away from 'by column' towards formats, this behaviour has been rationalized.
    *
    * Now, fields are serialized using the fieldname as the column header.
    *
    * To re-enable the legacy behaviour, and only serialize where a `by column` definition exists,
    * set this to true.
    *
    * This behaviour is considered deprecated, and will be removed in the future.
    */
   val useFieldNamesAsColumnNames: Boolean = true,
   val quoteChar: Char? = '"',
   val recordSeparator: String = "\r\n"
) : Serializable, AnnotationWrapper {
   override fun asAnnotation(schema: TaxiDocument): Annotation {
      return Annotation(
         CsvAnnotationSpec.NAME.parameterizedName,
         parameters = mapOf(
            nullIfDefault(CsvFormatSpecAnnotation::delimiter.name to delimiter, ','),
            nullIfDefault(CsvFormatSpecAnnotation::firstRecordAsHeader.name to firstRecordAsHeader, true),
            CsvFormatSpecAnnotation::nullValue.name to nullValue,
            nullIfDefault(CsvFormatSpecAnnotation::containsTrailingDelimiters.name to containsTrailingDelimiters, false),
            CsvFormatSpecAnnotation::ignoreContentBefore.name to ignoreContentBefore,
            nullIfDefault(CsvFormatSpecAnnotation::quoteChar.name to quoteChar, '"'),
            nullIfDefault(CsvFormatSpecAnnotation::recordSeparator.name to recordSeparator, recordSeparator),
      ).filter { (_,v) -> v != null })
   }

   private fun <K,V> nullIfDefault(pair: Pair<K,V>, default: V): Pair<K, V?> {
      return if (pair.second == default) {
         pair.first to null
      } else pair
   }

   val ingestionParameters: CsvIngestionParameters = CsvIngestionParameters(
      delimiter,
      firstRecordAsHeader,
      nullValue = nullValue?.let { setOf(it) } ?: emptySet(),
      containsTrailingDelimiters = containsTrailingDelimiters,
      preludeText = ignoreContentBefore,
      quote = quoteChar,
      recordSeparator = recordSeparator
   )


   companion object {
      private val SEPERATOR_REPLACEMENTS = listOf(
         "\\r" to "\r",
         "\\n" to "\n",
         "\\t" to "\t"
      )

      fun from(metadata: Metadata): CsvFormatSpecAnnotation {
         require(metadata.name == CsvAnnotationSpec.NAME) { "Cannot parse ${CsvAnnotationSpec.NAME} from an annotation of type ${metadata.name}" }
         val delimiter = (metadata.params["delimiter"] as String?)?.let { delimiterStr ->
            if (delimiterStr == """\t""") '\t' else delimiterStr.toCharArray()[0]
         } ?: ','

         val withQuote = when (val withQuoteStr = metadata.params["quoteChar"] as String?) {
            null -> '"'
            "null" -> null
            else -> withQuoteStr[0]
         }

         val firstRecordAsHeader: Boolean = metadata.params["firstRecordAsHeader"] as Boolean? ?: true
         val nullValue: String? = metadata.params["nullValue"] as String?
         val containsTrailingDelimiters: Boolean = metadata.params["containsTrailingDelimiters"] as Boolean? ?: false
         val ignoreContentBefore = metadata.params["ignoreContentBefore"] as String?
         val useFieldNamesAsColumnNames = metadata.params["useFieldNamesAsColumnNames"] as Boolean? ?: true
         val recordSeparator = (metadata.params["recordSeparator"] as String? ?: "\r\n").let { value ->
            // If the user has provided the string of \r, it's parsed as \\r, (to escape the sequence),
            // but we actually want the raw sequence.
            SEPERATOR_REPLACEMENTS.fold(value) { acc, replacements->
               val (input, replacement) = replacements
               acc.replace(input,replacement)
            }
         }

         return CsvFormatSpecAnnotation(
            delimiter,
            firstRecordAsHeader,
            nullValue,
            containsTrailingDelimiters,
            ignoreContentBefore,
            useFieldNamesAsColumnNames,
            withQuote,
            recordSeparator
         )
      }
   }


}

object CsvFormatSpec : ModelFormatSpec {
   override val annotations: List<QualifiedName> = listOf(CsvAnnotationSpec.NAME)
   override val serializer: CsvFormatSerializer = CsvFormatSerializer
   override val deserializer: CsvFormatDeserializer = CsvFormatDeserializer
   override val mediaType: String = MediaType.CSV_UTF_8.toString()
}
