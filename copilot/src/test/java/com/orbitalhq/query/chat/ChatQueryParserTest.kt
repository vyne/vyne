package com.orbitalhq.query.chat

import com.google.common.io.Resources
import com.orbitalhq.PackageMetadata
import com.orbitalhq.SourcePackage
import com.orbitalhq.VersionedSource
import com.orbitalhq.asSourcePackage
import com.orbitalhq.copilot.CopilotSettings
import com.orbitalhq.copilot.OpenAiChatService
import com.orbitalhq.query.VyneQlGrammar
import io.kotest.core.spec.style.DescribeSpec
import com.orbitalhq.schemas.taxi.TaxiSchema
import io.kotest.matchers.nulls.shouldNotBeNull
import lang.taxi.packages.TaxiSourcesLoader
import java.nio.file.Paths

/*class ChatQueryParserTest : DescribeSpec({
   // Don't run in CI/CD, just exploring.
   describe("exploring the ChatGPT query API") {
      val apiKey = Resources.getResource("apikey.txt")
         .readText()
         .trim()
      val parser = OpenAiChatService(CopilotSettings(apiKey = apiKey))
      it("should use chatGPT to parse a query") {
         val schema = TaxiSchema.from(
            """
         [[ The name of the movie ]]
         type Title inherits String

         [[ The Id of the film ]]
         type FilmId inherits Int

         [[ The review score ]]
         type Rating inherits Int

         [[ The text of a review ]]
         type ReviewText inherits String

         [[ The duration of a movie ]]
         type DurationInMinutes inherits Int

         [[ A review of a film ]]
         model FilmReview

         [[ The number of unqiue viewers who have watched a film ]]
         model ViewCount

         [[ The total number of watched minutes ]]
         model MinutesWatched

         model FilmAnalyticsEvent {
            uniqueViewers: ViewCount
            minutesWatched : MinutesWatched
         }

         service StreamService {
            operation reviews():Stream<FilmReview>
            operation analytics():Stream<FilmAnalytics>
         }
      """.trimIndent()
         )


         val callResult = parser.generateAndRefineQueryFromText(
            schema,
            "Tell me how long 'Gladiator' is, and it's review score"
         )
         callResult.shouldNotBeNull()
         TODO()

      }

//      it("exploring prompt engineering") {
//         val taxiQL = SourcePackage(
//            PackageMetadata.from("com.orbitalhq", "core-types", "1.0.0"),
//            listOf(
//               VersionedSource(
//                  "TaxiQL",
//                  version = "0.1.0",
//                  VyneQlGrammar.QUERY_TYPE_TAXI
//               )
//            )
//         )
//         val path = Paths.get("/home/martypitt/dev/orbital-demos/hz-demo/taxi")
//         val srcPackage = TaxiSourcesLoader.loadPackage(path).asSourcePackage()
//
//         val schema = TaxiSchema.from(listOf(taxiQL, srcPackage))
//         val taxi = parser.parseToTaxiQl(
//            schema,
//            "Build a real time stream of trades. Include the name of the trader, the name of the instrument, the quantity and hit price on the order. Also include the last traded price for the same instrument, and the ESG score (calculated as the average of the Environmental, Social and Governance pillar scores) for the instrument"
//         )
//         println(taxi)
//      }

      it("exploring date formatting") {
         val taxiQL = SourcePackage(
            PackageMetadata.from("com.orbitalhq", "core-types", "1.0.0"),
            listOf(
               VersionedSource(
                  "TaxiQL",
                  version = "0.1.0",
                  VyneQlGrammar.QUERY_TYPE_TAXI
               )
            )
         )
         val path = Paths.get("/home/martypitt/dev/orbital-demos/trading-demo/taxi")
         val srcPackage = TaxiSourcesLoader.loadPackage(path).asSourcePackage()

         val schema = TaxiSchema.from(listOf(taxiQL, srcPackage))
         parser.generateAndRefineQueryFromText(schema, "Give me all the orders executed in the last month")
      }
   }
})*/
