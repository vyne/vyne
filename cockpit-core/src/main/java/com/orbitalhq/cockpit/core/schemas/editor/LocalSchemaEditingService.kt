package com.orbitalhq.cockpit.core.schemas.editor


import arrow.core.*
import com.google.common.collect.Sets
import com.orbitalhq.PackageIdentifier
import com.orbitalhq.PackageMetadata
import com.orbitalhq.SourcePackage
import com.orbitalhq.VersionedSource
import com.orbitalhq.cockpit.core.schemas.BuiltInTypesProvider
import com.orbitalhq.cockpit.core.schemas.editor.generator.VyneSchemaToTaxiGenerator
import com.orbitalhq.cockpit.core.schemas.editor.operations.SchemaEdit
import com.orbitalhq.cockpit.core.schemas.editor.operations.SourceEditResult
import com.orbitalhq.cockpit.core.schemas.editor.splitter.SingleTypePerFileSplitter
import com.orbitalhq.cockpit.core.schemas.editor.splitter.SourceSplitter
import com.orbitalhq.schema.consumer.SchemaStore
import com.orbitalhq.schemaServer.core.editor.SchemaEditorService
import com.orbitalhq.schemaServer.core.packages.PackageService
import com.orbitalhq.schemaServer.editor.SchemaEditRequest
import com.orbitalhq.schemaServer.editor.SchemaEditResponse
import com.orbitalhq.schemaServer.editor.SchemaEditValidator
import com.orbitalhq.schemas.*
import com.orbitalhq.schemas.taxi.TaxiSchema
import com.orbitalhq.schemas.taxi.filtered
import com.orbitalhq.schemas.taxi.toVyneQualifiedName
import com.orbitalhq.security.VynePrivileges
import com.orbitalhq.spring.http.BadRequestException
import lang.taxi.CompilationError
import lang.taxi.CompilationException
import lang.taxi.TaxiDocument
import lang.taxi.errors
import lang.taxi.generators.GeneratedTaxiCode
import lang.taxi.types.CompilationUnit
import lang.taxi.types.Compiled
import lang.taxi.types.ImportableToken
import lang.taxi.types.Type
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*

// Note on tech debt.
// Originally, there were two seperate services - this one
// was part of the UI stack (Vyne query service), and the
// schema editing was done on a schema server.
// We folded those two together.
// So the SchemaEditorService here could actually be folded into this
// class, or refactored / split out.
@RestController
class LocalSchemaEditingService(
   private val packagesServiceApi: PackageService,
   private val editorService: SchemaEditorService,
   private val schemaStore: SchemaStore

) {
   fun getEditorConfig() = editorService.getEditorConfig()

   companion object {
      private val logger = KotlinLogging.logger {}
   }

   /**
    * Submits an edit to the source of a schema.
    *
    * This approach is experimental, but intended to replace submitEditedSchema,
    * which relies on Vyne Schema -> Taxi generation, and gets complex with
    * schemas that are merged together.
    *
    * Instead, this approach is intended to provide small edits to source files.
    */
   @PostMapping("/api/schemas/edits")
   @PreAuthorize("hasAuthority('${VynePrivileges.EditSchema}')")
   fun submitSchemaEditOperation(
      @RequestBody edit: SchemaEdit
   ): Mono<SchemaSubmissionResult> {
      val schema = schemaStore.schema()
      return packagesServiceApi.loadPackage(edit.packageIdentifier.uriSafeId)
         .flatMap { packageWithDescription ->
            if (!packageWithDescription.description.editable) {
               return@flatMap Mono.error(BadRequestException("${edit.packageIdentifier.id} is not editable"))
            }

            // Grab the current state, if applicable.
            // Currently, we look at the first edit, and determine if this requires us loading initial state.
            // (eg., for an edit operation).
            // For an import, we don't load the initial state.
            // This is for cosmetic reasons.
            // We return the result of all the edits back to the UI.
            // Generally, we only want to show the edited source that's relevant to this operation.
            // If this an insert-with-subsequent-edits, then we don't really wanna show all the other source in the package.
            // However, I suspect this will break thigns down the line.
            // This choice is cosmetic, and can be revisited.
            // Revisting:  This didn't work, as when we're doing something like
            // Modify an existing type with a new type I just created, then the
            // first edit is "new type I just created", but I need the existing type.
            // Find another way to solve the cosmetics.
            val firstEdit = edit.edits.first()

            val currentSourcePackage = packageWithDescription.parsedPackage.toSourcePackage()
//            val currentSourcePackage = if (firstEdit.loadExistingState) {
//               packageWithDescription.parsedPackage.toSourcePackage()
//            } else {
//               // If we're not loading the sources, start with an empty source package.
//               SourcePackage(packageWithDescription.parsedPackage.metadata, emptyList(), emptyMap())
//            }
            val initial: Either<CompilationException, SourceEditResult> =
               SourceEditResult(currentSourcePackage, schema.asTaxiSchema().taxi, emptySet(), emptyList()).right()

            // Apply all the edits, incrementally.
            val editResult = edit.edits
               .fold(initial) { acc, editOperation ->
                  acc.flatMap { (currentSource, currentTaxi, accumulatedEdits) ->
                     editOperation.applyTo(currentSource, currentTaxi)
                        .map { editResult ->
                           editResult.copy(touchedFileNames = editResult.touchedFileNames + accumulatedEdits)
                        }
                  }
               }

            val (updatedSourcePackage, updatedTaxi, touchedFilenames) = editResult.getOrElse { throw it }
            val (compilationMessages, updatedTaxiSchema) = TaxiSchema.compiled(
               listOf(updatedSourcePackage),
               imports = listOf(schema.asTaxiSchema())
            )

            val pendingUpdates = if (edit.dryRun) {
               edit.edits
            } else {
               emptyList()
            }

            val affectedSymbols = edit.edits.flatMap { it.calculateAffectedTypes() }
            val editedTypes = affectedSymbols.filter { (kind, _) -> kind == SchemaMemberKind.TYPE }
               .filter { (_, name) -> !BuiltInTypesProvider.isInternalNamespace(name.namespace) }
               .map { (_, name) -> name }
               .let { affectedTypeNames: List<QualifiedName> ->
                  updatedTaxiSchema.types.filter {
                     affectedTypeNames.contains(it.name)
                  }
               }

            val editedServices = affectedSymbols.filter { (kind, _) -> kind == SchemaMemberKind.SERVICE }
               .filter { (_, name) -> !BuiltInTypesProvider.isInternalNamespace(name.namespace) }
               .map { (_, name) -> name }
               .let { affectedTypeNames: List<QualifiedName> ->
                  updatedTaxiSchema.services.filter {
                     affectedTypeNames.contains(it.name)
                  }
               }

            val editedQueries = affectedSymbols.filter { (kind, _) -> kind == SchemaMemberKind.QUERY }
               .map { (_, name) -> name }
               .let { affectedTypeNames: List<QualifiedName> ->
                  updatedTaxiSchema.queries.filter {
                     affectedTypeNames.contains(it.name)
                  }
               }

            val sourcePackageWithOnlyTouchedFiles = updatedSourcePackage.copy(
               sources = updatedSourcePackage.sources.filter { touchedFilenames.contains(it.name) }
                  .map {
                     if (it.packageIdentifier == null) {
                        it.copy(packageIdentifier = updatedSourcePackage.identifier)
                     } else it
                  }
            )

            val messagesFromUpdate = if (edit.dryRun) {
               Mono.just(emptyList())
            } else {
               val modifiedSources = updatedSourcePackage.sources
                  .filter { touchedFilenames.contains(it.name) }
               submitEdits(modifiedSources, updatedSourcePackage.identifier).map { it.messages }
            }

            messagesFromUpdate.map { messages ->
               SchemaSubmissionResult(
                  editedTypes.toSet(),
                  editedServices.toSet(),
                  editedQueries.toSet(),
                  compilationMessages,
                  edit.dryRun,
                  sourcePackageWithOnlyTouchedFiles,
                  pendingUpdates,
                  messages
               )
            }
         }
   }

   /**
    * Submits an actual schema (a subset of it - just types and services/operations).
    * The schema is used to generate taxi.
    * Note that any taxi present in the types & services is ignored.
    * This operation is used when importing / editing from the UI, and is an approach which
    * reduces / eliminates the need for client-side taxi generation code.
    */
   @Deprecated("use submitSchemaEditOperation instead")
   @PostMapping("/api/schemas/edit", consumes = [MediaType.APPLICATION_JSON_VALUE])
   @PreAuthorize("hasAuthority('${VynePrivileges.EditSchema}')")
   fun submitEditedSchema(
      @RequestBody editedSchema: EditedSchema,
      @RequestParam("packageIdentifier") rawPackageIdentifier: String,
      @RequestParam("validate", required = false) validateOnly: Boolean = false,
   ): Mono<SchemaSubmissionResult> {
      logger.info {
         "Received request to edit schema: \n " +
            "types: ${editedSchema.types.map { it.fullyQualifiedName }} \n " +
            "services: ${editedSchema.services.map { it.name.fullyQualifiedName }}"
      }
      return ensureTargetPackageIsEditable(rawPackageIdentifier).flatMap {
         ensureSinglePackageForTypeOrService(PackageIdentifier.fromId(rawPackageIdentifier), editedSchema)
         val generator = VyneSchemaToTaxiGenerator()
         val existingPartialSchemaForEditedPackage =
            this.schemaStore.schemaSet.schema.getPartialSchemaForPackage(rawPackageIdentifier)
         //getPartialSchemaForPackage(rawPackageIdentifier)
         val generated = generator.generateWithPackageUpsertDelete(
            PackageIdentifier.fromId(rawPackageIdentifier),
            editedSchema,
            existingPartialSchemaForEditedPackage,
            this::getCurrentSchemaExcluding
         )
         if (generated.messages.isNotEmpty()) {
            val message =
               "Generation of taxi completed - ${generated.messages.size} messages: \n ${
                  generated.messages.joinToString(
                     "\n"
                  )
               }"
            if (generated.hasWarnings || generated.hasErrors) {
               logger.warn { message }
            } else {
               logger.info { message }
            }
         } else {
            logger.info { "Generation of taxi completed - no messages or warnings were produced" }
         }
         doSubmit(generated, validateOnly, rawPackageIdentifier)
      }
   }

   // If a type has definitions across multiple packages, we should reject.
   private fun ensureSinglePackageForTypeOrService(packageIdentifier: PackageIdentifier, editedSchema: EditedSchema) {
      val otherPackages =
         schemaStore.schemaSet.packages.filter { it.identifier != packageIdentifier }.map { it.identifier }
      val typesInOtherPackages = schemaStore
         .schemaSet
         .schema
         .types.filter { it.sources.any { source -> source.packageIdentifier != null && otherPackages.contains(source.packageIdentifier) } }
         .map { it.qualifiedName.fullyQualifiedName }
         .toSet()

      val servicesInOtherPackages = schemaStore
         .schemaSet
         .schema
         .services.filter {
            it.sourceCode.any { source ->
               source.packageIdentifier != null && otherPackages.contains(
                  source.packageIdentifier
               )
            }
         }
         .map { it.fullyQualifiedName }
         .toSet()

      val dupTypes = Sets.intersection(typesInOtherPackages, editedSchema.types.map { it.fullyQualifiedName }.toSet())
      if (dupTypes.isNotEmpty()) {
         val errorMessage = dupTypes.map {
            val definitionsInExistingPackages =
               schemaStore.schemaSet.schema.type(it).sources.joinToString { sourceCode ->
                  sourceCode.packageIdentifier?.id ?: ""
               }
            "${QualifiedName.from(it).shortDisplayName} is defined in $definitionsInExistingPackages"
         }
         throw BadRequestException("Editing types with definitions in multiple packages is not supported. $errorMessage")
      }

      val dupServices =
         Sets.intersection(servicesInOtherPackages, editedSchema.services.map { it.name.fullyQualifiedName }.toSet())
      if (dupServices.isNotEmpty()) {
         val errorMessage = dupServices.map {
            val definitionsInExistingPackages =
               schemaStore.schemaSet.schema.service(it).sourceCode.joinToString { sourceCode ->
                  sourceCode.packageIdentifier?.id ?: ""
               }
            "${QualifiedName.from(it).shortDisplayName} is defined in $definitionsInExistingPackages"
         }
         throw BadRequestException("Editing services with definitions in multiple packages is not supported. $errorMessage")
      }
   }

   // Changes need to be part of an editable package
   private fun ensureTargetPackageIsEditable(rawPackageIdentifier: String): Mono<Unit> {
      val packageIdentifier = PackageIdentifier.fromId(rawPackageIdentifier)
      return packagesServiceApi
         .listPackages()
         .map { it.firstOrNull { f -> f.identifier == packageIdentifier } }
         .map { if (it?.editable == true) Unit else throw BadRequestException("$rawPackageIdentifier is not editable") }
   }

   /**
    * Returns a TaxiSchema that does not contain definitions
    * for the types or services provided
    */
   private fun getCurrentSchemaExcluding(types: Set<PartialType>, services: Set<PartialService>): TaxiSchema {
      val currentSchema = schemaStore.schemaSet.schema
      val typeNames: Set<QualifiedName> = types.map { it.name }.toSet()
      val serviceNames = services.map { it.name }.toSet()
      // Expect that there's only a single taxi schema.  We've migrated schema handling
      // so that the schemaStore just composes everything into a single taxi schema.
      val taxiSchema = currentSchema.taxiSchemas.single()
      val filteredTaxiDocument = taxiSchema.document.filtered(
         typeFilter = { type: Type -> !typeNames.contains(type.toVyneQualifiedName()) },
         serviceFilter = { service -> !serviceNames.contains(service.toQualifiedName().toVyneQualifiedName()) }
      )
      return TaxiSchema(
         filteredTaxiDocument, taxiSchema.packages, taxiSchema.functionRegistry
      )
   }

   /**
    * Submit a taxi string containing schema changes.
    * Updates are persisted to the local schema repository, and then published to
    * the schema store.
    *
    * The updated Vyne types containing in the Taxi string are returned.
    */
   @PostMapping(
      "/api/schema/taxi/{packageIdentifier}",
      consumes = [MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE]
   )
   @PreAuthorize("hasAuthority('${VynePrivileges.EditSchema}')")
   fun submit(
      @RequestBody taxi: String,
      @RequestParam("validate", required = false) validateOnly: Boolean = false,
      @PathVariable("packageIdentifier") rawPackageIdentifier: String
   ): Mono<SchemaSubmissionResult> {
      return doSubmit(GeneratedTaxiCode(listOf(taxi), emptyList()), validateOnly, rawPackageIdentifier)
   }

   fun submitEdits(sourcePackage: SourcePackage): Mono<SchemaEditResponse> {
      return submitEdits(sourcePackage.sourcesWithPackageIdentifier, sourcePackage.identifier)
   }

   fun submitEdits(
      versionedSources: List<VersionedSource>,
      packageIdentifier: PackageIdentifier
   ): Mono<SchemaEditResponse> {
      logger.info { "Submitting edit requests to schema server for files ${versionedSources.joinToString(", ") { it.name }}" }
      return editorService.submitEdits(
         SchemaEditRequest(packageIdentifier, versionedSources)
      )

   }

   private fun doSubmit(
      generatedSource: GeneratedTaxiCode,
      validateOnly: Boolean = false,
      rawPackageIdentifier: String
   ): Mono<SchemaSubmissionResult> {
      val importRequestSourceName = "ImportRequest_${UUID.randomUUID()}"
      val packageIdentifier = PackageIdentifier.fromId(rawPackageIdentifier)
      val (compilationMessages, compiled) = validate(generatedSource, importRequestSourceName)
      val errors = compilationMessages.errors()
      if (errors.isNotEmpty()) {
         throw CompilationException(errors)
      }
      val typesInThisRequest = getCompiledElementsInSources(compiled.types, importRequestSourceName)
      val servicesInThisRequest = getCompiledElementsInSources(compiled.services, importRequestSourceName)
      val queriesInThisRequest = getCompiledElementsInSources(compiled.queries, importRequestSourceName)
      val generatedThings: List<Pair<ImportableToken, List<CompilationUnit>>> =
         typesInThisRequest + servicesInThisRequest + queriesInThisRequest
      val (updatedSchema, versionedSources) = toVersionedSourcesAndSchema(generatedThings, compiled)
      val persist = !validateOnly
      val vyneTypes = typesInThisRequest.map { (type, _) -> updatedSchema.type(type) }
      val vyneServices = servicesInThisRequest.map { (service, _) -> updatedSchema.service(service.qualifiedName) }
      val vyneQueries =
         queriesInThisRequest.map { (query, _) -> updatedSchema.queries.single { it.name.parameterizedName == query.name.parameterizedName } }

      val messagesFromUpdate =  if (persist) {
         submitEdits(versionedSources, packageIdentifier)
            .map { it.messages }
      } else {
         Mono.just(emptyList())
      }

      return messagesFromUpdate.map { messages ->
         SchemaSubmissionResult(
            vyneTypes.toSet(),
            vyneServices.toSet(),
            vyneQueries.toSet(),
            compilationMessages,
            dryRun = validateOnly,
            SourcePackage(PackageMetadata.from(packageIdentifier), emptyList(), emptyMap()),
            emptyList(),
            messages
         )
      }
   }

   private fun <T : Compiled> getCompiledElementsInSources(
      compiled: Set<T>,
      sourceNamePrefix: String
   ): List<Pair<T, List<CompilationUnit>>> {
      return compiled
         .mapNotNull { type ->
            val compilationUnitsInThisOperation =
               type.compilationUnits.filter { compilationUnit ->
                  compilationUnit.source.sourceName.startsWith(
                     sourceNamePrefix
                  )
               }
            if (compilationUnitsInThisOperation.isNotEmpty()) {
               type to compilationUnitsInThisOperation
            } else {
               null
            }
         }
   }

   //
   private fun validate(
      generatedSource: GeneratedTaxiCode,
      importRequestSourceName: String
   ): Pair<List<CompilationError>, TaxiDocument> {
      val sources = generatedSource.taxi.mapIndexed { index, src ->
         VersionedSource.unversioned("${importRequestSourceName}_$index", src)
      }
      // First we pre-validate with the compiler.
      // We need to do this, as we want to ensure the content is valid before writing to disk as VersionedSources.
      val (messages, compiled) = SchemaEditValidator.validate(sources, schemaStore.schema().asTaxiSchema())

      if (messages.errors().isNotEmpty()) {
         throw BadRequestException(messages.errors().joinToString("\n") { it.detailMessage })
      }
      return messages to compiled
   }

   private fun toVersionedSourcesAndSchema(
      typesAndSources: List<Pair<ImportableToken, List<CompilationUnit>>>,
      taxiDocument: TaxiDocument
   ): Pair<Schema, List<VersionedSource>> {
      val versionedSources = toVersionedSources(typesAndSources)
      return TaxiSchema(taxiDocument, listOf()) to versionedSources
   }

   private fun toVersionedSources(typesAndSources: List<Pair<ImportableToken, List<CompilationUnit>>>): List<VersionedSource> {
      // We have to work out a Type-to-file strategy.
      // As a first pass, I'm using a separate file for each type.
      // It's a little verbose on the file system, but it's a reasonable start, as it makes managing edits easier, since
      // we don't have to worry about insertions / modification within the middle of a file.
      val splitter: SourceSplitter = SingleTypePerFileSplitter
      return splitter.toVersionedSources(typesAndSources)
   }

   fun getSourcePackage(packageIdentifier: PackageIdentifier): Mono<SourcePackage> {
      return packagesServiceApi.loadPackage(packageIdentifier.uriSafeId)
         .map { it.parsedPackage.toSourcePackage() }
   }
}
