package com.orbitalhq.schemaServer.core.schemaStoreConfig.clustered

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.map.IMap
import com.orbitalhq.ParsedPackage
import com.orbitalhq.schema.publisher.ExpiringSourcesStore
import com.orbitalhq.schema.publisher.KeepAliveStrategyMonitor
import com.orbitalhq.schema.publisher.http.HttpPollKeepAliveStrategyMonitor
import com.orbitalhq.schemaServer.core.config.SchemaUpdateNotifier
import com.orbitalhq.schemaServer.core.schemaStoreConfig.clustered.DistributedSchemaStoreClient.Companion.SCHEMA_SOURCES_MAP
import com.orbitalhq.schemaStore.ValidatingSchemaStoreClient
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient
import java.time.Duration

@ConditionalOnProperty("vyne.schema.server.clustered", havingValue = "true", matchIfMissing = false)
@Configuration
class ClusteredSchemaServerSourceProviderConfiguration {
   /**
    * Can't use UnversionedPackageIdentifier here as String alias due to
    * https://youtrack.jetbrains.com/issue/KT-24700
    */
   @Bean(SCHEMA_SOURCES_MAP)
   @ConditionalOnExpression("\${vyne.schema.server.clustered:false}")
   fun streamStateCache(hazelcastInstance: HazelcastInstance
   ): IMap<String, ParsedPackage> {

      return hazelcastInstance
         .getMap(SCHEMA_SOURCES_MAP)
   }
   @Bean
   @ConditionalOnExpression("\${vyne.schema.server.clustered:false}")
   fun localValidatingSchemaStoreClient(hazelcastInstance: HazelcastInstance,
                                        @Qualifier(SCHEMA_SOURCES_MAP) packagesByIdMap: IMap<String, ParsedPackage>)
   : ValidatingSchemaStoreClient {
      return DistributedSchemaStoreClient(hazelcastInstance, packagesByIdMap)
   }

   @Bean
   @ConditionalOnExpression("\${vyne.schema.server.clustered:false}")
   fun httpPollKeepAliveStrategyMonitor(
      @Value("\${vyne.schema.management.keepAlivePollFrequency:1s}") keepAlivePollFrequency: Duration,
      @Value("\${vyne.schema.management.httpRequestTimeout:30s}") httpRequestTimeout: Duration,
      webClientBuilder: WebClient.Builder,
      hazelcastInstance: HazelcastInstance
   ): HttpPollKeepAliveStrategyMonitor = HttpPollKeepAliveStrategyMonitor(
      pollFrequency = keepAlivePollFrequency,
      httpRequestTimeout = httpRequestTimeout,
      webClientBuilder = webClientBuilder,
      lastPingTimes = hazelcastInstance.getMap("httpPollKeepAliveStrategyMonitorPingMap")
   )

   @Bean
   @ConditionalOnExpression("\${vyne.schema.server.clustered:false}")
   fun expiringSourcesStore(
      hazelcastInstance: HazelcastInstance,
      keepAliveStrategyMonitors: List<KeepAliveStrategyMonitor>
   ): ExpiringSourcesStore {
      return ExpiringSourcesStore(
         keepAliveStrategyMonitors = keepAliveStrategyMonitors,
         packages = hazelcastInstance.getMap("expiringSourceMap")
      )
   }

   @Bean
   @ConditionalOnExpression("\${vyne.schema.server.clustered:false}")
   fun schemaUpdateNotifier(
      validatingSchemaStoreClient: ValidatingSchemaStoreClient,
      hazelcastInstance: HazelcastInstance
   ): SchemaUpdateNotifier {
      return DistributedSchemaUpdateNotifier(
         hazelcastInstance.getReliableTopic("/schema-update"),
         validatingSchemaStoreClient
      )
   }
}
