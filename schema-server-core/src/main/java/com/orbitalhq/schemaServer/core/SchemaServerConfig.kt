package com.orbitalhq.schemaServer.core

/**
 * Marker class for config scanning at the base of the schema
 * server packages
 */
object SchemaServerConfig
