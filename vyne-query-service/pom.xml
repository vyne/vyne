<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <parent>
      <groupId>com.orbitalhq</groupId>
      <artifactId>platform</artifactId>
      <version>0.35.0-SNAPSHOT</version>
   </parent>
   <modelVersion>4.0.0</modelVersion>

   <artifactId>vyne-query-service</artifactId>

   <properties>
      <buildNumber>0</buildNumber>
      <app.main.class>com.orbitalhq.queryService.QueryServiceApp</app.main.class>
      <app.distribution.directory>${project.build.directory}/distributions/app</app.distribution.directory>
      <!-- see the plugin below for why we're disabling this -->
      <spring-boot.repackage.skip>true</spring-boot.repackage.skip>

      <ktor.version>2.3.10</ktor.version>
   </properties>

   <dependencies>
      <dependency>
         <groupId>com.hazelcast</groupId>
         <artifactId>hazelcast</artifactId>
         <version>${hazelcast.version}</version>
         <classifier>tests</classifier>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>copilot</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>io.ktor</groupId>
         <artifactId>ktor-client-cio-jvm</artifactId>
         <version>${ktor.version}</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>io.ktor</groupId>
         <artifactId>ktor-client-core-jvm</artifactId>
         <version>${ktor.version}</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>io.ktor</groupId>
         <artifactId>ktor-client-websockets-jvm</artifactId>
         <version>${ktor.version}</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>spring-utils</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>monitoring-common</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>cockpit-core</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>license-client</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-core-types</artifactId>
         <version>${project.version}</version>
         <scope>test</scope>
         <type>test-jar</type>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-csv-utils</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>protobuf-utils</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>schema-server-api</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>auth-common</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>org.taxilang</groupId>
         <artifactId>compiler</artifactId>
         <type>test-jar</type>
         <version>${taxi.version}</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-history-core</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>history-service</artifactId>
         <version>${project.version}</version>
      </dependency>

      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>history-persistence</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>jdbc-connector</artifactId>
         <version>${project.version}</version>
         <exclusions>
            <exclusion>
               <groupId>org.slf4j</groupId>
               <artifactId>slf4j-nop</artifactId>
            </exclusion>
         </exclusions>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>kafka-connector</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>s3-connector</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>blob-connector</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>sqs-connector</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <artifactId>lambda-connector</artifactId>
         <groupId>com.orbitalhq</groupId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>org.taxilang</groupId>
         <artifactId>taxi-lang-service</artifactId>
         <version>${taxi.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-search</artifactId>
         <version>${project.version}</version>
      </dependency>
      <!--      <dependency>-->
      <!--         <groupId>com.orbitalhq</groupId>-->
      <!--         <artifactId>cask-api</artifactId>-->
      <!--         <version>${project.version}</version>-->
      <!--      </dependency>-->
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-query-api</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>org.taxilang</groupId>
         <artifactId>lang-to-taxi-api</artifactId>
         <version>${taxi.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-spring</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-client</artifactId>
         <version>${project.version}</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.springframework.cloud</groupId>
         <artifactId>spring-cloud-contract-wiremock</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.bitbucket.b_c</groupId>
         <artifactId>jose4j</artifactId>
         <version>0.9.3</version>
         <scope>test</scope>
      </dependency>
      <!--      <dependency>-->
      <!--         <groupId>io.netty</groupId>-->
      <!--         <artifactId>netty-tcnative-boringssl-static</artifactId>-->
      <!--         <version>2.0.59.Final</version>-->
      <!--      </dependency>-->
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-security</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-actuator</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.security</groupId>
         <artifactId>spring-security-oauth2-resource-server</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.security</groupId>
         <artifactId>spring-security-oauth2-jose</artifactId>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>query-node-core</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>org.taxilang</groupId>
         <artifactId>swagger2taxi</artifactId>
         <version>${taxi.version}</version>
      </dependency>
      <!-- Commenting out jsonSchema support.  It wasn't actually used, and
      it introduces a high-risk CVE through unsupported dependency
      everit-org/json-schema -->
      <!--      <dependency>-->
      <!--         <groupId>org.taxilang</groupId>-->
      <!--         <artifactId>jsonSchema-to-taxi</artifactId>-->
      <!--         <version>${taxi.version}</version>-->
      <!--      </dependency>-->
      <dependency>
         <groupId>com.google.guava</groupId>
         <artifactId>guava</artifactId>
      </dependency>
      <dependency>
         <groupId>net.logstash.logback</groupId>
         <artifactId>logstash-logback-encoder</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-data-jpa</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-data-jdbc</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-webflux</artifactId>
      </dependency>
      <!--      <dependency>-->
      <!--         <groupId>com.orbitalhq</groupId>-->
      <!--         <artifactId>pipelines-orchestrator-api</artifactId>-->
      <!--         <version>${project.version}</version>-->
      <!--      </dependency>-->
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>pipeline-jet-api</artifactId>
         <version>${project.version}</version>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-reactor-netty</artifactId>
      </dependency>
      <dependency>
         <groupId>io.github.config4k</groupId>
         <artifactId>config4k</artifactId>
         <version>0.5.0</version>
      </dependency>

      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-websocket</artifactId>
         <exclusions>
            <!-- Exclude the Tomcat dependency -->
            <exclusion>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-tomcat</artifactId>
            </exclusion>
         </exclusions>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>schema-server-core</artifactId>
         <version>${project.version}</version>
      </dependency>

      <dependency>
         <groupId>javax.websocket</groupId>
         <artifactId>javax.websocket-api</artifactId>
         <version>1.1</version>
      </dependency>
      <dependency>
         <groupId>org.skyscreamer</groupId>
         <artifactId>jsonassert</artifactId>
         <version>1.5.1</version>
         <scope>test</scope>
         <exclusions>
            <exclusion>
               <groupId>com.vaadin.external.google</groupId>
               <artifactId>android-json</artifactId>
            </exclusion>
         </exclusions>
      </dependency>
      <!-- JSONAssert has an undeclared dependency on org.json:json
      https://github.com/skyscreamer/JSONassert/issues/170 -->
      <dependency>
         <groupId>org.json</groupId>
         <artifactId>json</artifactId>
         <version>20231013</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>test-cli</artifactId>
         <version>${project.version}</version>
      </dependency>

      <dependency>
         <groupId>org.jetbrains.kotlinx</groupId>
         <artifactId>kotlinx-coroutines-core</artifactId>
      </dependency>
      <dependency>
         <groupId>org.jetbrains.kotlinx</groupId>
         <artifactId>kotlinx-coroutines-reactor</artifactId>
      </dependency>
      <dependency>
         <groupId>org.jetbrains.kotlinx</groupId>
         <artifactId>kotlinx-coroutines-test</artifactId>
         <scope>test</scope>
      </dependency>

      <dependency>
         <groupId>app.cash.turbine</groupId>
         <artifactId>turbine-jvm</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.http4k</groupId>
         <artifactId>http4k-core</artifactId>
      </dependency>
      <dependency>
         <groupId>org.http4k</groupId>
         <artifactId>http4k-server-netty</artifactId>
         <scope>test</scope>
      </dependency>

      <!-- Monitoring and reporting -->

      <dependency>
         <groupId>io.micrometer</groupId>
         <artifactId>micrometer-core</artifactId>
      </dependency>

      <dependency>
         <groupId>io.micrometer</groupId>
         <artifactId>micrometer-registry-prometheus</artifactId>
      </dependency>

      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-testcontainers</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.testcontainers</groupId>
         <artifactId>junit-jupiter</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.testcontainers</groupId>
         <artifactId>testcontainers</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.testcontainers</groupId>
         <artifactId>postgresql</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.flywaydb</groupId>
         <artifactId>flyway-core</artifactId>
      </dependency>
      <dependency>
         <groupId>org.flywaydb</groupId>
         <artifactId>flyway-database-postgresql</artifactId>
      </dependency>
      <dependency>
         <groupId>com.squareup.okhttp3</groupId>
         <artifactId>mockwebserver</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>io.projectreactor</groupId>
         <artifactId>reactor-test</artifactId>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>io.projectreactor.kotlin</groupId>
         <artifactId>reactor-kotlin-extensions</artifactId>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>taxiql-query-engine</artifactId>
         <version>${project.version}</version>
         <scope>test</scope>
         <type>test-jar</type>
      </dependency>
      <dependency>
         <groupId>com.orbitalhq</groupId>
         <artifactId>vyne-spring</artifactId>
         <version>${project.version}</version>
         <type>test-jar</type>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>org.jetbrains.kotlinx</groupId>
         <artifactId>kotlinx-serialization-cbor</artifactId>
      </dependency>
      <!-- TODO Remove once the right version is picked automatically. This was needed due to wire-compiler of protobuf-utils pulling version 1.0.1 -->
      <dependency>
         <groupId>org.jetbrains.kotlinx</groupId>
         <artifactId>kotlinx-serialization-core-jvm</artifactId>
         <version>${kotlin.serialization.version}</version>
      </dependency>
      <dependency>
         <groupId>junit</groupId>
         <artifactId>junit</artifactId>
         <scope>test</scope>
      </dependency>
      <!-- https://mvnrepository.com/artifact/net.sourceforge.htmlunit/htmlunit -->
      <dependency>
         <groupId>net.sourceforge.htmlunit</groupId>
         <artifactId>htmlunit</artifactId>
         <version>2.70.0</version>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>javax.xml.bind</groupId>
         <artifactId>jaxb-api</artifactId>
         <version>2.3.1</version>
         <scope>test</scope>
      </dependency>
   </dependencies>

   <build>
      <plugins>
         <plugin>
            <groupId>io.github.git-commit-id</groupId>
            <artifactId>git-commit-id-maven-plugin</artifactId>
            <version>5.0.0</version>
            <executions>
               <execution>
                  <id>get-the-git-infos</id>
                  <goals>
                     <goal>revision</goal>
                  </goals>
               </execution>
            </executions>
            <configuration>
               <generateGitPropertiesFile>true</generateGitPropertiesFile>
               <generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties
               </generateGitPropertiesFilename>
            </configuration>
         </plugin>
         <!-- don't generate a source jar -->
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <configuration>
               <skipSource>true</skipSource>
            </configuration>
         </plugin>
         <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <executions>
               <execution>
                  <id>repackage</id>
                  <!-- We can't use a spring boot repackaged fat jar.
               We're using Wire (https://github.com/square/wire) for protobuf generation.
    At runtime, this uses okio (https://github.com/square/okio) generate artifacts in order to generate a protobuf schema - and it treats
    the current classpath as a zip.  This is reasonable, because normally the jar is a valid zip.
    However, spring boot rewires this to make an executable, fat jar.  That breaks okio, and therefore wire.

    Ideally, there'd be a workaround, but I haven't found one.
    See discussions here:
    https://github.com/square/okio/issues/1094
    https://github.com/square/wire/discussions/2209

    So, we're reverting to a classic jar build using maven appassembler

                -->
                  <goals>
                     <goal>repackage</goal>
                  </goals>
                  <configuration>
                     <skip>true</skip>
                  </configuration>
               </execution>
               <execution>
                  <goals>
                     <goal>build-info</goal>
                  </goals>
               </execution>
            </executions>
            <configuration>
               <additionalProperties>
                  <!--suppress UnresolvedMavenProperty -->
                  <baseVersion>
                     ${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.incrementalVersion}
                  </baseVersion>
                  <buildNumber>
                     ${buildNumber}
                  </buildNumber>
               </additionalProperties>
            </configuration>
         </plugin>
         <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>appassembler-maven-plugin</artifactId>
            <version>2.1.0</version>
            <executions>
               <execution>
                  <id>assemble-script</id>
                  <phase>package</phase>
                  <goals>
                     <goal>assemble</goal>
                  </goals>
                  <configuration>
                     <showConsoleWindow>true</showConsoleWindow>
                     <programs>
                        <program>
                           <mainClass>${app.main.class}</mainClass>
                           <id>vyne</id>
                        </program>
                     </programs>
                     <repositoryLayout>flat</repositoryLayout>
                     <repositoryName>lib</repositoryName>
                     <extraJvmArguments>
                        --add-exports=java.base/jdk.internal.ref=ALL-UNNAMED
                        --add-exports=java.base/sun.nio.ch=ALL-UNNAMED
                        --add-exports=jdk.unsupported/sun.misc=ALL-UNNAMED
                        --add-exports=jdk.compiler/com.sun.tools.javac.file=ALL-UNNAMED
                        --add-opens=jdk.compiler/com.sun.tools.javac=ALL-UNNAMED
                        --add-opens=java.base/java.lang=ALL-UNNAMED
                        --add-opens=java.base/java.lang.reflect=ALL-UNNAMED
                        --add-opens=java.base/java.io=ALL-UNNAMED
                        --add-opens=java.base/java.util=ALL-UNNAMED
                     </extraJvmArguments>
                  </configuration>
               </execution>
            </executions>
         </plugin>
         <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <version>3.4.2</version>
            <configuration>
               <descriptors>
                  <descriptor>src/main/assembly/assembly.xml</descriptor>
               </descriptors>
               <appendAssemblyId>false</appendAssemblyId>
               <finalName>vyne</finalName>
            </configuration>
            <executions>
               <execution>
                  <id>make-assembly</id> <!-- this is used for inheritance merges -->
                  <phase>package</phase> <!-- bind to the packaging phase -->
                  <goals>
                     <goal>single</goal>
                  </goals>
               </execution>
            </executions>
         </plugin>
         <!--         <plugin>-->
         <!--            <groupId>org.owasp</groupId>-->
         <!--            <artifactId>dependency-check-maven</artifactId>-->
         <!--         </plugin>-->
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-site-plugin</artifactId>
            <version>3.12.1</version>
         </plugin>
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>3.4.5</version>
         </plugin>
      </plugins>
   </build>
</project>
