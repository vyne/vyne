package com.orbitalhq.queryService.history.db

import app.cash.turbine.test
import app.cash.turbine.testIn
import app.cash.turbine.withTurbineTimeout
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.jayway.awaitility.Awaitility.await
import com.jayway.awaitility.Duration
import com.orbitalhq.cockpit.core.ConfigService
import com.orbitalhq.cockpit.core.connectors.hazelcast.HazelcastHealthCheckProvider
import com.orbitalhq.cockpit.core.content.DefaultContentRepository
import com.orbitalhq.copilot.OpenAiChatService
import com.winterbe.expekt.should
import io.kotest.matchers.collections.shouldNotBeEmpty
import com.orbitalhq.history.db.LineageRecordRepository
import com.orbitalhq.history.db.QueryHistoryDbWriter
import com.orbitalhq.history.db.QueryHistoryRecordRepository
import com.orbitalhq.history.db.QueryResultRowRepository
import com.orbitalhq.history.rest.QueryHistoryService
import com.orbitalhq.http.MockWebServerRule
import com.orbitalhq.http.respondWith
import com.orbitalhq.http.response
import com.orbitalhq.licensing.LicenseManager
import com.orbitalhq.licensing.OrbitalLicenseManager
import com.orbitalhq.models.FailedSearch
import com.orbitalhq.models.OperationResult
import com.orbitalhq.models.OperationResultReference
import com.orbitalhq.models.TypedNull
import com.orbitalhq.query.QueryResponse
import com.orbitalhq.query.ResponseCodeGroup
import com.orbitalhq.query.ResultMode
import com.orbitalhq.query.ValueWithTypeName
import com.orbitalhq.query.connectors.CacheAwareOperationInvocationDecorator
import com.orbitalhq.query.graph.operationInvocation.cache.local.LocalCachingInvokerProvider
import com.orbitalhq.query.history.QueryResultRow
import com.orbitalhq.query.history.QuerySummary
import com.orbitalhq.query.runtime.StreamResultStreamProvider
import com.orbitalhq.query.runtime.core.monitor.ActiveQueryController
import com.orbitalhq.queryService.BaseQueryServiceTest
import com.orbitalhq.queryService.TestSpringConfig
import com.orbitalhq.schema.api.SimpleSchemaProvider
import com.orbitalhq.schemaServer.core.editor.SchemaEditorService
import com.orbitalhq.schemaServer.core.packages.PackageService
import com.orbitalhq.schemaServer.core.repositories.WorkspaceConfigLoader
import com.orbitalhq.schemaServer.core.repositories.lifecycle.ProjectSpecLifecycleEventDispatcher
import com.orbitalhq.schemaServer.core.repositories.lifecycle.ReactiveProjectStoreManager
import com.orbitalhq.spring.http.auth.schemes.AuthWebClientCustomizer
import com.orbitalhq.spring.invokers.Invoker
import com.orbitalhq.spring.invokers.RestTemplateInvoker
import com.orbitalhq.testVyne
import com.orbitalhq.testVyneWithInvokers
import com.orbitalhq.typedObjects
import com.orbitalhq.utils.Benchmark
import com.orbitalhq.utils.Ids
import com.orbitalhq.utils.StrategyPerformanceProfiler
import io.kotest.matchers.string.shouldNotBeEmpty
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import mu.KotlinLogging
import org.http4k.core.Method.GET
import org.http4k.core.Response
import org.http4k.core.Status.Companion.NOT_FOUND
import org.http4k.core.Status.Companion.OK
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Http4kServer
import org.http4k.server.Netty
import org.http4k.server.asServer
import org.junit.After
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.reactive.function.client.WebClient
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.junit.jupiter.Container
import reactor.kotlin.test.test
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds
import kotlin.time.ExperimentalTime

private val logger = KotlinLogging.logger {}

@ExperimentalTime
@ExperimentalCoroutinesApi
@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@Import(TestSpringConfig::class)
@SpringBootTest(
   properties = [
      "vyne.schema.publicationMethod=LOCAL",
      "vyne.search.directory=./search/\${random.int}",
      "vyne.analytics.persistResults=true",
      "vyne.telemetry.enabled=false",
      "vyne.analytics.writerMaxBatchSize=1",
      "vyne.analytics.writerMaxDuration=100ms",
   ]
)
class QueryHistoryPersistenceTest : BaseQueryServiceTest() {
   companion object {
      @Container
      @ServiceConnection
      val postgres = PostgreSQLContainer<Nothing>("postgres:11.1").let {
         it.start()
         it.waitingFor(Wait.forListeningPort())
         it
      } as PostgreSQLContainer<*>

   }

   @MockBean
   lateinit var streamResultStreamProvider: StreamResultStreamProvider

   @MockBean
   lateinit var chatService: OpenAiChatService

   @MockBean
   lateinit var cmsService: DefaultContentRepository

   @MockBean
   lateinit var eventDispatcher: ProjectSpecLifecycleEventDispatcher

   @MockBean
   lateinit var reactiveProjectStoreManager: ReactiveProjectStoreManager

   @MockBean
   lateinit var configLoader : WorkspaceConfigLoader

   @MockBean
   lateinit var hazelcastHealthCheckProvider: HazelcastHealthCheckProvider

   @MockBean
   lateinit var configService: ConfigService
   @MockBean
   lateinit var licenseManager: OrbitalLicenseManager


   @Autowired
   lateinit var queryHistoryRecordRepository: QueryHistoryRecordRepository

   @Autowired
   lateinit var resultRowRepository: QueryResultRowRepository

   @Autowired
   lateinit var lineageRepository: LineageRecordRepository

   @Autowired
   lateinit var historyDbWriter: QueryHistoryDbWriter

   @Autowired
   lateinit var historyService: QueryHistoryService

   @Autowired
   lateinit var activeQueryController: ActiveQueryController

   @MockBean
   lateinit var packagesService: PackageService

   @MockBean
   lateinit var schemaEditorService: SchemaEditorService

   @Rule
   @JvmField
   final val server = MockWebServerRule()

   @Deprecated("Move to server from MockWebServerRule, to be consistent")
   var http4kServer: Http4kServer? = null


   @After
   fun tearDown() {
      if (http4kServer != null) {
         http4kServer!!.stop()
      }
   }

   @Test
   fun `can persist resultRow to jpa`() {
      val persisted = resultRowRepository.save(
         QueryResultRow(
            queryId = "queryId",
            json = "{ foo : Bar }",
            valueHash = 123
         )
      )
      persisted.rowId.should.not.be.`null`
   }

   @Test
   @Transactional
   fun `can persist querySummary to jpa`() {
      // Create a string 100,000 1's numbers long (ie., 100k characters)
      val largeString = (0 until 100000).joinToString(separator = "") { "1" }
      val querySummary = queryHistoryRecordRepository.save(
         QuerySummary(
            queryId = "queryId",
            clientQueryId = "clientQueryId",
            taxiQl = "find { Foo[] }",
            queryJson = largeString,
            startTime = Instant.now(),
            responseStatus = QueryResponse.ResponseStatus.RUNNING,
            anonymousTypesJson = largeString
         )
      )
      querySummary.id.should.not.be.`null`
      queryHistoryRecordRepository.findByClientQueryId("clientQueryId")!!.id.should.equal(querySummary.id)
      queryHistoryRecordRepository.findByQueryId("queryId").id.should.equal(querySummary.id)

      val endTime = Instant.now().plusSeconds(10)
      val updatedCount = queryHistoryRecordRepository.setQueryEnded(
         queryId = "queryId",
         endTime = endTime,
         status = QueryResponse.ResponseStatus.COMPLETED,
         message = "All okey dokey",
         recordCount = 1
      )
      updatedCount.should.equal(1)

      val updated = queryHistoryRecordRepository.findByQueryId(querySummary.queryId)
      updated.endTime?.truncatedTo(ChronoUnit.MILLIS).should.equal(endTime.truncatedTo(ChronoUnit.MILLIS))
      updated.responseStatus.should.equal(QueryResponse.ResponseStatus.COMPLETED)
      updated.errorMessage.should.equal("All okey dokey")
   }


   @Test
   //@Ignore // FLakey
   fun `can read and write query results to db from restful query`() {
      setupTestService(historyDbWriter)
      val clientId = Ids.id("query")
      runBlocking {
         val response = queryService.submitVyneQlQueryStreamingResponse("""find { Order[] }""", ResultMode.TYPED, MediaType.APPLICATION_JSON_VALUE, clientQueryId = clientId)
            .test {
               awaitItem()
               awaitComplete()
            }
      }

      await().atMost(Duration.FIVE_SECONDS).until<Boolean> {
         queryHistoryRecordRepository.findByClientQueryId(clientId) != null
      }
      val record = queryHistoryRecordRepository.findByClientQueryId(clientId)!!
      resultRowRepository.findAllByQueryId(record.queryId).isNotEmpty()
      await().atMost(Duration.FIVE_SECONDS).until<Boolean> {
         resultRowRepository.findAllByQueryId(record.queryId).isNotEmpty()
      }

      val queryHistory = queryHistoryRecordRepository.findByQueryId(record.queryId)

      queryHistoryRecordRepository.findById(queryHistory.id!!)
         .let { updatedHistoryRecord ->
            updatedHistoryRecord
            // Why sn't this workig?
            updatedHistoryRecord.get().endTime.should.not.be.`null`
         }
   }

   @Test
   fun `can read and write query results from taxiQl query`() {
      setupTestService(historyDbWriter)
      val id = UUID.randomUUID().toString()

      runTest {
         val turbine =
            queryService.submitVyneQlQueryStreamingResponse("find { Order[] } as Report[]", clientQueryId = id).testIn(this)

         val first = turbine.awaitItem()
         first.should.not.be.`null`
         turbine.awaitComplete()
      }

      await().atMost(com.jayway.awaitility.Duration.TEN_SECONDS).until {
         val historyRecord = queryHistoryRecordRepository.findByClientQueryId(id)
         historyRecord != null && historyRecord.endTime != null
      }

      val historyRecord = queryHistoryRecordRepository.findByClientQueryId(id)

      historyRecord.should.not.be.`null`
      historyRecord!!.taxiQl.should.equal("find { Order[] } as Report[]")
      historyRecord.endTime.should.not.be.`null`

      val results = resultRowRepository.findAllByQueryId(historyRecord.queryId)

      results.shouldNotBeEmpty()

      // This part of the test is flakey.
//      Awaitility.await().atMost(Duration.FIVE_SECONDS).until<Boolean> {
//         val profileData = historyService.getQueryProfileDataFromClientId(id).block()
//         profileData.remoteCalls.size == 5
//      }
//      val historyProfileData = historyService.getQueryProfileDataFromClientId(id)
//      historyProfileData.block().remoteCalls.should.have.size(5)
   }

   @Test
   fun `can read and write query results from taxiQl query with anonymous type projection`() {
      setupTestService(historyDbWriter)
      val id = UUID.randomUUID().toString()

      runTest {
         val turbine =
            queryService.submitVyneQlQueryStreamingResponse("""find { Order[] } as {
               | id: OrderId
               | traderDetails : {
               |    name : TraderName
               | }
               |}[]""".trimMargin(), clientQueryId = id).testIn(this)

         val first = turbine.awaitItem()
         first.should.not.be.`null`
         turbine.awaitComplete()
      }

      await().atMost(com.jayway.awaitility.Duration.TEN_SECONDS).until {
         val historyRecord = queryHistoryRecordRepository.findByClientQueryId(id)
         historyRecord?.endTime != null
      }

      val historyRecord = queryHistoryRecordRepository.findByClientQueryId(id)

      historyRecord.should.not.be.`null`
      historyRecord!!.anonymousTypesJson!!.shouldNotBeEmpty()
   }

   @Test
   @Ignore
   fun `failed http calls are present in history`() {
      val randomPort = Random.nextInt(10000, 12000)
      val vyne = testVyneWithInvokers(
         """
         model Book {
            title : BookTitle inherits String
            authorId : AuthorId inherits Int
         }
         model Author {
            @Id
            id : AuthorId
            name : AuthorName inherits String
         }
         model Output {
            title : BookTitle
            authorName : AuthorName
         }
         service Service {
            @HttpOperation(method = "GET" , url = "http://localhost:$randomPort/books")
            operation listBooks():Book[]
            @HttpOperation(method = "GET" , url = "http://localhost:$randomPort/authors/{authorId}")
            operation findAuthor(@PathVariable("authorId") authorId: AuthorId):Author
         }
      """.trimIndent()
      ) { schema ->
         listOf(
            CacheAwareOperationInvocationDecorator(
               RestTemplateInvoker(
                  SimpleSchemaProvider(schema),
                  WebClient.builder(),
                  AuthWebClientCustomizer.empty(),
               ),
               LocalCachingInvokerProvider.default()
            )
         )
      }

      http4kServer = routes(
         "/books" bind GET to { Response(OK).body(""" [ { "title" : "How to get taller" , "authorId" : 2 }, { "title" : "How to get taller" , "authorId" : 2 }, { "title" : "How to get taller" , "authorId" : 2 } ]""") },
         "/authors/2" bind GET to { Response(NOT_FOUND).body("""No author with id 2 found""") }
      ).asServer(Netty(randomPort)).start()

      setupTestService(vyne, null, historyDbWriter)

      val id = UUID.randomUUID().toString()

      val query = "find { Book[] } as Output[]"
      var firstResult: ValueWithTypeName? = null
      runTest {

         val turbine =
            queryService.submitVyneQlQueryStreamingResponse(query, clientQueryId = id, resultMode = ResultMode.TYPED).testIn(this)
         val first = turbine.awaitItem()
         firstResult = first as ValueWithTypeName
         first.should.not.be.`null`
         turbine.awaitItem()
         turbine.awaitItem()
         turbine.awaitComplete()
      }

      // Check the lineage on the results
      runBlocking {
         val output = vyne.query(query).typedObjects().first()
         val authorName = output["authorName"]
         authorName.should.be.instanceof(TypedNull::class.java)
         authorName.source.should.be.instanceof(FailedSearch::class.java)
         val source = authorName.source as FailedSearch
         source.failedAttempts.should.have.size(1)
         val failedCallSource = source.failedAttempts.first() as OperationResult
         failedCallSource.remoteCall.resultCode.should.equal(404)
      }

      Thread.sleep(2000) // Allow persistence to catch up

      val profileData = historyService.getQueryProfileDataFromClientId(id)
      val remoteCalls = profileData.block().remoteCalls
      remoteCalls.should.have.size(2)

      // We should see a failure in the operation stats
      val findAuthorStats = profileData.block().operationStats.first { it.operationName == "findAuthor" }
      findAuthorStats.responseCodes.should.contain(ResponseCodeGroup.HTTP_4XX to 1)
      findAuthorStats.callsInitiated.should.equal(1)

      // Should have rich lineage around the null value
      val nodeDetail = historyService.getNodeDetail(firstResult?.queryId!!, firstResult!!.valueId, "authorName")
      // FIXME same as above
//      nodeDetail.source.should.not.be.empty
   }

   @Test
   @Ignore // failing, can't work out why
   fun `remote calls leading to duplicate lineage results are persisted without exceptions`() {
      val randomPort = Random.nextInt(10000, 12000)
      val vyne = testVyneWithInvokers(
         """
         model Book {
            title : BookTitle inherits String
            authorId : AuthorId inherits Int
         }
         model Author {
            @Id
            id : AuthorId
            name : AuthorName inherits String
         }
         model Output {
            title : BookTitle
            authorName : AuthorName
         }
         service Service {
            @HttpOperation(method = "GET" , url = "http://localhost:$randomPort/books")
            operation listBooks():Book[]
            @HttpOperation(method = "GET" , url = "http://localhost:$randomPort/authors/{authorId}")
            operation findAuthor(@PathVariable("authorId") authorId: AuthorId):Author
         }
      """.trimIndent()
      ) { schema ->
         listOf(
            CacheAwareOperationInvocationDecorator(
               RestTemplateInvoker(
                  SimpleSchemaProvider(schema),
                  WebClient.builder(),
                  AuthWebClientCustomizer.empty(),
               ),
               LocalCachingInvokerProvider.default()
            )
         )
      }

      http4kServer = routes(
         "/books" bind GET to { Response(OK).body(""" [ { "title" : "How to get taller" , "authorId" : 2 }, { "title" : "How to get taller" , "authorId" : 2 }, { "title" : "How to get taller" , "authorId" : 2 } ]""") },
         "/authors/2" bind GET to { Response(OK).body("""{ "id" : 2 , "name" : "Jimmy" }""") }
      ).asServer(Netty(randomPort)).start()

      setupTestService(vyne, null, historyDbWriter)

      val id = UUID.randomUUID().toString()

      val query = "find { Book[] }"
      var results = mutableListOf<ValueWithTypeName>()
      runTest {

         withTurbineTimeout(10.seconds) {
            val turbine =
               queryService.submitVyneQlQueryStreamingResponse(
                  query,
                  clientQueryId = id,
                  resultMode = ResultMode.TYPED
               ).testIn(this)

            // Capture 3 results.
            results.add(turbine.awaitItem() as ValueWithTypeName)
            results.add(turbine.awaitItem() as ValueWithTypeName)
            results.add(turbine.awaitItem() as ValueWithTypeName)
            turbine.awaitComplete()
         }
      }

      // Check the lineage on the results
      runBlocking {
         val output = vyne.query(query).typedObjects().first()
         val authorName = output["authorName"]
         authorName.value!!.should.equal("Jimmy")
         authorName.source.should.be.instanceof(OperationResultReference::class.java)
      }

      val callable = ConditionCallable {
         historyService.getQueryProfileDataFromClientId(id)
      }
      await().atMost(com.jayway.awaitility.Duration.TEN_SECONDS).until<Boolean>(callable)

      callable.result!!.block().remoteCalls.size == 2
      // Should have rich lineage around the null value
      val firstRecordNodeDetail =
         historyService.getNodeDetail(results[0].queryId!!, results[0].valueId, "authorName").block()
      val secondRecordNodeDetail =
         historyService.getNodeDetail(results[1].queryId!!, results[1].valueId, "authorName").block()
      firstRecordNodeDetail.should.equal(secondRecordNodeDetail)
      firstRecordNodeDetail.source.should.not.be.empty

      historyService.listHistory().test()
         .expectNextMatches { querySummary ->
            querySummary.recordCount == 3 && querySummary.responseStatus == QueryResponse.ResponseStatus.COMPLETED
         }.verifyComplete()
   }

   @Test
   fun `large result set performance test`(): Unit = runBlocking {
      // Set this locally something larger than 5000
      val recordCount = 5

      val vyne = com.orbitalhq.spring.invokers.testVyne(
         """
         model Movie {
            @Id id : MovieId inherits Int
            title : MovieTitle inherits String
            director : DirectorId inherits Int
            producer : ProducerId  inherits Int
         }
         model Director {
            @Id id : DirectorId
            name : DirectorName inherits String
         }
         model ProductionCompany {
            @Id id : ProducerId
            name :  ProductionCompanyName inherits String
            country : CountryId inherits Int
         }
         model Country {
            @Id id : CountryId
            name :  CountryName inherits String
         }
         model Review {
            rating : MovieRating inherits Int
         }
         service Service {
            @HttpOperation(method = "GET", url = "http://localhost:${server.port}/movies")
            operation findMovies():Movie[]

             @HttpOperation(method = "GET", url = "http://localhost:${server.port}/directors/{id}")
            operation findDirector(@PathVariable("id") id : DirectorId):Director

            @HttpOperation(method = "GET", url = "http://localhost:${server.port}/producers/{id}")
            operation findProducer(@PathVariable("id") id : ProducerId):ProductionCompany

              @HttpOperation(method = "GET", url = "http://localhost:${server.port}/countries/{id}")
            operation findCountry(@PathVariable("id") id : CountryId):Country

             @HttpOperation(method = "GET", url = "http://localhost:${server.port}/ratings")
            operation findRating():Review
         }
      """, Invoker.RestTemplateWithCache
      )
      val jackson = jacksonObjectMapper()
      val directors = (0 until 5).map { mapOf("id" to it, "name" to "Steven ${it}berg") }
      val producers = (0 until 5).map { mapOf("id" to it, "name" to "$it Studios", "country" to it) }
      val countries = (0 until 5).map { mapOf("id" to it, "name" to "Northern $it") }

      val movies = (0 until recordCount).map {
         mapOf(
            "id" to it,
            "title" to "Rocky $it",
            "director" to directors.random()["id"],
            "producer" to producers.random()["id"]
         )
      }

      Benchmark.benchmark("Heavy load", warmup = 2, iterations = 5) {
         runBlocking {
            setupTestService(vyne, null, historyDbWriter)
            val invokedPaths = ConcurrentHashMap<String, Int>()
            server.prepareResponse(
               invokedPaths,
               "/movies" to response(jackson.writeValueAsString(movies)),
               "/directors" to respondWith { path ->
                  val directorId = path.split("/").last().toInt()
                  jackson.writeValueAsString(directors[directorId])
               },
               "/producers" to respondWith { path ->
                  val producerId = path.split("/").last().toInt()
                  jackson.writeValueAsString(producers[producerId])
               },
               "/countries" to respondWith { path ->
                  val id = path.split("/").last().toInt()
                  jackson.writeValueAsString(countries[id])
               },
               "/ratings" to response(jackson.writeValueAsString(mapOf("rating" to 5)))
            )

            val query = """find { Movie[] } as {
         title : MovieTitle
         director : DirectorName
         producer : ProductionCompanyName
         rating : MovieRating
         country : CountryName
         }[]
      """
            val clientQueryId = UUID.randomUUID().toString()
            val result =
               queryService.submitVyneQlQueryStreamingResponse(query, clientQueryId = clientQueryId, resultMode = ResultMode.TYPED)
                  .toList()
            result.should.have.size(recordCount)
         }

      }

      val stats = StrategyPerformanceProfiler.summarizeAndReset().sortedByCostDesc()
      logger.warn("Perf test of $recordCount completed")
      logger.warn("Stats:\n ${jackson.writerWithDefaultPrettyPrinter().writeValueAsString(stats)}")
   }

   @Test
   fun `large result set performance test with cancellation`(): Unit = runBlocking {
      // Set this locally something larger than 5000
      val recordCount = 50

      val vyne = com.orbitalhq.spring.invokers.testVyne(
         """
         model Movie {
            @Id id : MovieId inherits Int
            title : MovieTitle inherits String
            director : DirectorId inherits Int
            producer : ProducerId  inherits Int
         }
         model Director {
            @Id id : DirectorId
            name : DirectorName inherits String
         }
         model ProductionCompany {
            @Id id : ProducerId
            name :  ProductionCompanyName inherits String
            country : CountryId inherits Int
         }
         model Country {
            @Id id : CountryId
            name :  CountryName inherits String
         }
         model Review {
            rating : MovieRating inherits Int
         }
         service Service {
            @HttpOperation(method = "GET", url = "http://localhost:${server.port}/movies")
            operation findMovies():Movie[]

             @HttpOperation(method = "GET", url = "http://localhost:${server.port}/directors/{id}")
            operation findDirector(@PathVariable("id") id : DirectorId):Director

            @HttpOperation(method = "GET", url = "http://localhost:${server.port}/producers/{id}")
            operation findProducer(@PathVariable("id") id : ProducerId):ProductionCompany

              @HttpOperation(method = "GET", url = "http://localhost:${server.port}/countries/{id}")
            operation findCountry(@PathVariable("id") id : CountryId):Country

             @HttpOperation(method = "GET", url = "http://localhost:${server.port}/ratings")
            operation findRating():Review
         }
      """, Invoker.RestTemplateWithCache
      )
      val jackson = jacksonObjectMapper()
      val directors = (0 until 5).map { mapOf("id" to it, "name" to "Steven ${it}berg") }
      val producers = (0 until 5).map { mapOf("id" to it, "name" to "$it Studios", "country" to it) }
      val countries = (0 until 5).map { mapOf("id" to it, "name" to "Northern $it") }

      val movies = (0 until recordCount).map {
         mapOf(
            "id" to it,
            "title" to "Rocky $it",
            "director" to directors.random()["id"],
            "producer" to producers.random()["id"]
         )
      }
      var result = mutableListOf<ValueWithTypeName>()

      runBlocking(Job()) {
         setupTestService(vyne, null, historyDbWriter)
         val invokedPaths = ConcurrentHashMap<String, Int>()
         server.prepareResponse(
            invokedPaths,
            "/movies" to response(jackson.writeValueAsString(movies)),
            "/directors" to respondWith { path ->
               val directorId = path.split("/").last().toInt()
               jackson.writeValueAsString(directors[directorId])
            },
            "/producers" to respondWith { path ->
               val producerId = path.split("/").last().toInt()
               jackson.writeValueAsString(producers[producerId])
            },
            "/countries" to respondWith { path ->
               val id = path.split("/").last().toInt()
               jackson.writeValueAsString(countries[id])
            },
            "/ratings" to response(jackson.writeValueAsString(mapOf("rating" to 5)))
         )

         val query = """find { Movie[] } as {
         title : MovieTitle
         director : DirectorName
         producer : ProductionCompanyName
         rating : MovieRating
         country : CountryName
         }[]
      """
         var cancelSent = false
         val clientQueryId = UUID.randomUUID().toString()

         launch(Dispatchers.Default) {
            queryService.submitVyneQlQueryStreamingResponse(query, clientQueryId = clientQueryId, resultMode = ResultMode.TYPED)
               .onEach {
                  result.add(it as ValueWithTypeName)
                  if (!cancelSent) {
                     launch {
                        queryService.activeQueryMonitor.cancelQueryByClientQueryId(clientQueryId)
                     }

                     cancelSent = true
                  }
               }
               .toList()
         }
      }



      logger.warn { "RESULT SIZE ${result.size}" }
      result.size.should.above(0)
      result.size.should.below(recordCount)
   }
}
