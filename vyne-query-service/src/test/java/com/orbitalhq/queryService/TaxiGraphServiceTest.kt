package com.orbitalhq.queryService

import com.orbitalhq.VyneCacheConfiguration
import com.orbitalhq.cockpit.core.schemas.TaxiGraphService
import com.orbitalhq.cockpit.core.schemas.TypeLineageService
import com.orbitalhq.schema.api.SimpleSchemaProvider
import com.orbitalhq.schemas.taxi.TaxiSchema
import kotlinx.coroutines.runBlocking
import org.apache.commons.io.IOUtils
import org.junit.Test


class TaxiGraphServiceTest {

   val taxi = """
       type Author {
         firstName : FirstName inherits String
         lastName : LastName inherits String
       }
       type Book {
         author : Author
      }
      service AuthorService {
         operation lookupByName(authorName:FirstName):Author
      }

   """.trimIndent()
   @Test
   fun when_producingTaxiGraphSchema_that_verticesAreFiltered():Unit = runBlocking {
      val fullSchema = IOUtils.toString(this::class.java.getResourceAsStream("/schema.taxi"))
      val schemaProvider = SimpleSchemaProvider(TaxiSchema.from(fullSchema))
      val service = TaxiGraphService(
         schemaProvider, VyneCacheConfiguration.default(),
         TypeLineageService(schemaProvider)
      )
      service.getLinksFromType("com.orbitalhq.ClientJurisdiction")
   }


}
