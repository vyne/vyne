server:
   port: 9022
   compression:
      enabled: true
      mime-types: text/html,text/xml,text/plain,text/css,text/javascript,application/javascript,application/json
      min-response-size: 1024
   error:
      include-message: always
spring:
   application:
      name: query-service
   mvc:
      async:
         request-timeout: 18000000
   main:
      banner-mode: off
      web-application-type: reactive
   codec:
      max-in-memory-size: 100MB
   flyway:
      enabled: true
      locations: classpath:db/migration

   jpa:
      hibernate.ddl-auto: none

      properties:
         hibernate:
            jdbc:
               batch_size: 50
               order_inserts: false

   security:
      oauth2:
         resourceserver:
            jwt:
               jwk-set-uri: ${vyne.security.open-idp.jwks-uri:''}

   cloud.config.enabled: false
vyne:
   newSchemaSubmissionEnabled: true
   data-lineage:
      remoteCalls:
         enabled: false
   mvc:
      executor:
         corePoolSize: 5
         maxPoolSize: 15
         queueCapacity: 50
   graph:
      vyneGraphBuilderCache:
         baseSchemaCacheSize: 100
         graphWithFactTypesCacheSize: 100
         baseSchemaGraphCacheSize: 100
      vyneDiscoverGraphQuery:
         schemaGraphCacheSize: 5
         searchPathExclusionsCacheSize: 200000
   query-service:
      url: http://vyne
   analytics:
      mode: Inprocess
      persistRemoteCallResponses: false
      persistResults: false
      persistRemoteCallMetadata: true
      url: http://vyne-analytics-server
   schema-server:
      url: http://schema-server
   cask:
      url: http://cask
   pipeline-runner:
      url: http://vyne-pipeline-runner

   projection:
      distributionMode: LOCAL # One of [LOCAL | DISTRIBUTED] LOCAL = projections are performed on the instance process the query, DISTRIBUTED = distribute projection work to eligible members
      distributionPacketSize: 100 # Number of TypedInstances in a distributed workload - only applicable where distributionMode = DISTRIBUTED
      distributionRemoteBias: 10 # Number of nodes in the hazelcast cluster before work is preferentially distributed to remote nodes

   hazelcast:
      discovery: MULTICAST # One of aws/multicast/swarm
      memberTag: vyne-query-service # Member tags allow delegation of responsibilities
      eurekaUri: http://127.0.0.1:8761/eureka/


   pipelines:
      kibanaUrl: http://localhost:5601/
      logsIndex: 302a6cb0-040f-11ec-a473-775543ab603f
feign:
   hystrix.enabled: false
   client:
      config:
         default:
            connectTimeout: 600000
            readTimeout: 600000

reactive:
   feign:
      hystrix.enabled: false
      client:
         config:
            default:
               options:
                  connectTimeoutMillis: 600000
                  readTimeoutMillis: 600000


logging:
   level:

      org.springframework: WARN
      com.netflix.discovery.InstanceInfoReplicator: ERROR

      #       These two netflix loggers are REALLY noisy - it logs errors on startup, complaining it can't connect .. generally those errors are ignorable
      #        But, in some cases we may care -- so consider turning this back on.
      com.netflix.discovery.shared.transport.decorator.RedirectingEurekaHttpClient: OFF
      com.netflix.discovery.DiscoveryClient: OFF
      org.apache.http: OFF
      com.netflix: WARN
      com.hazelcast: WARN
      io.vyne: INFO
      io.vyne.query.chat: DEBUG
#       Enable this to get graph debug schemas:
#      io.vyne.query.GraphSearcher: TRACE
#      io.vyne.query: DEBUG
      org.apache.catalina: WARN


management:
   endpoints:
      web:
         exposure:
            include: prometheus,metrics,info,health,logfile,loggers
         base-path: /api/actuator

   endpoint:
      metrics:
         enabled: true
      prometheus:
         enabled: false
      health:
         show-details: always
         show-components: always
   prometheus:
      metrics:
         export:
            enabled: false
   influx:
      metrics:
         export:
            enabled: false
   dynatrace:
      metrics:
         export:
            enabled: false
   datadog:
      metrics:
         export:
            enabled: false
   elastic:
      metrics:
         export:
            enabled: false
   jmx:
      metrics:
         export:
            enabled: false
   graphite:
      metrics:
         export:
            enabled: false
