package com.orbitalhq.queryService

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.orbitalhq.cockpit.core.CockpitCoreConfig
import com.orbitalhq.cockpit.core.CustomSettings
import com.orbitalhq.cockpit.core.FeatureTogglesConfig
import com.orbitalhq.cockpit.core.WebUiUrlSupportFilter
import com.orbitalhq.cockpit.core.lsp.LanguageServerConfig
import com.orbitalhq.cockpit.core.schemas.BuiltInTypesSubmitter
import com.orbitalhq.cockpit.core.security.VyneUserConfig
import com.orbitalhq.cockpit.core.telemetry.TelemetryConfig
import com.orbitalhq.formats.csv.CsvFormatSpec
import com.orbitalhq.history.QueryAnalyticsConfig
import com.orbitalhq.history.db.InProcessHistoryConfiguration
import com.orbitalhq.history.noop.NoopQueryEventConsumerConfiguration
import com.orbitalhq.history.remote.RemoteHistoryConfig
import com.orbitalhq.history.rest.QueryHistoryRestConfig
import com.orbitalhq.licensing.LicenseConfig
import com.orbitalhq.models.format.ModelFormatSpec
import com.orbitalhq.monitoring.EnableCloudMetrics
import com.orbitalhq.pipelines.jet.api.transport.PipelineJacksonModule
import com.orbitalhq.serde.TaxiJacksonModule
import com.orbitalhq.query.runtime.core.EnableVyneQueryNode
import com.orbitalhq.schema.publisher.SchemaPublisherService
import com.orbitalhq.search.embedded.EnableVyneEmbeddedSearch
import com.orbitalhq.spring.EnableVyne
import com.orbitalhq.spring.VyneSchemaConsumer
import com.orbitalhq.spring.VyneSchemaPublisher
import com.orbitalhq.spring.config.DiscoveryClientConfig
import com.orbitalhq.spring.config.EnvVariablesConfig
import com.orbitalhq.spring.config.VyneSpringCacheConfiguration
import com.orbitalhq.spring.config.VyneSpringHazelcastConfiguration
import com.orbitalhq.spring.config.VyneSpringProjectionConfiguration
import com.orbitalhq.spring.http.auth.HttpAuthConfig
import com.orbitalhq.spring.http.websocket.WebSocketPingConfig
import com.orbitalhq.spring.projection.ApplicationContextProvider
import com.orbitalhq.spring.query.formats.FormatSpecRegistry
import com.orbitalhq.spring.utils.versionOrDev
import com.orbitalhq.utils.log
import io.orbital.station.OrbitalStationConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.Banner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import org.springframework.http.MediaType
import org.springframework.http.codec.EncoderHttpMessageWriter
import org.springframework.http.codec.HttpMessageWriter
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.http.codec.json.KotlinSerializationJsonEncoder
import org.springframework.web.reactive.config.WebFluxConfigurer
import java.util.Collections


@SpringBootApplication(scanBasePackageClasses = [QueryServiceApp::class, OrbitalStationConfig::class])
@EnableConfigurationProperties(
   VyneSpringCacheConfiguration::class,
   LanguageServerConfig::class,
   QueryAnalyticsConfig::class,
   VyneSpringProjectionConfiguration::class,
   VyneSpringHazelcastConfiguration::class,
   VyneUserConfig::class,
   FeatureTogglesConfig::class,
   CustomSettings::class,
   EnvVariablesConfig::class
)
@Import(
   HttpAuthConfig::class,
   ApplicationContextProvider::class,
   LicenseConfig::class,
   DiscoveryClientConfig::class,
   TelemetryConfig::class,
   WebSocketPingConfig::class

)
class QueryServiceApp {

   companion object {
      @JvmStatic
      fun main(args: Array<String>) {
//         DebugProbes.install()
//         DebugProbes.enableCreationStackTraces = false
         val app = SpringApplication(QueryServiceApp::class.java)
         app.setBannerMode(Banner.Mode.OFF)
         app.run(*args)
      }
   }

   @Bean
   fun csvFormatSpec(): ModelFormatSpec = CsvFormatSpec

   @Bean
   fun taxiJacksonModule() = TaxiJacksonModule

   @Bean
   fun jacksonCustomizer(): Jackson2ObjectMapperBuilderCustomizer {
      return Jackson2ObjectMapperBuilderCustomizer { builder ->
         builder.featuresToEnable(
            MapperFeature.DEFAULT_VIEW_INCLUSION
         )
      }
   }


   @Bean
   fun formatSpecRegistry(): FormatSpecRegistry = FormatSpecRegistry.default()


   @Autowired
   fun logInfo(@Autowired(required = false) buildInfo: BuildProperties? = null) {
      log().info("Orbital Query Server v ${buildInfo.versionOrDev()}")
   }
}


@Configuration
@EnableVyne
@VyneSchemaConsumer
@VyneSchemaPublisher
@EnableVyneEmbeddedSearch
@EnableVyneQueryNode
@EnableCloudMetrics
@Import(
   InProcessHistoryConfiguration::class,
   NoopQueryEventConsumerConfiguration::class,
   QueryHistoryRestConfig::class,
   RemoteHistoryConfig::class,
   CockpitCoreConfig::class,
   WebUiUrlSupportFilter::class
)
class VyneConfig {
   @Bean
   fun builtInTypesSubmitter(publisher: SchemaPublisherService): BuiltInTypesSubmitter =
      BuiltInTypesSubmitter(publisher)
}

@Configuration
class PipelineConfig {
   @Bean
   fun pipelineModule(): PipelineJacksonModule = PipelineJacksonModule()
}

@Configuration
class WebFluxWebConfig(private val objectMapper: ObjectMapper) : WebFluxConfigurer {
   override fun configureHttpMessageCodecs(configurer: ServerCodecConfigurer) {
      val defaults: ServerCodecConfigurer.ServerDefaultCodecs = configurer.defaultCodecs()
      defaults.jackson2JsonDecoder(Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON))
      // Spring Boot Admin 2.x
      // checks for the content-type application/vnd.spring-boot.actuator.v2.
      // If this content-type is absent, the application is considered to be a Spring Boot 1 application.
      // Spring Boot Admin can't display the metrics with Metrics are not supported for Spring Boot 1.x applications.
      defaults.jackson2JsonEncoder(
         Jackson2JsonEncoder(
            objectMapper,
            MediaType.APPLICATION_JSON,
            ActuatorV2MediaType,
            ActuatorV3MediaType
         )
      )

   }

   override fun addCorsMappings(registry: org.springframework.web.reactive.config.CorsRegistry) {
      registry.addMapping("/**")
         .allowedOrigins("*")
         .allowedHeaders("*")
         .exposedHeaders("*")
         .allowedMethods("*")

   }


   companion object {
      private val ActuatorV2MediaType = MediaType("application", "vnd.spring-boot.actuator.v2+json")
      private val ActuatorV3MediaType = MediaType("application", "vnd.spring-boot.actuator.v3+json")
   }
}

/**
 * Workaround to Spring 5.3 ordering of codecs, to favour Jacckson over Kotlin
 *
 * In Spring 5.3 it appears the KotlinSerializationJsonEncoder is weighted higher
 * than Jackson2JsonEncoder.
 *
 * This means if we try to return a class that is tagged with Kotlin's @Serializable annotation,
 * Spring will use Kotlin, rather than Jackson.
 *
 * This is undesirable, and causes serialization issues.  We also have a number of custom
 * Jackon serializers written, which we want to use.
 *
 * In Spring Reactive, there's no easy way to modify ordering of Codecs.
 * So, we use this adapter to swap out the order of the codecs, pushing Jackson to the front.
 *
 * https://github.com/spring-projects/spring-framework/issues/28856
 */
@Configuration
class CustomWebFluxConfigSupport {

   @Bean
   @Primary
   fun serverCodecConfigurerAdapter(other: ServerCodecConfigurer): ServerCodecConfigurer {
      return ReOrderingServerCodecConfigurer(other)
   }

   class ReOrderingServerCodecConfigurer(private val configurer: ServerCodecConfigurer) :
      ServerCodecConfigurer by configurer {

      override fun getWriters(): MutableList<HttpMessageWriter<*>> {
         val writers = configurer.writers
         val jacksonWriterIndex =
            configurer.writers.indexOfFirst { it is EncoderHttpMessageWriter && it.encoder is Jackson2JsonEncoder }
         val kotlinSerializationWriterIndex =
            configurer.writers.indexOfFirst { it is EncoderHttpMessageWriter && it.encoder is KotlinSerializationJsonEncoder }

         if (kotlinSerializationWriterIndex == -1 || jacksonWriterIndex == -1) {
            return writers
         }

         if (kotlinSerializationWriterIndex < jacksonWriterIndex) {
            Collections.swap(writers, jacksonWriterIndex, kotlinSerializationWriterIndex)
         }
         return writers
      }
   }
}
