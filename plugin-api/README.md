This is an early implementation of a plugin API for Orbital.

It is unlikely to be the long term strategy / direction. 

It's recommended that you don't build on this.
