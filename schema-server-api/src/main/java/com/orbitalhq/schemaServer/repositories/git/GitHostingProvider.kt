package com.orbitalhq.schemaServer.repositories.git

enum class GitHostingProvider {
   Noop,
   Github,
   Gitlab
}
