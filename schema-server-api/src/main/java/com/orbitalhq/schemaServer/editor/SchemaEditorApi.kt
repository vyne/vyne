package com.orbitalhq.schemaServer.editor

import com.orbitalhq.Message
import com.orbitalhq.PackageIdentifier
import com.orbitalhq.PackageSourceName
import com.orbitalhq.ResultWithMessage
import com.orbitalhq.VersionedSource
import com.orbitalhq.schema.publisher.loaders.Changeset
import com.orbitalhq.schemas.Metadata
import lang.taxi.CompilationMessage


/**
 * Provides the details of repositories that are
 * editable (described by the package Ids of those
 * repositories).
 *
 */
data class EditableRepositoryConfig(
   val editablePackages: List<PackageIdentifier>
) {
   val editingEnabled: Boolean = editablePackages.isNotEmpty()

   // Short term workaround...
   // For now, only support editing of a single package.
   // However, we'll need to allow multiple editable packages,
   // and edit requests will have to tell us which package the
   // edit should go to.

   fun getDefaultEditorPackage(): PackageIdentifier {
      return when (editablePackages.size) {
         0 -> error("There are no packages configured to be editable")
         1 -> editablePackages.single()
         else -> error("There are multiple packages defined as editable.")
      }
   }
}


data class StartChangesetRequest(
   val changesetName: String,
   val packageIdentifier: PackageIdentifier
)

data class AddChangesToChangesetRequest(
   val changesetName: String,
   val packageIdentifier: PackageIdentifier,
   val edits: List<VersionedSource>,
   val deletions: List<PackageSourceName> = emptyList()
)

data class SchemaEditRequest(
   val packageIdentifier: PackageIdentifier,
   val edits: List<VersionedSource>
)

data class FinalizeChangesetRequest(
   val changesetName: String,
   val packageIdentifier: PackageIdentifier
)

data class UpdateChangesetRequest(
   val changesetName: String,
   val newChangesetName: String,
   val packageIdentifier: PackageIdentifier
)

data class GetAvailableChangesetsRequest(
   val packageIdentifier: PackageIdentifier
)

data class SetActiveChangesetRequest(
   val changesetName: String,
   val packageIdentifier: PackageIdentifier
)

data class SchemaEditResponse(
   val success: Boolean,
   val compilationMessages: List<CompilationMessage>,
   override val messages: List<Message>
) : ResultWithMessage


data class UpdateTypeAnnotationRequest(
   val annotations: List<Metadata>,
   val changeset: Changeset
)


/**
 * This shouldn't be part of the schema server, since these are just annotations.
 * But the API for mutating annotations is too compelx to build right now
 */
data class UpdateDataOwnerRequest(
   val id: String,
   val name: String,
   val changeset: Changeset
)

data class SaveQueryRequest(
   val source: VersionedSource,
   val previousPath: PackageSourceName? = null,
   val changesetName: String = "",
)
