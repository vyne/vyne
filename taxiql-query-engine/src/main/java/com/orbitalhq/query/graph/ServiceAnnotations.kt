package com.orbitalhq.query.graph

enum class ServiceAnnotations(val annotation: String) {
   Datasource("Datasource")
}

enum class ServiceParams(val paramName: String) {
   Exclude("exclude")
}
