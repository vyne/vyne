package com.orbitalhq.query.graph.edges

import com.orbitalhq.models.FailedSearch
import com.orbitalhq.models.InPlaceQueryEngine
import com.orbitalhq.models.MixedSources
import com.orbitalhq.models.PermittedQueryStrategies
import com.orbitalhq.models.QueryFailureBehaviour
import com.orbitalhq.models.TypedCollection
import com.orbitalhq.models.TypedInstance
import com.orbitalhq.models.TypedNull
import com.orbitalhq.models.TypedObject
import com.orbitalhq.models.TypedObjectFactory
import com.orbitalhq.models.UndefinedSource
import com.orbitalhq.models.facts.CopyOnWriteFactBag
import com.orbitalhq.models.facts.FactDiscoveryStrategy
import com.orbitalhq.models.facts.ScopedFact
import com.orbitalhq.query.MetricTags
import com.orbitalhq.query.QueryContext
import com.orbitalhq.query.QuerySpecTypeNode
import com.orbitalhq.query.SearchGraphExclusion
import com.orbitalhq.query.TypedInstanceValidPredicate
import com.orbitalhq.query.UnresolvedTypeInQueryException
import com.orbitalhq.query.graph.operationInvocation.UnresolvedOperationParametersException
import com.orbitalhq.schemas.Operation
import com.orbitalhq.schemas.Parameter
import com.orbitalhq.schemas.RemoteOperation
import com.orbitalhq.schemas.Type
import com.orbitalhq.utils.log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.runBlocking
import lang.taxi.expressions.Expression
import lang.taxi.services.operations.constraints.Constraint
import lang.taxi.types.PrimitiveType
import java.util.concurrent.CopyOnWriteArrayList

class ParameterFactory {
   suspend fun discoverAll(operation: Operation, context: QueryContext): List<Pair<Parameter, TypedInstance>> {
      return operation.parameters.map { param ->
         param to discover(
            param.type,
            context,
            null,
            operation,
            defaultValue = param.defaultValue,
            param.nullable
         )
      }
   }


   suspend fun discover(
      paramType: Type,
      context: QueryContext,
      candidateValue: TypedInstance? = null,
      operation: RemoteOperation? = null,
      defaultValue: Expression? = null,
      nullable: Boolean
   ): TypedInstance {
      // First, search only the top level for facts
      val firstLevelDiscovery = context.getFactOrNull(paramType, strategy = FactDiscoveryStrategy.TOP_LEVEL_ONLY)
      if (hasValue(firstLevelDiscovery)) {
         // TODO (1) : Find an instance that is linked, somehow, rather than just something random
         // TODO (2) : Fail if there are multiple instances
         return firstLevelDiscovery!!
      }

      // Check to see if there's exactly one instance somewhere within the context
      // Note : I'm cheating here, I really should find the value required by retracing steps
      // walked in the path.  But, it's unclear if this is possible, given the scattered way that
      // the algorithims are evaluated
      val anyDepthOneDistinct =
         context.getFactOrNull(paramType, strategy = FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE_DISTINCT)
      if (hasValue(anyDepthOneDistinct)) {
         // TODO (1) : Find an instance that is linked, somehow, rather than just something random
         return anyDepthOneDistinct!!
      }

      if (defaultValue != null) {
         val evaluatedDefaultValue = context.evaluate(defaultValue)
         return evaluatedDefaultValue
      }

      if (nullable) {
         return TypedNull.create(paramType)
      }

      if (!canConstructType(paramType)) {
         throw UnresolvedOperationParametersException(
            "No instance of type ${paramType.name} is present in the graph, and the type is not a parameter type, so cannot be constructed. ",
            context.evaluatedPath(),
            context.profiler.root,
            emptyList()
         )
      }

      // We're allowed to try and build the input type, so pick the right strategy...
      return if (paramType.isCollection) {
         attemptToConstructCollection(paramType, context, operation, candidateValue)
      } else {
         attemptToConstruct(paramType, context, operation, candidateValue)
      }

   }

   private suspend fun attemptToConstructCollection(
      paramType: Type,
      context: QueryContext,
      operation: RemoteOperation?,
      candidateValue: TypedInstance?
   ): TypedInstance {
//      val fromCollectionBuilder = CollectionBuilder(context.queryEngine, context)
//         .build(paramType, AlwaysGoodSpec)
//
//
//      if (fromCollectionBuilder != null && !TypedNull.isFailedSearch(fromCollectionBuilder)) {
//         return fromCollectionBuilder
//      }

      // experiment: Are there any collections in the context we can iterate?
      val builtFromCollection = context.facts.filter { it.type.isCollection }
         .asSequence()
         .mapNotNull { collection: TypedInstance ->
            require(collection is TypedCollection) { "Expected to recieve a TypedCollection" }
            var exceptionThrown = false
            val built = collection
               .takeWhile { !exceptionThrown }
               .mapNotNull { member ->
                  runBlocking {
                     try {
                        val memberOnlyQueryContext = context.only(member)
                        val builtFromMember =
                           attemptToConstruct(paramType.collectionType!!, memberOnlyQueryContext, operation, member)
                        builtFromMember
                     } catch (e: UnresolvedOperationParametersException) {
                        exceptionThrown = true
                        null
                     }
                  }
               }
            if (exceptionThrown) {
               null
            } else {
               built
            }
         }
         .firstOrNull()
      if (builtFromCollection != null) {
         return TypedCollection.from(builtFromCollection, MixedSources)
      } else {
         throw UnresolvedOperationParametersException(
            "Unable to construct instance of type ${paramType.paramaterizedName}, as was unable to find a way to construct it's collection members from the facts known",
            context.evaluatedPath(),
            context.profiler.root,
            emptyList()
         )
      }
   }

   private fun canConstructType(paramType: Type): Boolean {
      return when {
         paramType.isPrimitive -> false // Don't attempt to construct a primitive type
         paramType.isScalar -> true
         paramType.isParameterType -> true
         paramType.isCollection -> canConstructType(paramType.collectionType!!)
         else -> false
      }
   }

   private fun hasValue(instance: TypedInstance?): Boolean {
      return when {
         instance == null -> false
         instance is TypedNull -> false
         // This is a big call, treating empty strings as not populated
         // It's probably the wrong call.
         // But we need to consider how to filter this data upstream from providers,
         // and allow that filtering to be configurable.
         // Adding this here now because it's caused a bug at a client, let's
         // revisit if/when it becomes problematic.
         instance.type.taxiType.basePrimitive == PrimitiveType.STRING && instance.value == "" -> false
         else -> true
      }
   }

   private suspend fun attemptToConstruct(
      paramType: Type,
      context: QueryContext,
      operation: RemoteOperation?,
      candidateValue: TypedInstance? = null,
      typesCurrentlyUnderConstruction: Set<Type> = emptySet()
   ): TypedInstance {
      // MP 6-Dec-23: The original impl. (below), duplicates much of the logic factored into the TypedObjectFactory.
      // This means things like expression evaluation are handled inconsistently.
      // I don't believe there's a reason for handling this evaluation seperately.
      // Adding logging to debug and track down

      // When searching to construct a parameter for an operation, exclude the operation itself.
      // Otherwise, if an operation result includes an input parameter, we can end up in a recursive loop, trying to
      // construct a request for the operation to discover a parameter needed to construct a request for the operation.
      val queryContextWithOperationExclusion = QueryContextWithOperationExclusion(context, operation)

      val built = TypedObjectFactory(
         paramType,
         context.facts,
         context.schema,
         source = UndefinedSource,
         inPlaceQueryEngine = queryContextWithOperationExclusion,
         constructClosedParameterTypes = true
      ).build()
      if (hasValue(built)) {
         return built
      }


      // Legacy impl. below. Need to understand if there's any differences here, than from what (is / would be / should be) returned
      // from the TypedObjectFactory

      // We need to understand if this can happen, and if further attempts below will yield a different result
      log().warn("Parameter construction via TypedObjectFactory failed. Investigate scenario to understand")

      require(!paramType.isCollection) {
         "This method is intended for building objects, not collections."
      }

      val fields = paramType.attributes.map { (attributeName, field) ->
         val attributeType = context.schema.type(field.type.fullyQualifiedName)

         var attributeValue: TypedInstance? =
            context.getFactOrNull(attributeType, FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE_DISTINCT)

         /**
          *  see com.orbitalhq.query.graph.ParameterTypeTest for the below if block.
          */
         if (attributeValue is TypedNull && candidateValue != null) {
            val factBag = CopyOnWriteFactBag(CopyOnWriteArrayList(setOf(candidateValue)), context.schema)
            attributeValue =
               context.copy(facts = factBag).getFactOrNull(
                  attributeType,
                  FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE_DISTINCT
               ) ?: attributeValue
         }

         // First, look in the context to see if it's there.
         if (attributeValue == null) {
            // ... if not, try and find the value in the graph
            // When searching to construct a parameter for an operation, exclude the operation itself.
            // Otherwise, if an operation result includes an input parameter, we can end up in a recursive loop, trying to
            // construct a request for the operation to discover a parameter needed to construct a request for the operation.
            val excludedOperations = excludeOperationFromSearch(operation)
            val queryResult = try {
               context.find(QuerySpecTypeNode(attributeType, expression = null), excludedOperations)
                  .results.firstOrNull()
            } catch (e: UnresolvedTypeInQueryException) {
               null
            }
            if (queryResult != null) {
               attributeValue = queryResult
            } else {
               // ... finally, try constructing the value...
               if (!attributeType.isScalar && !typesCurrentlyUnderConstruction.contains(attributeType)) {
                  log().debug(
                     "Parameter of type {} not present within the context.  Attempting to construct one.",
                     attributeType.name.fullyQualifiedName
                  )
                  val constructedType = attemptToConstruct(
                     attributeType,
                     context,
                     typesCurrentlyUnderConstruction = typesCurrentlyUnderConstruction + attributeType,
                     operation = operation
                  )
                  log().debug(
                     "Parameter of type {} constructed: {}",
                     constructedType,
                     attributeType.name.fullyQualifiedName
                  )
                  attributeValue = constructedType
               }
            }
         }

         when {
            attributeValue is TypedNull && !field.nullable -> {
               throw UnresolvedOperationParametersException(
                  "Unable to construct instance of type ${paramType.name}, as field $attributeName (of type ${attributeType.name}) is not present within the context, and is not constructable ",
                  context.evaluatedPath(),
                  context.profiler.root,
                  attributeValue.source.failedAttempts
               )
            }

            (attributeValue == null && !field.nullable) -> {
               throw UnresolvedOperationParametersException(
                  "Unable to construct instance of type ${paramType.name}, as field $attributeName (of type ${attributeType.name}) is not present within the context, and is not constructable ",
                  context.evaluatedPath(),
                  context.profiler.root,
                  emptyList()
               )
            }

            attributeValue == null && field.nullable -> {
               attributeName to TypedNull.create(
                  field.resolveType(context.schema),
                  FailedSearch("Could not find an instance when constructing a parameter")
               )
            }

            attributeValue == null -> error("This shouldn't be possible, but the compiler thinks it is")
            else -> attributeName to attributeValue
         }
      }.toMap()
      // We need a composite data source here, which contains
      // all the data sources/
      // Don't have one, and building one right now is too expensive.

      val dataSource = MixedSources.singleSourceOrMixedSources(fields.map { it.value })
      return TypedObject(paramType, fields, dataSource)
   }

   private fun excludeOperationFromSearch(operation: RemoteOperation?): Set<SearchGraphExclusion<RemoteOperation>> {
      val excludedOperations = operation?.let {
         setOf(
            SearchGraphExclusion(
               "Operation is excluded as we're searching for an input for it",
               it
            )
         )
      } ?: emptySet()
      return excludedOperations
   }

}

/**
 * A special InPlaceQueryEngine, which ensures that the specified operation is excluded from any searches.
 *
 * Otherwise, if an operation result includes an input parameter, we can end up in a recursive loop, trying to
 * construct a request for the operation to discover a parameter needed to construct a request for the operation.
 */
private class QueryContextWithOperationExclusion(
   private val context: QueryContext,
   private val operation: RemoteOperation?
) : InPlaceQueryEngine by context {
   private val excludedOperations = operation?.let {
      setOf(
         SearchGraphExclusion(
            "Operation is excluded as we're searching for an input for it",
            it
         )
      )
   } ?: emptySet()

   override fun withAdditionalFacts(facts: List<TypedInstance>, scopedFacts: List<ScopedFact>): InPlaceQueryEngine {
      return QueryContextWithOperationExclusion(context.withAdditionalFacts(facts, scopedFacts), operation)
   }

   override fun only(
      facts: List<TypedInstance>,
      scopedFacts: List<ScopedFact>,
      inheritParent: Boolean
   ): InPlaceQueryEngine {
      return QueryContextWithOperationExclusion(context.only(facts, scopedFacts, inheritParent), operation)
   }

   override suspend fun findType(
      type: Type,
      spec: TypedInstanceValidPredicate,
      permittedStrategy: PermittedQueryStrategies,
      failureBehaviour: QueryFailureBehaviour,
      constraint: List<Constraint>
   ): Flow<TypedInstance> {
      // This method is the same as the default implementation.
      // However, because of how InPlaceQueryEngine is implemented via delegation to context,
      // if we don't override here, then the value of "this" becomes the delegated context,
      // so the next call to findType() goes to the delegated context, rather than this instance,
      // and we lose the ability to inject our excluded operations.
      return this.findType(type, permittedStrategy, failureBehaviour, constraint)
         .filter { spec.isValid(it) }
   }

   override suspend fun findType(
      type: Type,
      permittedStrategy: PermittedQueryStrategies,
      failureBehaviour: QueryFailureBehaviour,
      constraints: List<Constraint>
   ): Flow<TypedInstance> {
      return context.find(QuerySpecTypeNode(type = type,
         dataConstraints = constraints), excludedOperations, failureBehaviour, MetricTags.NONE)
         .results
   }

}
