package com.orbitalhq.query

import com.orbitalhq.models.TypedInstance
import com.orbitalhq.schemas.Type
import lang.taxi.query.TaxiQLQueryString
import java.time.Instant

interface QueryEventConsumer : RemoteCallOperationResultHandler {
   fun handleEvent(event: QueryEvent)
   fun shutdown() {}
}

sealed class QueryEvent

data class RestfulQueryResultEvent(
   val query: Query,
   override val queryId: String,
   override val clientQueryId: String?,
   override val typedInstance: TypedInstance,
   override val queryStartTime: Instant
) : QueryResultEvent, QueryEvent() {
   override val anonymousTypes: Set<Type> = emptySet()
}

data class QueryFailureEvent(
   val queryId: String,
   val clientQueryId: String?,
   val failure: FailedQueryResponse
) : QueryEvent()

data class TaxiQlQueryResultEvent(
   val query: TaxiQLQueryString,
   override val queryId: String,
   override val clientQueryId: String?,
   override val typedInstance: TypedInstance,
   override val anonymousTypes: Set<Type>,
   override val queryStartTime: Instant
) : QueryResultEvent, QueryEvent()

interface QueryResultEvent {
   val queryId: String
   val clientQueryId: String?
   val typedInstance: TypedInstance
   val anonymousTypes: Set<Type>

   // We need the queryStartTime as we create the query record on the first emitted
   // result.
   val queryStartTime: Instant
}

data class QueryCompletedEvent(
   val queryId: String,
   val timestamp: Instant,
   val query: TaxiQLQueryString,
   val clientQueryId: String?,
   val message: String,
   val recordCount: Int = 0
) : QueryEvent()

data class TaxiQlQueryExceptionEvent(
   val query: TaxiQLQueryString,
   val queryId: String,
   val clientQueryId: String?,
   val timestamp: Instant,
   val message: String,
   val queryStartTime: Instant,
   val recordCount: Int = 0
) : QueryEvent()

data class StreamingQueryCancelledEvent(val query: TaxiQLQueryString,
                                        val queryId: String,
                                        val clientQueryId: String?,
                                        val timestamp: Instant,
                                        val message: String,
                                        val queryStartTime: Instant,
                                        val recordCount: Int = 0
) : QueryEvent()

data class RestfulQueryExceptionEvent(
   val query: Query,
   val queryId: String,
   val clientQueryId: String?,
   val timestamp: Instant,
   val message: String,
   val queryStartTime: Instant,
   val recordCount: Int = 0
) : QueryEvent()

data class QueryStartEvent(
   val queryId: String,
   val timestamp: Instant,
   val taxiQuery: TaxiQLQueryString?,
   val query: Query?,
   val clientQueryId: String,
   val message: String,
   val anonymousTypes: Set<Type>
) : QueryEvent()

