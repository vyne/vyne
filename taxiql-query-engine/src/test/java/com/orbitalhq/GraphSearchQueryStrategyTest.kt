package com.orbitalhq

import app.cash.turbine.test
import app.cash.turbine.testIn
import com.winterbe.expekt.should
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import com.orbitalhq.models.TypedInstance
import com.orbitalhq.models.json.parseJson
import com.orbitalhq.models.json.parseJsonModel
import com.orbitalhq.query.connectors.OperationResponseHandler
import com.orbitalhq.schemas.Parameter
import com.orbitalhq.schemas.RemoteOperation
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Test
import java.math.BigDecimal
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class GraphSearchQueryStrategyTest {
   @Test
   fun `Discover required type from a service returning child type of required type`() = runBlocking {
      val schema = """
         type Isin inherits String
         type NotionalValue inherits Decimal
         type InstrumentNotionalValue inherits NotionalValue
         model Input {
            isin: Isin
         }

         model Output {
            notionalValue: NotionalValue
         }

         model Instrument {
             instrumentNotionalValue: InstrumentNotionalValue
         }

         @DataSource
         service HelperService {
            operation `findAll`( ) : Input[]

         }

         service InstrumentService  {
             operation getInstrument( isin : Isin) : Instrument
         }
         """.trimIndent()
      val (vyne, stubService) = testVyne(schema)
      stubService.addResponse(
         "`findAll`", vyne.parseJsonModel(
            "Input[]", """
         [
            {  "isin": "isin1"}
         ]
         """.trimIndent()
         )
      )

      stubService.addResponse(
         "getInstrument", vyne.parseJsonModel(
            "Instrument",
            """
              {"instrumentNotionalValue": 100}
         """.trimIndent()
         )
      )

      val result = vyne.query(
         """find { Input[] } as Output[]""".trimIndent()
      ).firstRawObject()
      result.shouldBe(mapOf("notionalValue" to BigDecimal("100")))
   }

   @Test
   fun `Should not discover required type from relevant service return parent of required type`() = runBlocking {
      val schema = """
         type Isin inherits String
         type NotionalValue inherits Decimal
         type InstrumentNotionalValue inherits NotionalValue
         model Input {
            isin: Isin
         }

         model Output {
            notionalValue: InstrumentNotionalValue
         }

         model Instrument {
             instrumentNotionalValue: NotionalValue
         }

         @DataSource
         service HelperService {
            operation `findAll`( ) : Input[]

         }

         service InstrumentService  {
             operation getInstrument( isin : Isin) : Instrument
         }
         """.trimIndent()
      val (vyne, stubService) = testVyne(schema)
      stubService.addResponse(
         "`findAll`", vyne.parseJsonModel(
            "Input[]", """
         [
            {  "isin": "isin1"}
         ]
         """.trimIndent()
         )
      )

      stubService.addResponse(
         "getInstrument", vyne.parseJsonModel(
            "Instrument",
            """
              {"instrumentNotionalValue": 100}
         """.trimIndent()
         )
      )

      val result = vyne.query(
         """
            find {
                Input[]
              } as Output[]
            """.trimIndent()
      )

      result.rawResults.test {
         val expected = mapOf("notionalValue" to null)
         expectRawMap().should.equal(expected)
         awaitComplete()
      }
   }

   @Test
      /**
       * Our expectation is set the notionalValue field of Output to 100
       * so, we're looking for a value of NotionalValue
       *
       * There are multiple paths in tests
       *
       * Path 1: Fails as InstrumentService@@getNotional  always throws.
       * Input::isin -> NotionalValueRequest -> InstrumentService@@getNotional
       *
       * Path 2: Fails as ReferenceDataService@@keyToValue is invoked with 'null' value.
       * Input::StrategyId -> StrategyService@@getStrategy -> Strategy::notionalValueKey -> ReferenceDataService@@keyToValue
       *
       * Path 3: Fails as  ReferenceDataService@@keyToValue throws for NationalValueKey = 2
       * Input/tradeId ->  TradeDataService@@getTradeData -> TradeData/notionalValueKey -> ReferenceDataService@@keyToValue
       *
       * Path 4: Is the successful Path
       * Input::isin -> ReferenceDataService@getReferenceData -> ReferenceData/notionalValueKey -> ReferenceDataService@@keyToValue
       */
   fun `Discover correct path when there are service failures in discovered paths`() = runBlocking {
      val schema = """
      type Isin inherits String
      type NotionalValueKey inherits Int
      type NotionalValue inherits Decimal
      type StrategyId inherits String
      type TradeId inherits String
      model Input {
         isin: Isin
         strategyId: StrategyId
         tradeId: TradeId
      }

      parameter model NotionalValueRequest {
        isin: Isin
      }

      model NotionalValueResponse {
        notionalValue: NotionalValue
      }

      model Output {
         notionalValue: NotionalValue
      }

      model Instrument {
          instrumentNotionalValue: NotionalValue
      }

      model Strategy {
         notionalValueKey: NotionalValueKey?
      }

      model ReferenceData {
         notionalValueKey: NotionalValueKey
      }

      model NotionalKeyAndValue {
         key: NotionalValueKey
         value: NotionalValue
      }

      model TradeData {
          tradeId: TradeId
          notionalValueKey: NotionalValueKey
      }

      @DataSource
      service HelperService {
         operation `findAll`( ) : Input[]

      }

      service TradeDataService {
         operation getTradeData(tradeId: TradeId): TradeData
      }

      service StrategyService {
         operation getStrategy(id: StrategyId): Strategy
      }

      service InstrumentService  {
          operation getNotional( request : NotionalValueRequest) : NotionalValueResponse
      }

      service ReferenceDataService {
          operation getReferenceData( isin: Isin): ReferenceData
          operation keyToValue(key: NotionalValueKey): NotionalKeyAndValue
      }
      """.trimIndent()
      val (vyne, stubService) = testVyne(schema)
      stubService.addResponse(
         "`findAll`", vyne.parseJson(
            "Input[]", """
      [
         {  "isin": "isin1", "strategyId": 1, "tradeId": "trade123"}
      ]
      """.trimIndent()
         )
      )

      // this is invoked for
      // Input::ISIN -> NotionalValueRequest -> getNotional -> NotionalValueResponse::NotionalValue
      // we fail on getNotional operation.
      val getNotionalHandler: OperationResponseHandler =
         { _: RemoteOperation, _: List<Pair<Parameter, TypedInstance>> ->
            throw IllegalArgumentException("")
         }

      stubService.addResponse("getNotional", getNotionalHandler)

      // this is invoked for
      // Input::TradeId -> getTradeData -> TradeData::NotionalValueKey -> keyToValue operation
      // note that we return notionalValueKey value as '2' for which keyToValue function will throw exception (see below)
      stubService.addResponse(
         "getTradeData",
         vyne.parseJson(
            "TradeData",
            """
           {"tradeId": "trade123", "notionalValueKey": 2}
      """.trimIndent()
         )
      )

      // Below invoked through Input::StrategyId -> getStrategy -> Strategy::NotionalValueKey -> keyToValue path.
      // operation returns successfully, but the strategy id is null, so the path can't succeed.
      stubService.addResponse(
         "getStrategy",
         vyne.parseJson(
            "Strategy",
            """
           {"id": null}
      """.trimIndent()
         )
      )

      // This supposed to be invoked through
      // Input::Isin -> getReferenceData operation -> ReferenceData::NotionalValueKey -> keyToValue operation -> NotionalKeyAndValue::NotionalValue
      val getReferenceDataHandler: OperationResponseHandler =
         { _: RemoteOperation, _: List<Pair<Parameter, TypedInstance>> ->
            listOf(
               vyne.parseJson(
                  "ReferenceData",
                  """
           {"notionalValueKey": 1}
      """.trimIndent()
               )
            )
         }
      stubService.addResponse("getReferenceData", getReferenceDataHandler)

      val keyToValueHandler: OperationResponseHandler =
         { _: RemoteOperation, params: List<Pair<Parameter, TypedInstance>> ->
            // when the parameter value is 2, we arrive here through the tradeId attribute of the 'Input'
            // from tradeId we call getTradeData to fetch the TradeData which has a NotionalValueKey property.
            // we fail here to push the discovery to find
            // Input::Isin -> getReferenceData -> ReferenceData::NotionalValueKey -> keyToValue
            if (params.first().second.value == 2) {
               throw IllegalArgumentException("2")
            }

            if (params.first().second.value == null) {
               throw IllegalArgumentException("null key value")
            }
            listOf(
               vyne.parseJsonModel(
                  "NotionalKeyAndValue",
                  """
           {"key": 1, "value": 100}
      """.trimIndent()
               )
            )
         }

      stubService.addResponse("keyToValue", keyToValueHandler)

      val result = vyne.query(
         """
         find {
             Input[]
           } as Output[]
         """.trimIndent()
      )
      runTest {
         val turbine = result.rawResults.testIn(this)
         turbine.expectRawMap().should.equal(mapOf("notionalValue" to BigDecimal("100")))
         turbine.awaitComplete()
      }
   }

   @Test
      /**
       * Our expectation is set the notionalValue field of Output to 100
       * so, we're looking for a value of NotionalValue
       *
       * There are multiple paths in tests
       *
       * Path 1: Fails as InstrumentService@@getNotional  always throws.
       * Input::isin -> NotionalValueRequest -> InstrumentService@@getNotional
       *
       * Path 2: Fails as ReferenceDataService@@keyToValue is invoked with 'null' value.
       * Input::StrategyId -> StrategyService@@getStrategy -> Strategy::notionalValueKey -> ReferenceDataService@@keyToValue
       *
       * Path 3: Fails as  TradeData notionalValue returns null.
       * Input/tradeId ->  TradeDataService@@getTradeData -> TradeData/notionalValue
       *
       * Path 4: Is the successful Path
       * Input::isin -> ReferenceDataService@getReferenceData -> ReferenceData/notionalValueKey -> ReferenceDataService@@keyToValue
       */
   fun `Discover correct path when there are service failures in discovered paths for a FirstNotEmpty attribute`() =
      runBlocking {
         val schema = """
      type Isin inherits String
      type NotionalValueKey inherits Int
      type NotionalValue inherits Decimal
      type StrategyId inherits String
      type TradeId inherits String
      model Input {
         isin: Isin
         strategyId: StrategyId
         tradeId: TradeId
      }

      parameter model NotionalValueRequest {
        isin: Isin
      }

      model NotionalValueResponse {
        notionalValue: NotionalValue
      }

      model Output {
         @FirstNotEmpty
         notionalValue: NotionalValue
      }

      model Instrument {
          instrumentNotionalValue: NotionalValue
      }

      model Strategy {
         notionalValueKey: NotionalValueKey?
      }

      model ReferenceData {
         notionalValueKey: NotionalValueKey
      }

      model NotionalKeyAndValue {
         key: NotionalValueKey
         value: NotionalValue
      }

      model TradeData {
          tradeId: TradeId
          notionalValue: NotionalValue?
      }

      @DataSource
      service HelperService {
         operation `findAll`( ) : Input[]

      }

      service TradeDataService {
         operation getTradeData(tradeId: TradeId): TradeData
      }

      service StrategyService {
         operation getStrategy(id: StrategyId): Strategy
      }

      service InstrumentService  {
          operation getNotional( request : NotionalValueRequest) : NotionalValueResponse
      }

      service ReferenceDataService {
          operation getReferenceData( isin: Isin): ReferenceData
          operation keyToValue(key: NotionalValueKey): NotionalKeyAndValue
      }
      """.trimIndent()
         val (vyne, stubService) = testVyne(schema)
         stubService.addResponse(
            "`findAll`", vyne.parseJson(
               "Input[]", """
      [
         {  "isin": "isin1", "strategyId": 1, "tradeId": "trade123"}
      ]
      """.trimIndent()
            )
         )

         // this is invoked for
         // Input::ISIN -> NotionalValueRequest -> getNotional -> NotionalValueResponse::NotionalValue
         // we fail on getNotional operation.
         val getNotionalHandler: OperationResponseHandler =
            { _: RemoteOperation, _: List<Pair<Parameter, TypedInstance>> ->
               throw IllegalArgumentException("")
            }


         stubService.addResponse("getNotional", getNotionalHandler)

         // this is invoked for
         // Input::TradeId -> getTradeData -> TradeData::NotionalValue
         // note that we return notionalValue value as null which violates @FirstNotEmpty constraint on output property.
         stubService.addResponse(
            "getTradeData",
            vyne.parseJson(
               "TradeData",
               """
           {"tradeId": "trade123", "notionalValue": null}
      """.trimIndent()
            )
         )

         // Below invoked through Input::StrategyId -> getStrategy -> Strategy::NotionalValueKey -> keyToValue path.
         // operation returns successfully, but the strategy id is null, so the path can't succeed.
         stubService.addResponse(
            "getStrategy",
            vyne.parseJson(
               "Strategy",
               """
           {"id": null}
      """.trimIndent()
            )
         )

         // This supposed to be invoked through
         // Input::Isin -> getReferenceData operation -> ReferenceData::NotionalValueKey -> keyToValue operation -> NotionalKeyAndValue::NotionalValue
         val getReferenceDataHandler: OperationResponseHandler =
            { _: RemoteOperation, _: List<Pair<Parameter, TypedInstance>> ->
               listOf(
                  vyne.parseJson(
                     "ReferenceData",
                     """
           {"notionalValueKey": 1}
      """.trimIndent()
                  )
               )
            }
         stubService.addResponse("getReferenceData", getReferenceDataHandler)

         val keyToValueHandler: OperationResponseHandler =
            { _: RemoteOperation, params: List<Pair<Parameter, TypedInstance>> ->
               // when the parameter value is 2, we arrive here through the tradeId attribute of the 'Input'
               // from tradeId we call getTradeData to fetch the TradeData which has a NotionalValueKey property.
               // we fail here to push the discovery to find
               // Input::Isin -> getReferenceData -> ReferenceData::NotionalValueKey -> keyToValue
               if (params.first().second.value == 2) {
                  throw IllegalArgumentException("2")
               }

               if (params.first().second.value == null) {
                  throw IllegalArgumentException("null key value")
               }
               listOf(
                  vyne.parseJsonModel(
                     "NotionalKeyAndValue",
                     """
           {"key": 1, "value": 100}
      """.trimIndent()
                  )
               )
            }

         stubService.addResponse("keyToValue", keyToValueHandler)

         runTest {
            val query = """
               find {
                   Input[]
                 } as Output[]
                 """.trimIndent()
            val turbine = vyne.query(query).rawResults.testIn(this)
            turbine.expectRawMap().should.equal(mapOf("notionalValue" to BigDecimal("100")))
            turbine.awaitComplete()
         }
      }

   @Test
   fun `where two operations exist and facts are known for one of them do not invoke the other`(): Unit = runBlocking {
      val (vyne, stub) = testVyne(
         """
         model ComplexFilmDetails {
         // This deeply nested path exists to make the transition cost higher than for SimpleFilmDetails
            screenDate : ScreenDate inherits Date
            filmDetails : {
               filmName : {
                  english : {
                     title : FilmTitle inherits String
                  }
               }
            }
         }
         model SimpleFilmDetails {
            title : FilmTitle
         }
         type ApiKey inherits String
         service FilmService2 {
            operation lookupTodaysFilms(ApiKey):ComplexFilmDetails
         }

         service FilmService1 {
            operation lookupFilmsForDate(ApiKey, ScreenDate):SimpleFilmDetails
         }


      """.trimIndent()
      )

      stub.addResponse("lookupFilmsForDate") { _, _ -> error("This shouldn't be called") }
      stub.addResponse(
         "lookupTodaysFilms", vyne.parseJson(
            "ComplexFilmDetails", """{ "screenDate" : "2022-11-28",
         | "filmDetails" : {
         |     "filmName" : {
         |        "english" : {
         |           "title" : "A New Hope"
      |             }
   |             }
|             }
|             }""".trimMargin()
         )
      )
      val result = vyne.query("""given { apiKey : ApiKey = "123" } find { title: FilmTitle }""")
         .firstRawObject()

      result.shouldNotBeNull()
      result.should.equal(mapOf("title" to "A New Hope"))

      // We should not have called this service
      stub.invocations["lookupFilmsForDate"]?.shouldBeNull()
   }

   // ORB-117
   @Test
   fun `fields on anonymous classes are considered for links`(): Unit = runBlocking {
      val (vyne, stub) = testVyne(
         """
         model Movie {
            id : MovieId inherits Int
             // This is the test -- this object isn't explicitly defined.
            productionTeam : {
               director : DirectorId inherits Int
            }
         }

         model Director {
            name : PersonName inherits String
         }
         service Movies {
            operation getMovie():Movie
            operation getDirector(DirectorId):Director
         }
      """.trimIndent()
      )
      stub.addResponse(
         "getMovie", vyne.parseJson(
            "Movie", """
         {
            "id" : 1,
            "productionTeam" : {
               "director" : 2
            }
         }
      """.trimIndent()
         )
      )
      stub.addResponse("getDirector", vyne.parseJson("Director", """{ "name" : "Jimmy" }"""))
      val result = vyne.query(
         """
         find { Movie } as {
            id : MovieId
            director : PersonName
         }
      """.trimIndent()
      )
      val movie = result.firstRawObject()
      movie.shouldBe(mapOf("id" to 1, "director" to "Jimmy"))
      stub.invocations["getDirector"]!!.single().value!!.shouldBe(2)
   }

   // ORB-117
   @Test
   fun `can use the value returned from a nested anonymous type as the input into another operation`():Unit = runBlocking {
      val (vyne,stub) = testVyne("""
         model Movie {
            id : MovieId inherits Int
            productionTeam : { // This is the test -- this object isn't explicitly defined.
               director : DirectorId inherits Int
            }

         }
         model Director {
            name : PersonName inherits String
         }
         service Movies {
            operation getMovie(MovieId):Movie
            operation getDirector(DirectorId):Director
         }
      """.trimIndent())
      stub.addResponse("getMovie", vyne.parseJson("Movie", """{ "id" : 1, "productionTeam" : { "director" : 100 } }"""))
      stub.addResponse("getDirector", vyne.parseJson("Director", """{ "name" : "Jimmy" }"""))

      val result = vyne.query("""given { MovieId = 1 } find {
         |  director : PersonName
         |}
      """.trimMargin())
         .firstRawObject()
      result.shouldBe(mapOf("director" to "Jimmy"))
   }

   // Note: This actually ends up getting called in the DirectServiceInvocationStrategy,
   // but wanted to document the expected behaviour.
   @Test
   fun `will invoke an operation if fact is subtype of declared parameter type`() : Unit = runBlocking{
      val (vyne,stub) = testVyne(
         """
            type PersonId inherits String
            type MotherId inherits PersonId

            closed model Person {
               name : Name inherits String
            }
            service PersonApi {
               operation findPerson(PersonId):Person
            }
         """.trimIndent()
      )
      stub.addResponse("findPerson", """{ "name" : "Jimmy" }""")
      val queryResult = vyne.query("""
         given { MotherId = '123' }
         find { Person }
      """.trimIndent())
         .firstRawObject()
      queryResult.shouldBe(mapOf("name" to "Jimmy"))
   }

   @Test
   fun `will invoke an operation if fact has property of subtype of declared parameter type`() : Unit = runBlocking{
      val (vyne,stub) = testVyne(
         """
            type PersonId inherits String
            type DirectorId inherits PersonId

            closed model Person {
               name : Name inherits String
            }
            service PersonApi {
               operation findPerson(PersonId):Person
            }

            model Film {
               director : DirectorId
            }
         """.trimIndent()
      )
      stub.addResponse("findPerson", """{ "name" : "Jimmy" }""")
      val queryResult = vyne.query("""
         given { Film = { director: '123' } }
         find { Person }
      """.trimIndent())
         .firstRawObject()
      queryResult.shouldBe(mapOf("name" to "Jimmy"))
   }
}
