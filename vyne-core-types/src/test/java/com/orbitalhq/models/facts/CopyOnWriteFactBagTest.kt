package com.orbitalhq.models.facts

import com.orbitalhq.from
import com.orbitalhq.models.Provided
import com.winterbe.expekt.should
import io.kotest.matchers.nulls.shouldNotBeNull
import com.orbitalhq.models.TypedCollection
import com.orbitalhq.models.TypedInstance
import com.orbitalhq.models.TypedNull
import com.orbitalhq.models.TypedObject
import com.orbitalhq.schemas.taxi.TaxiSchema
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import org.junit.Test

class CopyOnWriteFactBagTest {
   val schema = TaxiSchema.from(

      """
      model Person {
         id : PersonId inherits Int
         name : FirstName inherits String
      }
      model Actor inherits Person {
         agentName : AgentName inherits String
      }

      model Film {
        cast : Actor[]
        crew : Person[]
        imdbScore : ImdbScore inherits Decimal
      }

      model Catalog {
         films : Film[]
      }
   """.trimIndent()
   )

   @Test
   fun `can find a property on an object`() {
      val person = TypedInstance.from(schema.type("Person"), """ { "name" : "Jimmy", "id" : 1 }""", schema)
      val factBag = CopyOnWriteFactBag(listOf(person), schema)
      val value =
         factBag.getFact(schema.type("FirstName"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE)
      value.toRawObject().should.equal("Jimmy")
   }

   @Test
   fun `can find a fact by type`() {
      val person = TypedInstance.from(schema.type("Person"), """ { "name" : "Jimmy" }""", schema)
      val actor = TypedInstance.from(schema.type("Actor"), """ { "name" : "Jack" }""", schema)
      val factBag = CopyOnWriteFactBag(listOf(person, actor), schema)

      val collection =
         factBag.getFact(schema.type("Person"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY) as TypedCollection
      collection.should.have.size(2)
   }

   @Test
   fun `search for missing type returns null`() {
      val person = TypedInstance.from(schema.type("Person"), """ { "name" : "Jimmy" }""", schema)
      val actor = TypedInstance.from(schema.type("Actor"), """ { "name" : "Jack" }""", schema)
      val factBag = CopyOnWriteFactBag(listOf(person, actor), schema)

      val result =
         factBag.getFactOrNull(schema.type("ImdbScore"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY)
      result.shouldBeNull()
   }

   @Test
   fun `does not return invalid type`() {
      val schema = TaxiSchema.from(
         """
         type Age inherits Int
         type Id inherits Int

         model Person {
            id : Id
         }
      """.trimIndent()
      )
   }


   @Test
   fun `can find a collection of properties from within a collection`() {
      val catalog = TypedInstance.from(
         schema.type("Catalog"), """
            {
               "films" : [
                  { "cast" : [
                     { "name" : "Mark" , "agentName" : "Jenny" },
                     { "name" : "Carrie" , "agentName" : "Amanda" }
                    ]
                  },
                  { "cast" : [
                     { "name" : "George" , "agentName" : "Sophie" },
                     { "name" : "Hamish" , "agentName" : "Leslie" }
                    ]
                  }
               ]
            }
         """.trimIndent(), schema
      )
      val factBag = CopyOnWriteFactBag(catalog, schema)
      val collection =
         factBag.getFact(schema.type("AgentName"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY) as TypedCollection
      collection.should.have.size(4)

   }

   @Test
   fun `two searches are considered equal`() {
      // This test doesn't make sense / work under the new approach
      if (CopyOnWriteFactBag.useExperimentalFactSearch) {
         return
      }
      val person = TypedInstance.from(schema.type("Person"), """ { "name" : "Jimmy" }""", schema)
      val actor = TypedInstance.from(schema.type("Actor"), """ { "name" : "Jack" }""", schema)
      val factBag = CopyOnWriteFactBag(listOf(person, actor), schema)

      factBag.getFact(schema.type("Person"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY) as TypedCollection
      factBag.searchIsCached(schema.type("Person"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY).should.be.`true`
   }

   @Test
   fun `querying allow all for collections returns a flattened collection`() {
      val film = TypedInstance.from(
         schema.type("Film"), """{
            "cast": [   { "name" : "Jack" } , { "name" : "Jimmy" } ],
            "crew" : [ { "name" : "Pete" } , { "name" : "Paul" } ]
            }""", schema
      )

      val factBag = CopyOnWriteFactBag(listOf(film), schema)

      val collection =
         factBag.getFact(schema.type("Person"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY) as TypedCollection
      // Result should be flattened - i.e.,
      // Expect a single collection with all elements, not a collection of two collections, with two elements each
      collection.should.have.size(4)
   }

   @Test
   fun `querying for a collection of types collects non collection values that match on type`() {
      val film = TypedInstance.from(
         schema.type("Film"), """{
            "cast": [   { "name" : "Jack" } , { "name" : "Jimmy" } ],
            "crew" : [ { "name" : "Pete" } , { "name" : "Paul" } ],
            "imdbScore" : 5.5
            }""", schema
      )
      val factBag = CopyOnWriteFactBag(listOf(film), schema)
      val facts =
         factBag.getFact(schema.type("ImdbScore[]"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY) as TypedCollection
      facts.toRawObject().should.equal(listOf(5.5))
   }

   @Test
   fun `querying for a collection of types collects non collection values that match on type within nested collection`() {
      val film = TypedInstance.from(
         schema.type("Catalog"), """{
            "films" : [
            {
               "cast": [   { "name" : "Jack" } , { "name" : "Jimmy" } ],
               "crew" : [ { "name" : "Pete" } , { "name" : "Paul" } ],
               "imdbScore" : 5.5
            },
            {
               "cast": [   { "name" : "Jack" } , { "name" : "Jimmy" } ],
               "crew" : [ { "name" : "Pete" } , { "name" : "Paul" } ],
               "imdbScore" : 2.5
            }
         ]
         }""", schema
      )
      val factBag = CopyOnWriteFactBag(listOf(film), schema)
      val facts =
         factBag.getFact(schema.type("ImdbScore[]"), FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY) as TypedCollection
      facts.toRawObject().should.equal(listOf(5.5, 2.5))
   }

   @Test
   fun `requesting a collection type returns the collection`() {
      val film = TypedInstance.from(
         schema.type("Catalog"), """{
            "films" : [
            {
               "cast": [   { "name" : "Jack" } , { "name" : "Jimmy" } ],
               "crew" : [ { "name" : "Pete" } , { "name" : "Paul" } ],
               "imdbScore" : 5.5
            },
            {
               "cast": [   { "name" : "Jack" } , { "name" : "Jimmy" } ],
               "crew" : [ { "name" : "Pete" } , { "name" : "Paul" } ],
               "imdbScore" : 2.5
            }
         ]
         }""", schema
      )
      val factBag = CopyOnWriteFactBag(listOf(film), schema)
      val facts = factBag.getFact(schema.type("Film[]"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE) as TypedCollection
      facts.should.have.size(2)
   }

   @Test
   fun `can fetch an enum synonym from a factbag`() {
      val schema = TaxiSchema.from(
         """
         enum TwoLetterCountryCode {
            NZ,
            UK
         }
         enum ThreeLetterCountryCode {
            NZL synonym of TwoLetterCountryCode.NZ,
            UKI synonym of TwoLetterCountryCode.UK
         }
         model Person {
            country: TwoLetterCountryCode
         }
      """.trimIndent()
      )
      val person = TypedInstance.from(schema.type("Person"), """{ "country": "NZ" }""", schema)
      val factBag = CopyOnWriteFactBag(person, schema)
      val result = factBag.getFact(schema.type("ThreeLetterCountryCode"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE)
      result.shouldNotBeNull()
      result.typeName.shouldBe("ThreeLetterCountryCode")
      result.toRawObject().shouldBe("NZL")
   }

   @Test
   fun `can find enum object values`() {
      val schema = TaxiSchema.from(
         """
            model ErrorDetails {
               code : ErrorCode inherits Int
               message : ErrorMessage inherits String
            }
            enum Errors<ErrorDetails> {
               BadRequest({ code : 400, message : 'Bad Request' }),
               Unauthorized({ code : 401, message : 'Unauthorized' })
            }
""".trimIndent()
      )
      val enumValue = schema.type("Errors")
         .enumTypedInstance("BadRequest", Provided)
      val factBag = CopyOnWriteFactBag(enumValue, schema)
      factBag.getFactOrNull(schema.type("Errors")).shouldNotBeNull()
      factBag.getFactOrNull(schema.type("ErrorDetails"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE).shouldNotBeNull()
      factBag.getFactOrNull(schema.type("ErrorCode"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE).shouldNotBeNull()
      factBag.getFactOrNull(schema.type("ErrorMessage"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE).shouldNotBeNull()
   }

   @Test
   fun `when using take last search strategy insertion order is respected`() {
      val schema = TaxiSchema.from("""
         model Person {
            name : PersonName inherits String
         }
         model NameUpdatedEvent {
            name : PersonName
         }
      """.trimIndent())
      val person = TypedInstance.from(schema.type("Person"), """{ "name" : "Jimmy" }""", schema)
      val updateEvent = TypedInstance.from(schema.type("NameUpdatedEvent"), """{ "name" : "Jack" }""", schema)

      val factBag = CopyOnWriteFactBag(listOf(person,updateEvent), schema)
      val fact = factBag.getFactOrNull(schema.type("PersonName"), FactDiscoveryStrategy.ANY_DEPTH_TAKE_LAST)
      fact!!.value.shouldBe("Jack")

      // Swap the insertion order
      val factBag2 = CopyOnWriteFactBag(listOf(updateEvent, person), schema)
      val fact2 = factBag2.getFactOrNull(schema.type("PersonName"), FactDiscoveryStrategy.ANY_DEPTH_TAKE_LAST)
      fact2!!.value.shouldBe("Jimmy")
   }

   @Test
   fun `when object has typed null then null is returned`() {
      val schema = TaxiSchema.from("""
         model Person {
            name : PersonName inherits String
            spouseName : SpouseName inherits String
         }
      """.trimIndent())
      val person = TypedInstance.from(schema.type("Person"), """{ "name" : "Jimmy" }""", schema) as TypedObject
//      person["spouseName"].shouldBeInstanceOf<TypedNull>()

      val factBag = CopyOnWriteFactBag(listOf(person), schema)
      val searchResult = factBag.getFactOrNull(schema.type("SpouseName"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE)
      searchResult.shouldBeInstanceOf<TypedNull>()
   }


   @Test
   fun `when object has scoped typed null then null is returned`() {
      val schema = TaxiSchema.from("""
         model Person {
            name : PersonName inherits String
            spouseName : SpouseName inherits String
         }
      """.trimIndent())
      val person = TypedInstance.from(schema.type("Person"), """{ "name" : "Jimmy" }""", schema) as TypedObject
//      person["spouseName"].shouldBeInstanceOf<TypedNull>()

      val factBag = CopyOnWriteFactBag(emptyList(), schema)
         .withAdditionalScopedFacts(listOf(scopedFact(person, "person")), schema)

      val searchResult = factBag.getFactOrNull(schema.type("SpouseName"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE_DISTINCT)
      searchResult.shouldBeInstanceOf<TypedNull>()
   }

   /**
    * This is an important conscious design choice.
    * If T is present (but null), then searches for properties of T must also be null.
    *
    * This is critical to ensure that scopes don't leak.
    * eg:
    * find { Film } as {
    *    filmAward : AwardTitle //comes from film
    *    // find the star of the film (CastMember), and return their AwardTitle
    *    starAward = first(CastMember[], (IsStar) -> IsStar == true) as AwardTitle
    * }
    *
    * In the above example, if first(CastMember[]) returns null, then the StarAward must also be null.
    * However, if we don't yeild a null here, then we'll search elsewhere in higher scopes, and find the
    * AwardTitle from the parent object - a Film.
     */
   @Test
   fun `when object itself is null then search for child attribute returns null`() {
      val schema = TaxiSchema.from("""
         model Person {
            name : PersonName inherits String
            spouseName : SpouseName inherits String
         }
      """.trimIndent())
      val person = TypedNull.create(schema.type("Person"))

      val factBag = CopyOnWriteFactBag(emptyList(), schema)
         .withAdditionalScopedFacts(listOf(scopedFact(person, "person")), schema)

      val searchResult = factBag.getFactOrNull(schema.type("SpouseName"), FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE_DISTINCT)

      // SearchResult should not be null (which indicates 'nothing found'), but should
      // be a TypedNull
      searchResult.shouldBeInstanceOf<TypedNull>()
   }

}
