package com.orbitalhq

import com.orbitalhq.schemas.taxi.TaxiSchema
import io.kotest.matchers.collections.shouldContain
import lang.taxi.TaxiDocument
import java.time.Instant

fun List<VersionedSource>.asPackage(
   organisation: String = "com.foo",
   name: String = "test",
   version: String = "1.0.0"
): SourcePackage = SourcePackage(PackageMetadata.from(organisation, name, version), this, emptyMap(), null)

fun VersionedSource.asPackage(
   organisation: String = "com.foo",
   name: String = "test",
   version: String = "1.0.0",
   submissionDate: Instant = Instant.now()
): SourcePackage {
   return SourcePackage(
      PackageMetadata.from(PackageIdentifier(organisation, name, version), submissionDate),
      listOf(this),
      emptyMap(),
      readme = null
   )
}


fun ParsedSource.asParsedPackage(
   organisation: String = "com.foo",
   name: String = "test",
   version: String = "1.0.0"
): ParsedPackage {
   return ParsedPackage(
      PackageMetadata.from(organisation, name, version),
      listOf(this),
      emptyMap(),
         readme = null
   )
}


fun SourcePackage.toParsedPackages(): List<ParsedPackage> = listOf(this.toParsedPackage())
fun SourcePackage.toParsedPackage(): ParsedPackage {
   return ParsedPackage(
      this.packageMetadata,
      this.sourcesWithPackageIdentifier.map { ParsedSource(it) },
      this.additionalSources,
      readme = null
   )
}

fun List<ParsedSource>.asParsedPackage(
   organisation: String = "io.cask",
   name: String = "test",
   version: String = "1.0.0"
): ParsedPackage {
   return ParsedPackage(PackageMetadata.from(organisation, name, version), this, emptyMap(),readme = null)
}

fun List<ParsedSource>.asParsedPackages(
   organisation: String = "io.cask",
   name: String = "test",
   version: String = "1.0.0"
): List<ParsedPackage> = listOf(this.asParsedPackage(organisation, name, version))

fun TaxiSchema.Companion.from(sources: List<VersionedSource>): TaxiSchema {
   return TaxiSchema.from(sources.asPackage())
}
fun TaxiSchema.Companion.from(source: VersionedSource): TaxiSchema {
   return TaxiSchema.from(listOf(source).asPackage())
}



fun SourcePackage.shouldCompileTheSameAs(expected: String):TaxiDocument {
   return lang.taxi.testing.TestHelpers.expectToCompileTheSame(this.sources.map { it.content }, expected)
}
