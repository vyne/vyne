package com.orbitalhq.schemas

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.google.common.base.MoreObjects
import com.orbitalhq.VersionedSource
import com.orbitalhq.VyneTypes
import com.orbitalhq.annotations.http.HttpIgnoreErrorsAnnotationSchema
import com.orbitalhq.annotations.http.HttpRetryAnnotationSchema
import com.orbitalhq.models.TypedInstance
import com.orbitalhq.query.RemoteCall
import com.orbitalhq.utils.ImmutableEquality
import lang.taxi.annotations.HttpOperation
import lang.taxi.expressions.Expression
import lang.taxi.services.OperationScope
import lang.taxi.services.operations.constraints.Constraint
import lang.taxi.services.operations.constraints.ConstraintComparison
import lang.taxi.types.Documented
import reactor.util.retry.Retry
import reactor.util.retry.RetryBackoffSpec
import java.io.Serializable
import java.math.BigDecimal
import java.time.Duration


typealias OperationName = String
typealias ServiceName = String

object ParamNames {
   fun isParamName(input: String): Boolean {
      return input.startsWith("param/")
   }

   fun typeNameInParamName(paramName: String): String {
      return paramName.removePrefix("param/")
   }

   fun toParamName(typeName: String): String {
      return "param/$typeName"
   }
}

object OperationNames {
   private const val DELIMITER: String = "@@"
   fun name(serviceName: String, operationName: String): String {
      return listOf(serviceName, operationName).joinToString(DELIMITER)
   }

   fun displayName(serviceName: String, operationName: OperationName): String {
      return "$serviceName / $operationName"
   }

   fun qualifiedName(serviceName: ServiceName, operationName: OperationName): QualifiedName {
      return name(serviceName, operationName).fqn()
   }

   fun serviceAndOperation(qualifiedOperationName: String): Pair<ServiceName, OperationName> {
      val parts = qualifiedOperationName.split(DELIMITER)
      require(parts.size == 2) {
         "$qualifiedOperationName is not a valid operation name."
      }
      return parts[0] to parts[1]
   }

   fun serviceAndOperation(qualifiedOperationName: QualifiedName): Pair<ServiceName, OperationName> {
      return serviceAndOperation(qualifiedOperationName.fullyQualifiedName)
   }

   fun operationName(qualifiedOperationName: QualifiedName): OperationName {
      return serviceAndOperation(qualifiedOperationName).second
   }

   fun serviceName(qualifiedOperationName: QualifiedName): ServiceName {
      return serviceAndOperation(qualifiedOperationName).first
   }

   fun isName(memberName: String): Boolean {
      return memberName.contains(DELIMITER)
   }

   fun isName(memberName: QualifiedName): Boolean {
      return memberName.fullyQualifiedName.contains(DELIMITER)
   }

   fun shortDisplayNameFromOperation(operationName: QualifiedName): String {
      val (serviceName, operationName) = OperationNames.serviceAndOperation(operationName)
      return displayName(serviceName.fqn().shortDisplayName, operationName)
   }

   fun displayNameFromOperationName(operationName: QualifiedName): String {
      val (serviceName, operationName) = OperationNames.serviceAndOperation(operationName)
      return displayName(serviceName, operationName)
   }
}

// Need to use @JsonDeserialize on this type, as the PartialXxxx
// interface is overriding default deserialization behaviour
// causing all of these to be deserialized as partials, even when
// they're the real thing
@JsonDeserialize(`as` = Parameter::class)
data class Parameter(
   @get:JsonSerialize(using = TypeAsNameJsonSerializer::class)
   val type: Type,
   override val name: String? = null,
   override val metadata: List<Metadata> = emptyList(),
   val constraints: List<Constraint> = emptyList(),
   val typeDoc: String? = null,
   val nullable: Boolean,
   val defaultValue: Expression? = null
) : MetadataTarget, PartialParameter {
   fun isNamed(name: String): Boolean {
      return this.name != null && this.name == name
   }

   /**
    * Returns constraints defined both on the parameter directly,
    * and within the type.
    *
    */
   val allConstraints = mapOf("" to constraints).filter { (_,constraints) -> constraints.isNotEmpty() } + type.constraintsByPath

   private val equality = ImmutableEquality(
      this,
      Parameter::name,
      Parameter::type,
      Parameter::metadata,
   )

   override fun equals(other: Any?): Boolean = equality.isEqualTo(other)
   override fun hashCode(): Int = equality.hash()

   override val typeName: QualifiedName = type.name
}

// Need to use @JsonDeserialize on this type, as the PartialXxxx
// interface is overriding default deserialization behaviour
// causing all of these to be deserialized as partials, even when
// they're the real thing
@JsonDeserialize(`as` = Operation::class)
data class Operation(
   override val qualifiedName: QualifiedName,
   override val parameters: List<Parameter>,
   @get:JsonSerialize(using = TypeAsNameJsonSerializer::class)
   override val returnType: Type,
   // similar to scope in taxi - ie., read / write
   override val operationType: OperationScope,
   override val metadata: List<Metadata> = emptyList(),
   override val contract: OperationContract = OperationContract(returnType),
   @get:JsonIgnore
   val sources: List<VersionedSource>,
   override val typeDoc: String? = null
) : MetadataTarget, SchemaMember, RemoteOperation, PartialOperation {
   private val cachedHashCode: Int = run {
      var result = qualifiedName.hashCode()
      result = 31 * result + returnType.hashCode()
      result = 31 * result + parameters.hashCode()
      result = 31 * result + metadata.hashCode()
      result
   }

   override fun equals(other: Any?): Boolean {
      if (this === other) return true
      if (other !is Operation) return false

      return qualifiedName == other.qualifiedName &&
         returnType == other.returnType &&
         parameters == other.parameters &&
         metadata == other.metadata
   }

   override fun hashCode(): Int {
      return cachedHashCode
   }

   override val operationKind: OperationKind = OperationKind.ApiCall
   override val schemaMemberKind: SchemaMemberKind = SchemaMemberKind.OPERATION

   fun parameter(name: String): Parameter? {
      return this.parameters.firstOrNull { it.name == name }
   }

   override val returnTypeName: QualifiedName = returnType.name
}

/**
 * The base interface that covers both traditional operations
 * ( Operation ), and query operations (QueryOperation)
 *
 */
interface RemoteOperation : MetadataTarget, Documented, SchemaMember {
   val parameters: List<Parameter>
   val returnType: Type
   val contract: OperationContract

   val operationType: OperationScope

   val operationKind: OperationKind

   val name: String
      get() = OperationNames.operationName(qualifiedName)
}

enum class OperationKind {
   ApiCall,
   Query,
   Stream,
   Table
}

// Need to use @JsonDeserialize on this type, as the PartialXxxx
// interface is overriding default deserialization behaviour
// causing all of these to be deserialized as partials, even when
// they're the real thing

data class ConsumedOperation(val serviceName: ServiceName, val operationName: String) {
   val operationQualifiedName: QualifiedName = OperationNames.qualifiedName(serviceName, operationName)
}

data class ServiceLineage(
   val consumes: List<ConsumedOperation>,
   val stores: List<QualifiedName>,
   val metadata: List<Metadata>
) {
   fun consumesOperation(operationName: QualifiedName): Boolean {
      return consumes.any {
         it.operationQualifiedName == operationName
      }
   }

   fun getConsumerOf(operationName: QualifiedName): List<ConsumedOperation> {
      return this.consumes.filter { it.operationQualifiedName == operationName }
   }

   companion object {
      fun empty() = ServiceLineage(emptyList(), emptyList(), emptyList())
   }
}


enum class ServiceKind : Serializable {
   API,
   Database,
   Kafka,
   Mongo;

   companion object {

      fun inferFromMetadata(
         serviceMetadata: List<Metadata>,
         operations: List<Operation> = emptyList(),
         streamOperations: List<StreamOperation>,
         tableOperations: List<TableOperation>
      ): ServiceKind? {
         val allOperationMetadata = operations.flatMap { it.metadata }
         val hasOperations = operations.isNotEmpty()
         val hasStreams = streamOperations.isNotEmpty()
         val hasTables = tableOperations.isNotEmpty()
         // We have to use string literals here, rather than constants, as we don't have
         // compile time dependencies on the connector libraries in core.
         return when {
            !hasOperations && !hasStreams && hasTables -> Database
            !hasOperations && hasStreams && !hasTables -> Kafka

            serviceMetadata.containsMetadata("${VyneTypes.NAMESPACE}.kafka.KafkaService") -> Kafka
            serviceMetadata.containsMetadata("${VyneTypes.NAMESPACE}.jdbc.DatabaseService") -> Database
            serviceMetadata.containsMetadata("${VyneTypes.NAMESPACE}.aws.dynamo.DynamoService") -> Database
            serviceMetadata.containsMetadata("${VyneTypes.NAMESPACE}.mongo.MongoService") -> Mongo
            allOperationMetadata.containsMetadata(HttpOperation.NAME) -> API
            else -> null
         }
      }
   }
}

// Need to use @JsonDeserialize on this type, as the PartialXxxx
// interface is overriding default deserialization behaviour
// causing all of these to be deserialized as partials, even when
// they're the real thing
@JsonDeserialize(`as` = Service::class)
data class Service(
   override val name: QualifiedName,
   override val operations: List<Operation>,
   override val queryOperations: List<QueryOperation>,
   override val streamOperations: List<StreamOperation> = emptyList(),
   override val tableOperations: List<TableOperation> = emptyList(),
   override val metadata: List<Metadata> = emptyList(),
   val sourceCode: List<VersionedSource>,
   override val typeDoc: String? = null,
   val lineage: ServiceLineage? = null,
   val serviceKind: ServiceKind? = ServiceKind.inferFromMetadata(
      metadata,
      operations,
      streamOperations,
      tableOperations
   ),
) : MetadataTarget, SchemaMember, PartialService {

   override val schemaMemberKind: SchemaMemberKind = SchemaMemberKind.SERVICE

   override fun equals(other: Any?): Boolean {
      if (this === other) return true
      if (other !is Service) return false

      return name == other.name &&
         operations == other.operations &&
         queryOperations == other.queryOperations &&
         tableOperations == other.tableOperations &&
         streamOperations == other.streamOperations &&
         typeDoc == other.typeDoc &&
         metadata == other.metadata
   }

   override fun hashCode(): Int {
      var result = name.hashCode()
      result = 31 * result + operations.hashCode()
      result = 31 * result + queryOperations.hashCode()
      result = 31 * result + tableOperations.hashCode()
      result = 31 * result + streamOperations.hashCode()
      result = 31 * result + typeDoc.hashCode()
      result = 31 * result + metadata.hashCode()
      return result
   }

   fun queryOperation(name: String): QueryOperation {
      return this.queryOperations.first { it.name == name }
   }

   fun operation(name: String): Operation {
      return this.operations.first { it.name == name }
   }

   val remoteOperations: List<RemoteOperation> =
      operations + streamOperations + queryOperations + tableOperations.flatMap { it.queryOperations }


   fun remoteOperation(name: String): RemoteOperation {
      return this.queryOperations.firstOrNull { it.name == name }
         ?: this.tableOperations.firstOrNull { it.name == name }
         ?: this.tableOperations.flatMap { it.queryOperations }.firstOrNull { it.name == name }
         ?: this.streamOperations.firstOrNull { it.name == name }
         ?: this.operations.firstOrNull { it.name == name }
         ?: error("No operation named $name found on service ${this.name.longDisplayName}")
   }

   fun hasRemoteOperation(name: String): Boolean {
      return this.remoteOperations.any { it.name == name }
   }

   fun hasOperation(name: String): Boolean {
      return this.operations.any { it.name == name }
   }

   @JsonIgnore
   override val qualifiedName: QualifiedName = name
   val fullyQualifiedName = name.fullyQualifiedName
}


@Deprecated(message = "Try to move towards Taxi's OperationContract")
data class OperationContract(
   @JsonSerialize(using = TypeAsNameJsonSerializer::class)
   val returnType: Type, val constraints: List<Constraint> = emptyList()
) {
   fun containsConstraint(clazz: Class<out Constraint>): Boolean {
      return constraints.filterIsInstance(clazz)
         .isNotEmpty()
   }

   fun <T : Constraint> containsConstraint(clazz: Class<T>, predicate: (T) -> Boolean): Boolean {
      return constraints.filterIsInstance(clazz).any(predicate)
   }

   fun <T : Constraint> constraint(clazz: Class<T>, predicate: (T) -> Boolean): T {
      return constraints.filterIsInstance(clazz).first(predicate)
   }

   fun <T : Constraint> constraint(clazz: Class<T>): T {
      return constraints.filterIsInstance(clazz).first()
   }

   fun satisfies(requiredConstraint: Constraint): ConstraintComparison {
      if (this.constraints.isEmpty()) return ConstraintComparison.NOT_SATISFIED
      return this.constraints.map { it.satisfiesRequestedConstraint(requiredConstraint) }
         .reduce { acc, constraintComparison -> acc + constraintComparison }
   }
   fun satisfiesAll(constraints: List<Constraint>): Boolean {
      return constraints.all { satisfies(it).satisfiesRequestedConstraint }
   }
}


fun RemoteOperation.httpOperationMetadata(): VyneHttpOperation {
   val metadataName = HttpOperation.NAME
   val annotation = firstMetadata(metadataName)
   val url = annotation.params["url"] as String
   val method = annotation.params["method"] as String
   return VyneHttpOperation(httpOperationMetadata = annotation, url = url, method = method)
}

fun RemoteOperation.retrySpec(): VyneHttpRetrySpec? {
   return if (hasMetadata(HttpRetryAnnotationSchema.NAME)) {
      val annotation = firstMetadata(HttpRetryAnnotationSchema.NAME)
      val responseCodes = annotation.params["responseCode"] as List<Int>
      val fixedRetryPolicy = annotation.params["fixedRetryPolicy"] as? Map<String, Int>
      val exponentialRetryPolicy = annotation.params["exponentialRetryPolicy"] as? Map<String, Any>

      when {
         fixedRetryPolicy != null -> VyneHttpRetrySpec(
            responseCodes = responseCodes.toSet(),
            retrySpec = Retry.fixedDelay(
               fixedRetryPolicy["maxRetries"]!!.toLong(),
               Duration.ofSeconds(fixedRetryPolicy["retryDelay"]!!.toLong())
            )
         )

         exponentialRetryPolicy != null ->
            VyneHttpRetrySpec(
               responseCodes = responseCodes.toSet(),
               retrySpec = Retry.backoff(
                  (exponentialRetryPolicy["maxRetries"]!! as Int).toLong(),
                  Duration.ofSeconds((exponentialRetryPolicy["retryDelay"]!! as Int).toLong())
               ).jitter(
                  ((exponentialRetryPolicy["jitter"]!! as BigDecimal)).toDouble()
               )
            )

         else -> null
      }
   } else {
      null
   }
}

fun RemoteOperation.ignoreErrorsSpec(): VyneHttpIgnoreErrorsSpec? {
   return if (hasMetadata(HttpIgnoreErrorsAnnotationSchema.NAME)) {
      val annotation = firstMetadata(HttpIgnoreErrorsAnnotationSchema.NAME)
      val responseCodes = annotation.params["responseCodes"] as List<String>
      VyneHttpIgnoreErrorsSpec(responseCodes.toSet())
   } else {
      null
   }
}

data class VyneHttpOperation(val httpOperationMetadata: Metadata, val url: String, val method: String)
data class VyneHttpRetrySpec(val responseCodes: Set<Int>, val retrySpec: RetryBackoffSpec) {
   fun toLogString() {
      MoreObjects.toStringHelper(this)
         .add("responseCode", responseCodes)
         .add("maxAttempts", retrySpec.maxAttempts)
         .add("minBackoff", retrySpec.minBackoff)
         .add("maxBackoff", retrySpec.maxBackoff)
         .add("jitter", retrySpec.jitterFactor)
   }
}

class VyneHttpIgnoreErrorsSpec(httpStatusCodes: Set<String>) {
   private val statusCodeRanges: Set<ClosedRange<Int>>
   private val statusCodes: Set<Int>
   init {
      statusCodeRanges = httpStatusCodes
          .filter { statusCode -> statusCode.endsWith("XX", true) }
          .map { statusCodeRange ->
             val start = statusCodeRange[0].digitToInt() * 100
             val end = (statusCodeRange[0].digitToInt() + 1)* 100
             (start..end)
          }.toSet()

      statusCodes = httpStatusCodes.filterNot { statusCode -> statusCode.endsWith("XX", true) }
         .mapNotNull { statusCode -> statusCode.toIntOrNull() }
         .toSet()
   }
   fun match(httpStatusCode: Int): Boolean {
      return statusCodeRanges.any { range -> range.contains(httpStatusCode) } || statusCodes.contains(httpStatusCode)
   }
}

/**
 * Use this exception when we failed to actually send the request.
 * There's no response code or remote call, because we never got the request away
 * (eg., when there's a malformed address).
 */
class OperationRequestFailedException(
   message: String,
   throwable: Throwable?
) : RuntimeException(message, throwable)

class OperationInvocationException(
   message: String,
   val httpStatus: Int,
   val remoteCall: RemoteCall,
   val parameters: List<Pair<Parameter, TypedInstance>>
) : RuntimeException(message)

private fun List<Metadata>.containsMetadata(name: String): Boolean {
   return this.any { it.name.fullyQualifiedName == name }
}
