package com.orbitalhq.models

import com.fasterxml.jackson.databind.ObjectMapper
import com.orbitalhq.errors.OrbitalQueryException
import com.orbitalhq.models.conditional.ConditionalFieldSetEvaluator
import com.orbitalhq.models.facts.*
import com.orbitalhq.models.format.FormatDetector
import com.orbitalhq.models.format.ModelFormatSpec
import com.orbitalhq.models.functions.FunctionRegistry
import com.orbitalhq.models.functions.FunctionResultCacheKey
import com.orbitalhq.models.json.Jackson
import com.orbitalhq.models.json.JsonParsedStructure
import com.orbitalhq.models.json.isJson
import com.orbitalhq.policies.ScopedPolicyEngine
import com.orbitalhq.query.AlwaysGoodSpec
import com.orbitalhq.query.TypedInstanceValidPredicate
import com.orbitalhq.schemas.*
import com.orbitalhq.schemas.taxi.toVyneQualifiedName
import com.orbitalhq.utils.timeBucket
import com.orbitalhq.utils.xtimed
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import lang.taxi.accessors.*
import lang.taxi.expressions.Expression
import lang.taxi.expressions.LambdaExpression
import lang.taxi.expressions.TypeExpression
import lang.taxi.services.operations.constraints.Constraint
import lang.taxi.types.FieldProjection
import lang.taxi.types.FormatsAndZoneOffset
import mu.KotlinLogging
import org.apache.commons.csv.CSVRecord
import java.util.concurrent.CopyOnWriteArrayList
import java.util.stream.Collectors


/**
 * Constructs a TypedObject
 *
 * @param evaluateAccessors Determines if accessors defined in the schema should be evaluated.  Normally
 * this should be true.  However, for content served from a cask, the content is already preparsed, and so
 * does not need accessors to be evaluated.
 */
class TypedObjectFactory(
   private val type: Type,
   private val value: Any,
   internal val schema: Schema,
   val nullValues: Set<String> = emptySet(),
   val source: DataSource,
   private val objectMapper: ObjectMapper = Jackson.defaultObjectMapper,
   private val functionRegistry: FunctionRegistry = schema.functionRegistry,
   private val evaluateAccessors: Boolean = true,
   override val inPlaceQueryEngine: InPlaceQueryEngine? = null,
   private val accessorHandlers: List<AccessorHandler<out Accessor>> = emptyList(),
   private val formatSpecs: List<ModelFormatSpec> = emptyList(),
   private val parsingErrorBehaviour: ParsingFailureBehaviour = ParsingFailureBehaviour.ThrowException,
   private val functionResultCache: MutableMap<FunctionResultCacheKey, Any> = mutableMapOf(),
   private val projectionScope: ProjectionFunctionScope? = null,
   private val metadata: Map<String, Any> = emptyMap(),
   private val valueSuppliers: List<ValueSupplier> = emptyList(),
   /**
    * Normally, we don't allow construction of closed types.
    * However, if a type is both closed AND a parameter type,
    * then we optionally want to allow construction of these types.
    * Generally, set this to false, unless attempting to construct a
    * parameter to a call
    */
   private val constructClosedParameterTypes: Boolean = false,
   private val policyEngine: ScopedPolicyEngine? = null,

   /**
    * Controls how we read from a fact bag.
    * Practically speaking, this determines what to do if there are multiple facts of the same
    * type found in the fact bag.
    *
    * A long standing reasonable default is ANY_DEPTH_EXPECT_ONE_DISTINCT.
    * When performing merges over time (eg., merging values of multiple streams),
    * you may want ANY_DEPTH_TAKE_LAST
    */
   private val factBagSearchStrategy: FactDiscoveryStrategy = FactDiscoveryStrategy.ANY_DEPTH_EXPECT_ONE_DISTINCT,

   private val parsingOptions: ParsingOptions = ParsingOptions.DEFAULT


) : EvaluationValueSupplier, ValueProjector {

   companion object {
      private val logger = KotlinLogging.logger {}
   }

   private val buildSpecProvider = TypedInstancePredicateFactory()

   private val valueReader = ValueReader()
   private val accessorReader: AccessorReader by lazy {
      AccessorReader(
         this,
         this.functionRegistry,
         this.schema,
         this.accessorHandlers,
         this.functionResultCache,
         valueProjector = this

      )
   }
   private val conditionalFieldSetEvaluator = ConditionalFieldSetEvaluator(this, this.schema, accessorReader)
   private val formatDetector = FormatDetector.get(formatSpecs)
   private val currentValueFactBag: FactBag by lazy {
      when {
         value is FactBag -> value
         value is List<*> && value.filterIsInstance<TypedInstance>()
            .isNotEmpty() -> FactBag.of(value.filterIsInstance<TypedInstance>(), schema)

         else -> FactBag.empty()
      }
   }

   /**
    * Even if evaluateAccessors is globally true, we sometimes want to
    * disable accessor evaluation temporarily.
    *
    * This is typically for evaluating a default() value, to check if the underlying value is actually present / discoverable,
    * before falling back to the default.
    *
    * This approach isn't great, as we have mutable state here.
    * However, given the lazy function evaluation we're using, there isn't an easy alternative.
    *
    * If/when we finally remove accessors for just outright ExpressionEvaluation, (ie., removing "special" accessors like
    * default() and column() ), we can remove this.
    * */
   private var accessorEvaluationSupressed: Boolean = false


   private val attributesToMap = type.attributes

   private val fieldInitializers: Map<AttributeName, Lazy<TypedInstance>> by lazy {
      attributesToMap.map { (attributeName, field) ->
         attributeName to lazy {

            val fieldValue =
               xtimed("build field $attributeName ${field.typeDisplayName}") {
                  buildField(
                     field,
                     attributeName
                  )
               }.let { constructedField ->
                  if (policyEngine != null && inPlaceQueryEngine != null) {
                     policyEngine.evaluate(constructedField, inPlaceQueryEngine)
                  } else {
                     constructedField
                  }
               }

            // Do not start a projection if the type we're projecting to is the same as the value we have.
            // This happens if the projection was processed internally already within the object construction
            if (field.fieldProjection != null && !field.fieldProjection.projectedType.isAssignableTo(fieldValue.type.taxiType)) {
//               val sw = Stopwatch.createStarted()
               val projection = xtimed("Project field $attributeName") {
                  projectField(
                     field,
                     fieldValue,
                  )
               }
//               logger.debug { "Projection to ${projection.type.name.shortDisplayName} took ${sw.elapsed().toMillis()}ms" }
               projection

            } else {
               fieldValue
            }
         }
      }.toMap()
   }

   /**
    * Where a field has an inline projection defined (
    * foo : Thing as {
    *    a : A, b: B
    * }
    */
   private fun projectField(
      field: Field,
      fieldValue: TypedInstance,
   ): TypedInstance {
      if (fieldValue is TypedNull) {
         // Don't attempt to project nulls
         return TypedNull.create(field.resolveType(schema), fieldValue.source)
      }
      // 16-Apr-23:
      // Anonymous types for fields are now on the field directly.
      // But, I'm not sure if the fieldProjectionType is always ths same as the fieldType
      val projectedType =
         if (field.anonymousType != null && field.anonymousType.name.parameterizedName == field.fieldProjection!!.projectedType.toVyneQualifiedName().parameterizedName) {
            field.anonymousType
         } else {
            schema.type(field.fieldProjection!!.projectedType)
         }

      return project(
         fieldValue,
         field.fieldProjection,
         projectedType,
         schema,
         this.nullValues,
         source,
         field.format,
         field.nullable,
         true
      )
   }

   override fun project(
      valueToProject: TypedInstance,
      projection: FieldProjection,
      targetType: Type,
      schema: Schema,
      nullValues: Set<String>,
      source: DataSource,
      format: FormatsAndZoneOffset?,
      nullable: Boolean,
      allowContextQuerying: Boolean
   ): TypedInstance {
      // MP: 26-Feb-24: We now support multiple scoped facts in the projection context.
      // However, it's not obvious how to handle this here.
      // Let's fail for now, then implement once the use-case is better understood.
      require(projection.projectionFunctionScope.size <= 1) { "How to handle multiple scoped facts here?" }
      val projectedFieldValue = if (valueToProject is TypedCollection && targetType.isCollection) {
         // Project each member of the collection seperately
         valueToProject
            .parallelStream()
            .map { collectionMember ->
               newFactory(
                  targetType.collectionType!!,
                  collectionMember,
                  scope = projection.projectionFunctionScope.firstOrNull()
               )
                  .build()
            }.collect(Collectors.toList())
            .let { projectedCollection ->
               // Use arrayOf (instead of from), as the collection may be empty, so we want to be explicit about it's type
               TypedCollection.arrayOf(targetType.collectionType!!, projectedCollection, source)
            }
      } else {
         newFactory(targetType, valueToProject, scope = projection.projectionFunctionScope.firstOrNull()).build()
      }
      return projectedFieldValue
   }


   fun newFactoryWithOnly(
      type: Type,
      newValue: FactBag,
      scope: ProjectionFunctionScope? = null
   ): TypedObjectFactory {
      return TypedObjectFactory(
         type,
         newValue,
         schema,
         nullValues,
         source,
         objectMapper,
         functionRegistry,
         evaluateAccessors,
         inPlaceQueryEngine,
         accessorHandlers,
         formatSpecs,
         parsingErrorBehaviour,
         functionResultCache,
         scope,
         factBagSearchStrategy = factBagSearchStrategy,
         parsingOptions = parsingOptions
      )
   }

   /**
    * Returns a new TypedObjectFactory,
    * merging the current set of known values with the newValue if possible.
    */
   fun newFactory(
      type: Type,
      newValue: Any,
      factsToExclude: Set<TypedInstance> = emptySet(),
      // TODO : 20-01-25: I suspect this should be List<ProjectionFunctionScope>, as
      // I'm pretty sure we support multiple scoped variables here.
      scope: ProjectionFunctionScope?
   ): TypedObjectFactory {


      val newMergedValue = when {
         this.value is FactBag && newValue is TypedInstance -> {
            if (scope != null) {

               // MP: 20-Jan-25:
               // Added this, as we need to call ProjectionFunctionScopeEvaluator.build
               // to consistently convert scope into ScopedFacts (previously we were
               // constructing inline, without evaluating an expression on the scope)
               // Not sure if this is actually a problem -- could we hit this without a query engine?
               require(inPlaceQueryEngine != null) { "Cannot evaluate scope $scope as no query engine is present" }
               val primaryFacts: List<TypedInstance> = listOf(newValue)
               val inputs: List<Argument> = listOf(scope)
               val scopedFacts = ProjectionFunctionScopeEvaluator.build(
                  inputs,
                  primaryFacts,
                  this.inPlaceQueryEngine
               )

               // old way:
//               val oldScopedFact = ScopedFact(scope, newValue)
//               if (oldScopedFact != scopedFacts.single()) {
//                  println("This should be removed.")
//               }


               CascadingFactBag(
                  CopyOnWriteFactBag(CopyOnWriteArrayList(), scopedFacts, schema),
                  this.value
               )
            } else {
               CascadingFactBag(CopyOnWriteFactBag(newValue, schema), this.value)
            }
         }

         this.value is FactBag && newValue is FactBag -> {
            // Validate that the factbag has correctly defined it's scope.
            if (scope != null) {
               require(newValue.scopedFacts.any { it.scope == scope }) {
                  "The new factbag has been prepared incorrectly, as it doesn't contain a scoped value matching the provided scope"
               }
            }
            CascadingFactBag(newValue, this.value)

         }

         this.value !is FactBag && factsToExclude.isNotEmpty() -> error("Cannot exclude facts when the source of facts is not a FactBag")
         else -> newValue
      }

      return TypedObjectFactory(
         type,
         newMergedValue,
         schema,
         nullValues,
         source,
         objectMapper,
         functionRegistry,
         evaluateAccessors,
         inPlaceQueryEngine,
         accessorHandlers,
         formatSpecs,
         parsingErrorBehaviour,
         functionResultCache,
         scope,
         factBagSearchStrategy = factBagSearchStrategy,
         parsingOptions = parsingOptions
      )
   }

   suspend fun buildAsync(decorator: suspend (attributeMap: Map<AttributeName, TypedInstance>) -> Map<AttributeName, TypedInstance> = { attributesToMap -> attributesToMap }): TypedInstance {
      if (isJson(value)) {
         val jsonParsedStructure = JsonParsedStructure.from(value as String, objectMapper)
         return TypedInstance.from(
            type,
            jsonParsedStructure,
            schema,
            nullValues = nullValues,
            source = source,
            evaluateAccessors = evaluateAccessors,
            functionRegistry = functionRegistry,
            formatSpecs = formatSpecs,
            parsingErrorBehaviour = parsingErrorBehaviour,
            valueSuppliers = valueSuppliers
         )
      }

      // TODO : Naieve first pass.
      // This approach won't work for nested objects.
      // I think i need to build a hierachy of object factories, and allow nested access
      // via the get() method
      val mappedAttributes = attributesToMap.map { (attributeName) ->
         // The value may have already been populated on-demand from a conditional
         // field set evaluation block, prior to the iterator hitting the field
         attributeName to getOrBuild(attributeName)
      }.toMap()

      val attributes = decorator(mappedAttributes)
      return TypedObject(type, attributes, source, metadata)
   }

   fun build(decorator: (attributeMap: Map<AttributeName, TypedInstance>) -> Map<AttributeName, TypedInstance> = { attributesToMap -> attributesToMap }): TypedInstance {
      return timeBucket("Build ${this.type.name.shortDisplayName}") {
         doBuild(decorator)
      }
   }


   private fun readWithFormatSpecDeserializer(
      paramMetadata: Metadata,
      modelFormatSpec: ModelFormatSpec
   ): TypedInstance {
      val (parsedValue, parsedType) = modelFormatSpec.deserializer.parse(value, type, paramMetadata, schema, source)
         .let { parsedValue ->
            when {
               // When parsing CSV, we may provide the type as T, and get back T[]
               // This is a legacy behaviour, and should be removed.
               parsedValue is Collection<*> && parsedValue.size > 1 && !type.isCollection -> parsedValue to type.asArrayType()

               // The parser will generally return a collection.
               // If the collection has a single element, and the requested type isn't a collection, unwrap the collection
               parsedValue is Collection<*> && parsedValue.size == 1 && !type.isCollection -> parsedValue.single() to type

               else -> parsedValue to type
            }
         }

      return TypedInstance.from(
         parsedType,
         parsedValue,
         schema,
         source = source,
         evaluateAccessors = evaluateAccessors,
         functionRegistry = functionRegistry,
         formatSpecs = formatSpecs,
         inPlaceQueryEngine = inPlaceQueryEngine,
         parsingErrorBehaviour = parsingErrorBehaviour,
         metadata = metadata,
         valueSuppliers = valueSuppliers
      )
   }

   private fun doBuild(decorator: (attributeMap: Map<AttributeName, TypedInstance>) -> Map<AttributeName, TypedInstance> = { attributesToMap -> attributesToMap }): TypedInstance {
      val metadataAndFormat = formatDetector.getFormatType(type)
      if (metadataAndFormat != null) {
         val (metadata, modelFormatSpec) = metadataAndFormat
         if (modelFormatSpec.deserializer.canParse(value, metadata, type)) {
            return readWithFormatSpecDeserializer(metadata, modelFormatSpec)
         }
      }
      if (value is FactBag && value.hasFactOfType(type, factBagSearchStrategy)) {
         return value.getFact(type, factBagSearchStrategy)
      }
      if (isJson(value)) {
         val jsonParsedStructure = JsonParsedStructure.from(value as String, objectMapper)
         return TypedInstance.from(
            type,
            jsonParsedStructure,
            schema,
            nullValues = nullValues,
            source = source,
            evaluateAccessors = evaluateAccessors,
            functionRegistry = functionRegistry,
            formatSpecs = formatSpecs,
            inPlaceQueryEngine = inPlaceQueryEngine,
            parsingErrorBehaviour = parsingErrorBehaviour,
            metadata = metadata,
            valueSuppliers = valueSuppliers
         )
      }

      if (type.isCollection && CollectionReader.canRead(type, value)) {
         return CollectionReader.readCollectionFromNonTypedCollectionValue(
            type,
            value,
            schema,
            source,
            functionRegistry,
            inPlaceQueryEngine,
            metadata
         )
      }
      if (type.isScalar && type.hasExpression) {
         return evaluateExpressionType(type, null)
      }

      // the value is FactBag line here is a bit of a hack..
      // We want to build closed objects when deserializing a result.
      // However we don't currently have an easy way to pass that flag in.
      // It's unlikely we're serializing results using a FactBag.
      val typeIsConstructable = when {
         type.isClosed && !type.isParameterType -> false
         type.isClosed && type.isParameterType && constructClosedParameterTypes -> true
         else -> true
      }
      if (!typeIsConstructable && value is FactBag) {
         logger.debug { "Not attempting to build ${type.name.shortDisplayName} as it is closed - triggering search" }
         return queryForParentType()
      }

      // Attempt to build / discover each attribute
      val mappedAttributes = attributesToMap.map { (attributeName) ->
         // The value may have already been populated on-demand from a conditional
         // field set evaluation block, prior to the iterator hitting the field
         attributeName to xtimed("build attribute $attributeName") { getOrBuild(attributeName) }
      }.toMap()

      val decorated = xtimed("apply decorator") { decorator(mappedAttributes) }
      return TypedObject(type, decorated, source, metadata)
   }

   /**
    * Called because the parent type is closed, so construction isn't possible
    */
   private fun queryForParentType(): TypedInstance {
      if (inPlaceQueryEngine == null) {
         return TypedNull.create(
            type,
            FailedSearch("Cannot search for type ${type.name.shortDisplayName} as no query engine is provided, and constructing is disallowed because the type is closed")
         )
      }
      val searchFailureBehaviour: QueryFailureBehaviour = QueryFailureBehaviour.defaultBehaviour(type)
      return runBlocking {
         logger.debug { "Initiating query to search for closed type ${type.name.shortDisplayName}" }
         queryForType(type, searchFailureBehaviour, AlwaysGoodSpec, attributeName = null)
      }


   }

   private fun getOrBuild(attributeName: AttributeName, allowAccessorEvaluation: Boolean = true): TypedInstance {
      // Originally we used a concurrentHashMap.computeIfAbsent { ... } approach here.
      // However, functions on accessors can access other fields, which can cause recursive access.
      // Therefore, migrated to using initializers with kotlin Lazy functions
      val initializer = fieldInitializers[attributeName]
         ?: error("Cannot request field $attributeName as no initializer has been prepared")

      val accessorEvaluationWasSupressed = accessorEvaluationSupressed
      accessorEvaluationSupressed = !allowAccessorEvaluation

      // Reading the value will trigger population the first time.
      val value = initializer.value

      accessorEvaluationSupressed = accessorEvaluationWasSupressed
      return value
   }

   override fun getScopedFact(scope: Argument): TypedInstance {
      if (value is FactBag) {
         return value.getScopedFact(scope).fact
      } else {
         error("No scope of ${scope.name} exists on the provided value. (Was passed a value of type ${value::class.simpleName})")
      }
   }

   override fun getScopedFactOrNull(scope: Argument): TypedInstance? {
      return if (value is FactBag) {
         return value.getScopedFactOrNull(scope)?.fact
      } else {
         null
      }
   }

   override fun withAdditionalScopedFacts(scopedFacts: List<ScopedFact>): TypedObjectFactory {
      return when (value) {
         is FactBag -> newFactory(
            this.type, value.withAdditionalScopedFacts(scopedFacts, this.schema),
            scope = this.projectionScope
         )

         is TypedInstance -> newFactory(
            this.type, FactBag.of(this.value, this.schema).withAdditionalScopedFacts(scopedFacts, this.schema),
            scope = this.projectionScope
         )

         else -> {
            error("Cannot append scoped facts to the evaluation context, as the provided source fact is neither a TypedInstance nor a FactBag")
         }
      }

   }


   /**
    * Returns a value looked up by it's type
    */
   override fun getValue(
      typeName: QualifiedName,
      queryIfNotFound: Boolean,
      allowAccessorEvaluation: Boolean,
      constraints: List<Constraint>
   ): TypedInstance {
      val requestedType = schema.type(typeName)
      // MP - 2-Nov-21:  Added to allow seart
      val fromFactBag = currentValueFactBag.getFactOrNull(
         FactSearch.findType(
            requestedType,
            strategy = factBagSearchStrategy
         )
      )
      if (fromFactBag != null) {
         return fromFactBag
      }
      val candidateTypes = this.type.attributes.filter { (name, field) ->
         val fieldType = field.resolveType(schema)
         fieldType.isAssignableTo(requestedType)
      }
      return when (candidateTypes.size) {
         0 -> {
            if (requestedType.hasExpression) {
               // If the type is an expression, we may be able to calculate it, even though the
               // value wasn't explictly present.
               // Potential for stack overflow here -- might need to do some recursion checking
               // that prevents self-referential loops.
               evaluateExpressionType(typeName)
            } else {
               handleTypeNotFound(requestedType, queryIfNotFound, constraints)
            }
         }

         1 -> getValue(candidateTypes.keys.first(), allowAccessorEvaluation)
         else -> TypedNull.create(
            requestedType, FailedEvaluatedExpression(
               typeName.fullyQualifiedName,
               emptyList(),
               "Ambiguous type request for type ${typeName.parameterizedName} - there are ${candidateTypes.size} matching attributes: ${candidateTypes.keys.joinToString()}"
            )
         )
      }
   }

   private fun handleTypeNotFound(
      requestedType: Type,
      queryIfNotFound: Boolean,
      constraints: List<Constraint>
   ): TypedInstance {
      fun createTypedNull(
         errorMessage: String = "No attribute with type ${requestedType.name.parameterizedName} is present on type ${this.type.name.parameterizedName}"
      ): TypedNull {
         return TypedNull.create(
            requestedType, FailedEvaluatedExpression(
               requestedType.name.fullyQualifiedName,
               emptyList(),
               errorMessage
            )
         )
      }
      return when {
         queryIfNotFound && inPlaceQueryEngine != null -> {
            // TODO : Remove the blocking behaviour here.
            // TypedObjectFactory has always been blocking (but
            // historically hasn't invoked services), so leaving as
            // blocking when introducing type expressions with lookups.
            // However, in future, we need to mkae the TypedObjectFactory
            // async up the chain.
            runBlocking {
               if (requestedType.isStream) {
                  error("Cannot perform an inner search for a stream")
               }
               val resultsFromSearch = try {
                  // MP: 29-Jan-25: Found that facts from the context
                  // are not being passed when doing an in-place search
                  // So, in a nested projection, we were not searching with the facts
                  // from the current scope.
                  val queryEngine = if (value is FactBag) {
                     inPlaceQueryEngine.withAdditionalFacts(value.rootFacts(), value.scopedFacts)
                  } else {
                     inPlaceQueryEngine
                  }
                  queryEngine.findType(requestedType, constraints = constraints)
                     .toList()
               } catch (e: Exception) {
                  // OrbitalQueryException comes from a policy expression: e.g.when below `else` branch is triggered:
                  /*policy AdminRestrictedInstrument against Instrument  (userInfo : UserInfo) -> {
                     read {
                        when {
                           userInfo.roles.contains('QueryRunner') -> Instrument
                           else -> throw((NotAuthorizedError) { message: 'Not Authorized' })
                        }
                     }
                  }*/
                  // so, we need to catch OrbitalQueryException and re-throw to halt the execution.
                  // a query like find { fist(Instrument[]) } against the above policy would hit the debug point placed here.
                  if (e is OrbitalQueryException) {
                     throw e
                  }

                  // handle com.orbitalhq.query.UnresolvedTypeInQueryException
                  emptyList()
               }
               when {
                  resultsFromSearch.isEmpty() -> createTypedNull(
                     "No attribute with type ${requestedType.name.parameterizedName} is present on type ${type.name.parameterizedName} and attempts to discover a value from the query engine failed"
                  )

                  resultsFromSearch.size == 1 && !requestedType.isCollection -> resultsFromSearch.first()
                  resultsFromSearch.size >= 1 && requestedType.isCollection -> TypedCollection.from(
                     resultsFromSearch,
                     MixedSources.singleSourceOrMixedSources(resultsFromSearch)
                  )

                  resultsFromSearch.size > 1 && !requestedType.isCollection -> {
                     val errorMessage =
                        "Search for ${requestedType.name.shortDisplayName} returned ${resultsFromSearch.size} results, which is invalid for non-array types. Returning null."
                     logger.warn { errorMessage }
                     createTypedNull(errorMessage)
                  }

                  else -> createTypedNull(
                     "No attribute with type ${requestedType.name.parameterizedName} is present on type ${type.name.parameterizedName} and attempts to discover a value from the query engine returned ${resultsFromSearch.size} results.  Given this is ambiguous, returning null"
                  )
               }

            }

         }

         queryIfNotFound && inPlaceQueryEngine == null -> {
            logger.debug { "Requested to use queryEngine to lookup value ${requestedType.qualifiedName.parameterizedName} but no query engine was provided.  Returning null" }
            createTypedNull()
         }

         else -> {
            createTypedNull()
         }
      }
   }


   private fun getValue(attributeName: AttributeName, allowAccessorEvaluation: Boolean): TypedInstance {
      return getOrBuild(attributeName, allowAccessorEvaluation)
   }

   /**
    * Returns a value looked up by it's name
    */
   override fun getValue(attributeName: AttributeName): TypedInstance {
      return getValue(attributeName, allowAccessorEvaluation = true)
   }

   override fun readAccessor(type: Type, accessor: Accessor, format: FormatsAndZoneOffset?): TypedInstance {
      // MP 24-Jan-25:
      // Changed this to allowContextQuerying = true.
      // Otherwise, types on projections-with-expressions were not triggering querying - eg:
      // find { Film } as {
      //   id : FilmId
      //   // Here, CastResponse is supposed to be invoked via an operation.
      //   cast : CastResponse as Actor[] as (actor:Actor) -> {
      //      personName : PersonName
      //   }[]
      //}
      return accessorReader.read(
         value,
         type,
         accessor,
         schema,
         source = source,
         format = format,
         allowContextQuerying = true
      )
   }

   override fun readAccessor(
      type: QualifiedName,
      accessor: Accessor,
      nullable: Boolean,
      format: FormatsAndZoneOffset?
   ): TypedInstance {
      return accessorReader.read(
         value,
         type,
         accessor,
         schema,
         nullValues,
         source = source,
         nullable = nullable,
         allowContextQuerying = true,
         format = format
      )
   }

   fun evaluateExpressionType(expressionType: Type, format: FormatsAndZoneOffset?): TypedInstance {
      val expression = expressionType.expression!!
      return if (expression is LambdaExpression) {
         // Lambda Expression types are evaluated more like functions.
         // Lambda expression inputs are declared independent of queries where they're evaluated
         // eg:
         // type AllowedFilms by (Film[], viewerAge:Age) -> Film[].filter( (Film) -> Film::Age > viewerAge )
         //
         // To evaluate this, we need to create a separate evaluation context
         // where variable names are scoped relative to the expression types.
         // Enapsulate the logic in a dedicated function
         return evaluateLambdaExpression(expression, format)
      } else {
         accessorReader.evaluate(value, expressionType, expression, schema, nullValues, source, format)
      }

   }

   fun evaluateLambdaExpression(expression: LambdaExpression, format: FormatsAndZoneOffset?): TypedInstance {
      // Lambda Expression types are evaluated more like functions.
      // Their inputs are declared independent of queries where they're evaluated
      // eg:
      // type AllowedFilms by (Film[], viewerAge:Age) -> Film[].filter( (Film) -> Film::Age > viewerAge )
      //
      // To evaluate this, we need to create a separate evaluation context
      // where variable names are scoped relative to the expression types.
      // Enapsulate the logic in a dedicated function
      // Lambda Expression types are evaluated more like functions.
      // Lambda expression inputs are declared independent of queries where they're evaluated
      // eg:
      // type AllowedFilms by (Film[], viewerAge:Age) -> Film[].filter( (Film) -> Film::Age > viewerAge )
      //
      // To evaluate this, we need to create a separate evaluation context
      // where variable names are scoped relative to the expression types.
      // eg: a query that declares a variable named "viewerAge" in the given clause
      // should not cause a conflict based on name.
      //
      // given { viewerAge : SomeOtherType }
      // find { AllowedFilms } // viewerAge means something different here.
      //
      // Likewise, when trying to resolve viewerAge to pass into the type expression, we don't
      // actually want to look for things named viewerAge - that name only has meaning WITHIN
      // the evaluation context of the expression type.
      // MP: 24-Sep-24
      // Actually, we DO need to resolve inputs here, as the semantics of the scopes
      // change as we let things evaluate.
      val valueSupplier = when (value) {
         is FactBag -> FactBagValueSupplier(value, schema, this)
         is TypedInstance -> FactBagValueSupplier.of(listOf(value), schema, this)
         else ->
            // How do we evaluate the args?
            error("TODO - LambdaExpression evaluation requires a FactBag as value, but have ${value::class.simpleName}")
      }

      val inputs = expression.inputs.map { argument ->
         // If the argument isn't explicitly resolved, we try to resolve
         // it by type from the object factory.
         // This is to handle scenarios like:
         // type AllowedFilms by (Film[], viewerAge:Age) -> Film[].filter( (Film) -> Film::Age > viewerAge )
         // where viewerAge can be supplied in a given {} clause.
         if (argument is ProjectionFunctionScope && argument.expression is TypeExpression && (argument.expression as TypeExpression).constraints.isNotEmpty()) {
            // The inputs into this expression have constraints defined.
            // We can't search directly for the type, we need to do a search for the type with the provided constraints.
            val argumentTypeExpression = argument.expression as TypeExpression
            // TODO : Fix runblocking, but that requires a big change, and not sure the direction of travel
            // wrt/ coroutines vs flux atm.
            val argumentExpressionReturnType = schema.type(argumentTypeExpression.type)
            val scopedFacts = this.getCurrentScopedFacts()
            val typedInstance = runBlocking {
               // If we're doing nested traversal of lambda expressions,
               // there could be scoped facts we've been passed that will
               // be needed as inputs

               val queryEngineWithScopedFacts =
                  valueSupplier.inPlaceQueryEngine!!.withAdditionalFacts(emptyList(), scopedFacts)
               val collectedList = queryEngineWithScopedFacts.findType(
                  argumentExpressionReturnType, constraints = argumentTypeExpression.constraints
               ).toList()
               when {
                  argumentExpressionReturnType.isCollection -> TypedCollection.from(collectedList)
                  collectedList.isEmpty() -> TypedNull.create(argumentExpressionReturnType)
                  collectedList.size == 1 -> collectedList.single()
                  else -> {
                     error("Expected a single value returned for the expression type ${argumentTypeExpression}, but got ${collectedList.size}")
                  }
               }

            }
//            val typedInstance = TypedInstance.from(argumentExpressionReturnType, result, schema)
            ScopedFact(argument, typedInstance)
         } else {
            val argumentValue = valueSupplier.getScopedFactOrNull(argument)
               ?: getValue(argument.type.toVyneQualifiedName(), queryIfNotFound = true)
            ScopedFact(argument, argumentValue)
         }
      }

      // FactBag.empty() seems a big call here.
      // But if we change it, we need to prune the facts that were used previously
      // ie., if a global fact is declared (in a given), and we assign it to an argument
      // then it can't also be present without the argument scope, or it'll pollute the scope
      // with duplicate values.
      val inputsForLambda = FactBag.empty().withAdditionalScopedFacts(inputs, schema)
      val evaluationContext = newFactory(type, inputsForLambda, scope = null)

      // If the expression contains a nested lambda (ie., with inputs), then we recurse into
      // that expression to collection inputs etc...
      val result = if (expression.expression is LambdaExpression) {
         evaluationContext.evaluateLambdaExpression(expression.expression as LambdaExpression, format)
      } else {
         //...otherwise, we just evaluate the expression
         evaluationContext.accessorReader.evaluate(
            inputsForLambda,
            schema.type(expression.returnType),
            expression,
            schema,
            nullValues,
            source,
            format
         )
      }
      return result
   }

   fun evaluateExpression(expression: Expression): TypedInstance {
      return accessorReader.evaluate(
         value,
         schema.type(expression.returnType),
         expression,
         schema,
         nullValues,
         source,
         null
      )
   }

   private fun evaluateExpressionType(typeName: QualifiedName): TypedInstance {
      val type = schema.type(typeName)
      return evaluateExpressionType(type, null)
   }


   private fun buildField(field: Field, attributeName: AttributeName): TypedInstance {
      // When we're building a field, if there's a projection on it,
      // we build the source type initially.  Once the source is built, we
      // then project to the target type.
      val (fieldType, constraints) = if (field.fieldProjection != null) {
         // MP: 29-Nov-24:
         // Two different scenarios for constraints here:
         // Constraint on expression as input to projection:
         // existing : Deal[] = ExistingDeals(BorrowerId == deal.borrowerId) as Deal[]
         //
         // or constraint on source type:
         // repayments: Repayment[]( LoanId == Loan::LoanId ) as  (repayments:Repayment) -> {
         // There are tests that cover both of these scenarios
         schema.type(field.fieldProjection.sourceType) to field.constraints + field.fieldProjection.sourceTypeConstraints
      } else {
         field.resolveType(schema) to field.constraints
      }
      val fieldTypeName = fieldType.qualifiedName

      // We don't always want to use accessors.
      // When parsing content from a cask, which has already been processed, what we
      // receive is a TypedObject.  The accessors should be ignored in this scenario.
      // By default, we want to cosndier them.
      val considerAccessor =
         field.accessor != null && evaluateAccessors && !accessorEvaluationSupressed && field.accessor.enabledForValueType(
            value
         )
      val evaluateTypeExpression = fieldType.hasExpression && evaluateAccessors
      val valueSupplier = valueSuppliers.firstOrNull { supplier ->
         supplier.canSupply(field, fieldType)
      }

      // Support embedded formats.
      // Eg: A field in a JSON message that contains an XML payload.
      // Or a field in Protobuf that contains JSON, etc etc
      val fieldModelFormatSpecPair = formatDetector.getFormatType(fieldType)

      // Questionable design choice: Favour directly supplied values over accessors and conditions.
      // The idea here is that when we're reading from a file or non parsed source, we need
      // to know how to construct the instance.
      // However, if that work has already been done, and we're trying to rebuild the instance
      // from a parsing result, we need to be able to.
      // Therefore, if we've been directly supplied the value, use it.
      // Otherwise, look to leverage conditions.
      // Note - revisit if this proves to be problematic.
      return when {
         valueSupplier != null -> valueSupplier.supplyValue(field, fieldType, schema, source, this)

         // Cheaper readers first
         value is CSVRecord && field.accessor is ColumnAccessor && considerAccessor -> {
            readAccessor(fieldTypeName, field.accessor, field.nullable, field.format)
         }
//         modelFormatSpecPair != null && modelFormatSpecPair.second.deserializer.canParse(value, modelFormatSpecPair.first) -> {
//            val (metadata,formatSpec) = modelFormatSpecPair
//            readWithFormatSpecDeserializer(metadata,formatSpec)
//         }

         // ValueReader can be expensive if the value is an object,
         // so only use the valueReader early if the value is a map
         // MP 19-Nov-20: field.accessor null check had been added here to fix a bug, but I can't remember what it was.
         // However, the impact of adding it is that when parsing TypedObjects from remote calls that have already been
         // processed (and so the accessor isn't required) means that we fall through this check and try using the
         // accessor, which will fail, as this isn't raw content anymore, it's parsed / processed.
         value is Map<*, *> && !considerAccessor && valueReader.contains(value, attributeName) -> readWithValueReader(
            attributeName,
            fieldType,
            field.format
         )

         // This is not nice, but we're trying to solve the following problem where a model has a column accessor, e.g.:
         // model Foo { isin: by column("ISIN") }
         // and we have a rest operation returning Foo as a json, i.e.:
         // { "isin": "IT0003123" }
         // return value of this service can be parsed into 'Foo' without below.
         value is JsonParsedStructure && considerAccessor && field.accessor !is JsonPathAccessor && valueReader.contains(
            value,
            attributeName
         ) -> readWithValueReader(attributeName, fieldType, field.format)

         considerAccessor -> {
            readAccessor(field.resolveType(schema), field.accessor!!, field.format)
         }

         evaluateTypeExpression -> {
            evaluateExpressionType(fieldTypeName)
         }

         field.readCondition != null -> {
            conditionalFieldSetEvaluator.evaluate(
               "What do I pass here?",
               field.readCondition,
               attributeName,
               fieldType,
               UndefinedSource
            )
         }
         // Not a map, so could be an object, try the value reader - but this is an expensive
         // call, so we defer to last-ish
         valueReader.contains(value, attributeName) && constraints.isNullOrEmpty() -> readWithValueReader(
            attributeName,
            fieldType,
            field.format
         )

         // Support embedded formats.
         // Eg: A field in a JSON message that contains an XML payload.
         // Or a field in Protobuf that contains JSON, etc etc
         // This comes AFTER the valueReader approach, beceause
         // if we're not a Map<>, and we don't have a valueReader, there's no way
         // of plucking just the field - it's possible the whole message is just this field
         // (eg., an avro message composed into a field of an object, with metadata on the other fields)
         fieldModelFormatSpecPair != null -> readFieldWithFormatSpecDeserializer(
            field,
            fieldType,
            fieldModelFormatSpecPair
         )

         // Is there a default?
//         field.defaultValue != null -> TypedValue.from(
//            fieldType,
//            field.defaultValue,
//            ConversionService.DEFAULT_CONVERTER,
//            source = DefinedInSchema,
//            parsingErrorBehaviour
//         )

         // 2-Nov-22: Added this when trying to build inline
         // projections.  However, concerned about knock-on effects...
         value is FactBag -> {

            // The rationale here is if I asked for Foo[], I want all the Foo's,
            // not just a single collection.
            val searchStrategy = if (fieldType.isCollection) {
               FactDiscoveryStrategy.ANY_DEPTH_ALLOW_MANY
            } else {
               factBagSearchStrategy
            }
            val searchedValue = value.getFactOrNull(
               FactSearch.findType(
                  fieldType,
                  strategy = searchStrategy
               )
            )
            when {
               searchedValue != null -> searchedValue
               // Since we know that the value isn't present in the fact bag,
               // and that it's scalar, the only thing left to do is query.
               fieldType.isScalar -> queryForFieldValue(field, fieldType, attributeName, constraints)

               fieldType.isCollection -> {
                  // TODO : I suspect this needs to be richer.
                  // Currently supporting the use case of picking a collection
                  // from a query result onto a field.  (see
                  // VyneProjectionTest.will populate collection on inline projection)
                  // However, there's likely other nuanced cases we need to support.
                  queryForFieldValue(field, fieldType, attributeName, constraints)

               }

               else -> {
                  // BugFix: Only attempt to build a field object if the fieldType isn't scalar.
                  // Otherwise, we're calling into TypedObjectFactory with a scalar type,
                  // which is incorrect (it's intended for Object types).
                  attemptToBuildFieldObject(fieldType, constraints)
                     ?: queryForFieldValue(field, fieldType, attributeName, constraints)
               }
            }
         }

         else -> queryForFieldValue(field, fieldType, attributeName, constraints)
      }
         .let { value ->
            if (!constraints.isNullOrEmpty()) {
               verifyValueSatisfiesConstraints(value, field, type, attributeName, constraints)
            } else {
               value
            }
         }
         .let { value ->
            // If there was a format provided (ie., a date format), and it doesn't match,
            // apply it now
            if (value is TypedValue && value.format != field.format) {
               value.copy(format = field.format)
            } else {
               value
            }
         }
   }

   /**
    * A field was defined with a constraint.
    * We need to ensure that the value we're using
    * satisfies that constraint. Otherwise, search for it.
    */
   private fun verifyValueSatisfiesConstraints(
      value: TypedInstance,
      field: Field,
      type: Type,
      attributeName: AttributeName,
      constraints: List<Constraint>
   ): TypedInstance {
      val validator = DataSourceConstraintValidator()
      return if (!validator.sourceSatisfiesConstraint(constraints, value, schema)) {
         queryForFieldValue(field, field.resolveType(schema), attributeName, constraints)
      } else {
         value
      }
   }

   private fun readFieldWithFormatSpecDeserializer(
      field: Field,
      fieldType: Type,
      fieldModelFormatSpecPair: Pair<Metadata, ModelFormatSpec>
   ): TypedInstance {

      // Scenario:
      // We're deserializing a message - the message isn't something we can traverse the values of
      // (such as a Map, or something with a valueReader), so it's probably something like a bytearray.
      // Therefore, we attempt to read the entire body value.
      // This would typically happen when you've got something like an Avro / Protobuf message on-the-wire,
      // but the Taxi model is composing it into another model, adding things like metadata (eg: Kafka topic / headers)
      return readWithFormatSpecDeserializer(value, fieldModelFormatSpecPair, fieldType)
   }

   private fun readWithFormatSpecDeserializer(
      valueToRead: Any,
      modelFormatSpecPair: Pair<Metadata, ModelFormatSpec>,
      type: Type
   ): TypedInstance {
      val (metadata, modelFormatSpec) = modelFormatSpecPair
      val parsed = when {
         // "canParse" here can indicate "is any more deserializaiton required?"
         modelFormatSpec.deserializer.canParse(valueToRead, metadata, type) -> modelFormatSpec.deserializer.parse(
            valueToRead,
            type,
            metadata,
            schema,
            source
         )

         else -> valueToRead
      }
      return when (parsed) {
         is TypedInstance -> parsed
         else -> TypedInstance.from(type, parsed, schema)
      }
   }

   /**
    * HACK.
    * If we're building a field, and the field is an anonymous object, and we couldn't
    * find a match directly in the facts (which we almost never would, b/c it's an anonymous inlined typed),
    * then we need to build the object.
    *
    * This is a hack, because "build an object" is already handled by the ObjectBuilder.
    * We should be calling out to that to perform this task.
    *
    * That creates challenges, as we need to pass the ObjectBuilder in the TypedObjectFactory,
    * which becomes recursive.
    */
   private fun attemptToBuildFieldObject(
      fieldType: Type,
      constraints: List<Constraint>
   ): TypedInstance? {
      if (type.isScalar) {
         return null
      }
      // Don't try to build something that has constraints - that needs to be
      // resolved by querying
      if (constraints.isNotEmpty()) {
         return null
      }
      val result = newFactory(fieldType, this.value, scope = projectionScope).build()
      return if (result is TypedNull) {
         null
      } else {
         result
      }
   }

   // See also doFind() in QueryEngine, which provides more customizable behaviour for handling failures.
   private fun failWithTypedNull(
      fieldType: Type,
      attributeName: AttributeName,
      message: String = "Can't populate attribute $attributeName on type ${type.name} as no attribute or expression was found on the supplied value of type ${value::class.simpleName}"
   ): TypedNull {
      return TypedNull.create(
         fieldType,
         ValueLookupReturnedNull(
            message,
            type.name
         )
      )
   }

   private fun queryForFieldValue(
      field: Field,
      type: Type,
      attributeName: AttributeName,
      constraints: List<Constraint>
   ): TypedInstance {
      return if (inPlaceQueryEngine != null) {
         val searchFailureBehaviour: QueryFailureBehaviour = QueryFailureBehaviour.defaultBehaviour(field)
         val fieldInstanceValidPredicate = buildSpecProvider.provide(field)
         queryForType(
            type,
            searchFailureBehaviour,
            fieldInstanceValidPredicate,
            attributeName,
            constraints
         )
      } else {
         failWithTypedNull(type, attributeName)
      }
   }

   private fun queryForType(
      searchType: Type,
      searchFailureBehaviour: QueryFailureBehaviour,
      instanceValidPredicate: TypedInstanceValidPredicate,
      attributeName: AttributeName?, // null if searching for top-level type
      constraint: List<Constraint> = emptyList()
   ): TypedInstance {
      require(inPlaceQueryEngine != null)
      fun failWithTypedNull(failureMessage: String): TypedNull {
         return if (attributeName != null) {
            failWithTypedNull(
               searchType,
               attributeName,
               failureMessage
            )
         } else {
            TypedNull.create(searchType, ValueLookupReturnedNull(failureMessage, searchType.name))
         }
      }

      val (additionalFacts, additionalScope) = getFactsInScopeForSearch()
      val buildResult = runBlocking {
         logger.debug { "Initiating query to search for attribute $attributeName (${searchType.name.shortDisplayName})" }
         inPlaceQueryEngine.withAdditionalFacts(additionalFacts, additionalScope)
            .findType(
               searchType,
               instanceValidPredicate,
               PermittedQueryStrategies.EXCLUDE_BUILDER_AND_MODEL_SCAN,
               searchFailureBehaviour,
               constraint
            )
            .toList()
      }
      val attributeNameErrorMessagePart = if (attributeName != null) {
         " (attempting to build attribute $attributeName of ${this.type.name}"
      } else ""
      return when {
         searchType.isCollection -> {
            if (isEmptyCollectionBuildResult(buildResult, searchFailureBehaviour)) {
               // Unwrap the array
               TypedCollection.empty(searchType)
            } else if (isNullListBuildResult(buildResult, searchFailureBehaviour)) {
               val failureMessage =
                  "Searching for ${searchType.name.shortDisplayName}${attributeNameErrorMessagePart} failed"
               failWithTypedNull(failureMessage)

            } else {
               TypedCollection.arrayOf(searchType.collectionType!!, buildResult.filter { it !is TypedNull })
            }
         }

         buildResult.isEmpty() -> {
            val message = "Searching for ${searchType.name.shortDisplayName}${attributeNameErrorMessagePart} failed"
            failWithTypedNull(message)
         }

         buildResult.size == 1 -> buildResult.single()
         else -> {
            val message =
               "Querying to find type ${searchType.name.shortDisplayName}${attributeNameErrorMessagePart} returned ${buildResult.size} results, which is ambiguous.  Returning null"
            logger.debug { message }
            failWithTypedNull(message)
         }
      }
   }

   /**
    * When we build with SEND_TYPED_NULL_OR_EMPTY_ARRAY and the result can't be found,
    * the internal builder returns an empty array.
    *
    * However, because this is inside a flow that has a .toList(), we end up with a nested list - ie: [[]]
    *
    * We check here to unwrap.
    *
    * However, this feels like a smell, and maybe there's a smarter way
    *
    */
   private fun isEmptyCollectionBuildResult(
      buildResult: List<TypedInstance>,
      searchFailureBehaviour: QueryFailureBehaviour
   ): Boolean {
      return when {
         searchFailureBehaviour != QueryFailureBehaviour.SEND_TYPED_NULL_OR_EMPTY_ARRAY -> false
         buildResult.size != 1 -> false
         else -> {
            val singleResult = buildResult.single()
            singleResult is TypedCollection && singleResult.isEmpty()
         }
      }
   }

   /**
    * When we build with SEND_TYPED_NULL and the result can't be found,
    * the internal builder returns a typed null.
    *
    * However, because this is inside a flow that has a .toList(), we end up with a nested list - ie: [null]
    *
    * We check here to unwrap.
    *
    * However, this feels like a smell, and maybe there's a smarter way
    *
    */
   private fun isNullListBuildResult(
      buildResult: List<TypedInstance>,
      searchFailureBehaviour: QueryFailureBehaviour
   ): Boolean {
      return when {
         searchFailureBehaviour != QueryFailureBehaviour.SEND_TYPED_NULL -> false
         buildResult.size != 1 -> false
         else -> {
            val singleResult = buildResult.single()
            singleResult is TypedNull
         }
      }
   }

   private fun getCurrentScopedFacts(): List<ScopedFact> {
      return if (value is FactBag) {
         value.scopedFacts
      } else {
         emptyList()
      }
   }

   private fun getFactsInScopeForSearch(): Pair<List<TypedInstance>, List<ScopedFact>> {
      return if (value is CascadingFactBag) {
         // This is a basic implementation now, which is required to make tests pass.
         // The use case here is building an object within an inline field projection.
         //
         // eg:
         // find { Foo } as {
         //    thing : Thing as {
         //       ... // <--- if fields are declared here that require searching, we need to include the "Thing" that's the current projection scope
         //    }
         // (see VyneProjectionTest."should resolve items on inline projection")
         // However, additional thought in future should be given to
         // do we need a broader scoped fact?
         val additionalFacts = emptyList<TypedInstance>()
         val scopedFacts = listOfNotNull(this.projectionScope?.let { scope -> value.getScopedFact(scope) })
         additionalFacts to scopedFacts
      } else {
         emptyList<TypedInstance>() to emptyList();
      }

   }


   private fun readWithValueReader(
      attributeName: AttributeName,
      type: Type,
      format: FormatsAndZoneOffset?
   ): TypedInstance {
      val attributeValue = valueReader.read(value, attributeName).let { attributeValue ->
         if (type.isCollection && attributeValue is Map<*,*> && parsingOptions.convertSingleObjectToArray) {
            listOf(attributeValue)
         } else {
            attributeValue
         }
      }
      val modelFormatSpecPair = formatDetector.getFormatType(type)
      return when {
         attributeValue == null -> TypedNull.create(type, source)
         attributeValue is Expression -> evaluateExpression(attributeValue)
         // Support embedded formats.
         // Eg: A field in a JSON message that contains an XML payload.
         // Or a field in Protobuf that contains JSON, etc etc
         modelFormatSpecPair != null -> {
            readWithFormatSpecDeserializer(attributeValue, modelFormatSpecPair, type)
         }

         else -> TypedInstance.from(
            type,
            attributeValue,
            schema,
            true,
            source = source,
            parsingErrorBehaviour = parsingErrorBehaviour,
            format = format,
            valueSuppliers = valueSuppliers,
            parsingOptions = parsingOptions
         )
      }
   }

}
