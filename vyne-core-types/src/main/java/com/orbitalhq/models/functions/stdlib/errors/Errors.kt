package com.orbitalhq.models.functions.stdlib.errors

import com.orbitalhq.models.functions.NamedFunctionInvoker

object Errors {
   val functions: List<NamedFunctionInvoker> = listOf(
      Throw
   )
}
