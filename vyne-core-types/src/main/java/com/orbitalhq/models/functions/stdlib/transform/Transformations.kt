package com.orbitalhq.models.functions.stdlib.transform

import com.orbitalhq.models.functions.NamedFunctionInvoker

object Transformations {
   val functions: List<NamedFunctionInvoker> = listOf(
      Convert,
      ToRawType
   )
}
