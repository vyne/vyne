package com.orbitalhq.models

import com.orbitalhq.models.facts.FactBag
import com.orbitalhq.models.facts.ScopedFact
import com.orbitalhq.query.AlwaysGoodSpec
import com.orbitalhq.query.QueryContextSchemaProvider
import com.orbitalhq.query.TypedInstanceValidPredicate
import com.orbitalhq.schemas.Type
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import lang.taxi.expressions.Expression
import lang.taxi.services.operations.constraints.Constraint

/**
 * This is a lightweight version of the API exposed by Vyne's
 * QueryContext / QueryEngine.
 *
 * That API has high tech debt, and lots of attributes that are deprecated, but that
 * make it hard to move from Vyne to VyneCore.
 *
 * This interface provides a lightweight way of running queries, without access to the full
 * power of Vyne.  Used in places like TypedObjectFactory, when we want to be able to do
 * in-place discovery.
 *
 * Crucially, this interface doesn't provide a way to mutate the scope of the query.
 * It's  (currently) expected that the implementor of this interface has enough context to understand
 * what's going on, so can perform scoping on behalf of the query caller.  However, this design
 * choice isn't carefully considered at this point, and may need to chang.e
 */
interface InPlaceQueryEngine : FactBag, QueryContextSchemaProvider {
   suspend fun findType(
      type: Type,
      spec: TypedInstanceValidPredicate = AlwaysGoodSpec,
      permittedStrategy: PermittedQueryStrategies = PermittedQueryStrategies.EVERYTHING,
      failureBehaviour: QueryFailureBehaviour = QueryFailureBehaviour.THROW,
      constraint: List<Constraint> = emptyList()
   ): Flow<TypedInstance> {
      return this.findType(type, permittedStrategy, failureBehaviour, constraint)
         .filter { spec.isValid(it) }
   }

   suspend fun findType(
      type: Type,
      permittedStrategy: PermittedQueryStrategies = PermittedQueryStrategies.EVERYTHING,
      failureBehaviour: QueryFailureBehaviour = QueryFailureBehaviour.THROW,
      constraints: List<Constraint> = emptyList()
   ): Flow<TypedInstance>

   fun only(fact: TypedInstance, scopedFacts: List<ScopedFact> = emptyList(), inheritParent: Boolean = true): InPlaceQueryEngine {
      return only(listOf(fact), scopedFacts, inheritParent)
   }

   fun only(facts: List<TypedInstance>, scopedFacts: List<ScopedFact> = emptyList(), inheritParent: Boolean = true): InPlaceQueryEngine

   fun withAdditionalFacts(facts: List<TypedInstance>, scopedFacts: List<ScopedFact>): InPlaceQueryEngine

   fun evaluate(expression: Expression, facts: FactBag = FactBag.empty(), source:DataSource = Provided): TypedInstance
   fun evaluate(expression: Expression, value:TypedInstance, source:DataSource = Provided): TypedInstance

   // See ProjectionFunctionScopeEvaluator.queryContextForFact
   // this method will need to be implemented eventually
//   suspend fun find(
//      typeName: String,
//      permittedStrategy: PermittedQueryStrategies = PermittedQueryStrategies.EVERYTHING,
//   )
}

/**
 * A light-weight version of the PermittedQueryStrategyPredicate,
 * which we don't want to leak into the core library.
 */
enum class PermittedQueryStrategies {
   EVERYTHING,
   EXCLUDE_BUILDER,
   EXCLUDE_BUILDER_AND_MODEL_SCAN
}
