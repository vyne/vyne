package com.orbitalhq.utils.files

import mu.KotlinLogging
import reactor.core.publisher.Flux
import reactor.core.publisher.Sinks
import java.nio.file.ClosedWatchServiceException
import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchKey
import java.nio.file.WatchService
import java.util.concurrent.atomic.AtomicReference

class ReactiveWatchingFileSystemMonitor(
   private val path: Path,
   private val excludedDirectoryNames: List<String> = emptyList()
) : ReactiveFileSystemMonitor {


   private val logger = KotlinLogging.logger { }
   private var watcherThread: Thread? = null
   private val registeredKeys = ArrayList<WatchKey>()
   private val watchServiceRef = AtomicReference<WatchService>()

   private val sink = Sinks.many().replay().latest<List<FileSystemChangeEvent>>()
   private val suspendedEvents = SuspendableEventPublisher.forFileSystemChangeEvents(sink)
   override fun startWatching(): Flux<List<FileSystemChangeEvent>> {
      this.watcherThread = watch()
      return sink.asFlux().doOnCancel {
         logger.info { "Cancelling subscription" }
         this.stop()
      }
   }

   override fun suspend() {
      logger.debug { "Suspending event publication for  ${path.toFile().canonicalPath}" }
      suspendedEvents.suspend()
   }

   override fun resume() {
      logger.debug { "Resuming event publication for  ${path.toFile().canonicalPath}" }
      suspendedEvents.resume()
   }

   override fun stop() {
      unregisterKeys()
      cancelWatch()
      sink.tryEmitComplete()
      watcherThread?.interrupt()
   }


   fun cancelWatch() {
      watchServiceRef.get()?.close()
   }

   private fun registerKeys(watchService: WatchService) {
      path.toFile().walkTopDown()
         .onEnter {
            val excluded = (it.isDirectory && excludedDirectoryNames.contains(it.name))
            !excluded
         }
         .filter { it.isDirectory }
         .forEach { directory ->
            watchDirectory(directory.toPath(), watchService)
         }
   }

   private fun watchDirectory(path: Path, watchService: WatchService) {
      registeredKeys.add(
         path.register(
            watchService,
            StandardWatchEventKinds.ENTRY_CREATE,
            StandardWatchEventKinds.ENTRY_DELETE,
            StandardWatchEventKinds.ENTRY_MODIFY,
            StandardWatchEventKinds.OVERFLOW
         )
      )
   }

   private fun unregisterKeys() {
      registeredKeys.apply {
         forEach {
            it.cancel()
         }
         clear()
      }
   }

   private fun watch(): Thread {
      val thread = Thread {
         logger.info("Starting to watch ${path.toFile().canonicalPath}")
         val watchService = FileSystems.getDefault().newWatchService()

         watchServiceRef.set(watchService)
         registerKeys(watchService)

         try {
            while (true) {
               val key = watchService.take()
               val events = key.pollEvents()
                  .mapNotNull { event ->
                     (event.context() as? Path?)?.let { path ->
                        /** Even if the subdirectory itself is not watched (is excluded), change events will still be generated
                         * for that subdirectory by the parent directory watcher (at least on Windows). Thus, we skip those events.
                         */
                        if (excludedDirectoryNames.contains(path.toString())) {
                           return@mapNotNull null
                        }
                        val eventType = when (event.kind()) {
                           StandardWatchEventKinds.ENTRY_CREATE -> FileSystemChangeEvent.FileSystemChangeEventType.FileCreated
                           StandardWatchEventKinds.ENTRY_DELETE -> FileSystemChangeEvent.FileSystemChangeEventType.FileDeleted
                           else -> FileSystemChangeEvent.FileSystemChangeEventType.Unspecified
                        }
                        val resolvedPath = (key.watchable() as Path).resolve(path)
                        FileSystemChangeEvent(resolvedPath, eventType)
                     }
                  }
               if (events.isEmpty()) {
                  key.reset()
                  continue
               } else {
                  suspendedEvents.publish(events)
                  key.reset()
               }
            }
         } catch (e: ClosedWatchServiceException) {
            logger.warn(e) { "Watch service was closed. ${e.message}" }
         } catch (e: Exception) {
            logger.error(e) { "Error in watch service: ${e.message}" }
         }
      }
      thread.start()
      return thread
   }

}
