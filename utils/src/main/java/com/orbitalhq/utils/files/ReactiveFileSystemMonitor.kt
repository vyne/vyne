package com.orbitalhq.utils.files

import reactor.core.publisher.Flux
import java.nio.file.Path

interface ReactiveFileSystemMonitor {
   fun startWatching(): Flux<List<FileSystemChangeEvent>>
   // Don't need to stop, just stop when all the subscribers to start() have gone away.
   fun suspend()
   fun resume()
   fun stop()
}

data class FileSystemChangeEvent(
   val path: Path,
   val eventType: FileSystemChangeEventType
) {
   enum class FileSystemChangeEventType {
      DirectoryCreated,
      DirectoryChanged,
      DirectoryDeleted,
      FileCreated,
      FileChanged,
      FileDeleted,
      Unspecified
   }
}
