// homepage
export const searchBar: string = '[class="ng-select ng-select-single ng-select-typeahead ng-select-searchable ng-select-clearable"]'
export const matchedText: string = '[class="matchedText"]'
// query editor 
export const callResult: string = '[class = "pill result status-success"]'// profiler
export const callAddress: string = '[class = "address"]'// profiler
export const objectViewItem: string = ':nth-child(1) > .collection-member > app-object-view > .typed-object-container > :nth-child(1) > .attribute-value'// first item on object view
export const grid: string = 'div[class="ag-root ag-unselectable ag-layout-normal"]'
export const profilerSchema: string = 'rect[class="actor"]'// schema template
export const downloadMenu: string = '[class="mat-menu-content"]'
// query builder
export const targetToFind: string = '[id="mat-chip-list-input-0"]'
export const discover: string = '[value= "DISCOVER"]'
export const build: string = '[value= "BUILD"]'
export const gather: string = '[value= "GATHER"]'
export const queryModeSelect: string = '[class="mat-select-arrow-wrapper"]'
export const choosenSchema: string = 'smyrna.orders.Order' // test data type
// query history
export const disabledHistoryDisplay: string = '[class="results-section ng-star-inserted"]'
export const queryHistoryFirstItem: string = ':nth-child(1) > app-query-history-card > :nth-child(1) > :nth-child(1) > .ng-star-inserted > app-vyneql-record > .history-item > code'
export const queryHistoryList: string = '.list-container'
export const queryHistoryProgressbar: string = '[class="progress-container ng-star-inserted"]'
export const queryDone: string = ':nth-child(1) > app-query-history-card > :nth-child(1) > .record-stats > .ng-star-inserted > .clock-icon'
export const discoverHistoryFirstItem: string = ':nth-child(1) > app-query-history-card > .history-item'
// cask
export const startDate: string = '#mat-input-0' //ingestion, data-e2e-id="start-date"
export const endDate: string = '#mat-input-1' // data-e2e-id="end-date"
export const caskItems: string = '[class="cask-typename"]'
// data catalog
export const operationNamespace = '[class="member-package"]'
export const operationName = '[class="member-name"]'
export const filterNamespace: string = 'bank.common'
export const filterName: string = 'currencycode'
export const searchResult: string = '[class="ng-option ng-star-inserted"]'
export const markedResult: string = '[class="ng-option ng-option-marked ng-star-inserted"]'
export const searchText: string = 'ClientOrderID'
export const typeName: string = 'h1'
export const link: string = '[id="links"]'
export const line: string = '[class="line"]'
export const dataCatalogList: string = '[class="search-result ng-star-inserted"]'
// data explorer
export const selectType: string = '[id="mat-input-0"]'
export const dropInput: string = '[class="ngx-file-drop__file-input"]'
export const fixtureDataCsv: string = 'data-smyrna.csv'
export const dropData: string = 'drop-data-smyrna.csv'
export const fixtureDataJson: string = 'smyrna.JSON'
export const monoBadge: string = '[class="inline mono-badge"]'
export const explorerGrid: string = '[class="mat-tab-body-wrapper"]'
export const dataSource: string = '[class="data-source-configuration ng-star-inserted"]'
export const typeBox: string = '[class="node ng-star-inserted"]'
export const caskStore: string = '[class="mat-expansion-panel-header-title"]'
export const storeSuccess: string = '[class="ng-star-inserted"]'
export const drillRelateItem: string = '[class="edge-label"]'
export const dropzone: string = '[class="drop-container"]'