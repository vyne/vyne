/**
 * A list of places in the docs we link to.
 * Allows whitelabels to provide analagous links in their own docs
 */
export type DocsLinks = {
  configureMetricsReporting: string;
  publishQueriesAsEndpoints: string;
  workspaceConfigFile: string;
  authenticationToServices: string;
  managingSecrets: string;
  authentication: string;
  dynamoDbConnection: string;
  lambdaDbConnection: string;
  s3Connection: string;
  sqsConnection: string;
  dataPolicies: string;
  projectReadme: string;
  docsHome: string;
  nebulaDocs?: string;
}
