export const environment = {
  production: false,
  // Convention for relative urls:  Start with an /, but don't end with one
  serverUrl: `//localhost:9500`,
};
