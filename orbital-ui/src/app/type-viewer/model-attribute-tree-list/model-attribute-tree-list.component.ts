import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PartialSchema, QualifiedName, Type } from '../../services/schema';
import { MatDialog } from '@angular/material/dialog';
import { BaseSchemaMemberDisplay } from './base-schema-member-display';

@Component({
  selector: 'app-model-attribute-tree-list',
  template: `
    <app-model-member *ngFor="let field of model.attributes | keyvalue: unsorted"
                      [member]="field.value"
                      [memberName]="field.key"
                      [parentModel]="model"
                      [editable]="editable"
                      [schemaMemberNavigable]="schemaMemberNavigable"
                      [new]="true"
                      [showFullTypeNames]="showFullTypeNames"
                      [anonymousTypes]="anonymousTypes"
                      [commitMode]="commitMode"
                      (newTypeCreated)="newTypeCreated.emit(type)"
                      (updateDeferred)="this.updateDeferred.emit({schemaEditOperation: $event.schemaEditOperation, member: type})"
                      (typeNameClicked)="typeNameClicked.emit($event)"
                      [schema]="schema"
                      [partialSchema]="partialSchema"
    ></app-model-member>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./model-attribute-tree-list.scss']
})
export class ModelAttributeTreeListComponent extends BaseSchemaMemberDisplay {

  constructor(dialog: MatDialog) {
    super(dialog);
  }

  @Input()
  showFullTypeNames = false;

  @Output()
  typeNameClicked = new EventEmitter<QualifiedName>()

  private _model: Type;
  @Input()
  get model(): Type {
    return this._model;
  }

  set model(value: Type) {
    this._model = value;
  }

  @Input()
  partialSchema: PartialSchema;

  @Output()
  newTypeCreated = new EventEmitter<Type>()


  get type(): Type {
    return this._model;
  }

  get isModel(): Boolean {
    return this._model && Object.keys(this._model.attributes).length > 0;
  }

  unsorted(a: any, b: any): number { return 0; }
}
