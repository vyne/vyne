import { moduleMetadata } from "@storybook/angular";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { schemaWithNestedTypes } from "../../data-source-import/data-source-import.data";
import { TypeViewerModule } from "../type-viewer.module";
import { VERY_LARGE_SCHEMA } from "./very-large-schema";

export default {
  title: "Model tree list",

  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        TypeViewerModule,
      ],
    }),
  ],
};

export const ModelDisplay = () => {
  return {
    template: `
        <app-model-attribute-tree-list [model]="model" [schema]="schema" [editable]="true" [anonymousTypes]="anonymousTypes"></app-model-attribute-tree-list>
      `,
    props: {
      schema: schemaWithNestedTypes,
      model: schemaWithNestedTypes.types.find(
        (t) => t.name.fullyQualifiedName === "io.vyne.demo.Person",
      ),
      anonymousTypes: [],
    },
  };
};

ModelDisplay.story = {
  name: "model display",
};

export const LargeModelDisplay = () => {
  return {
    template: `
        <app-model-attribute-tree-list [model]="model" [schema]="schema" [editable]="true" [anonymousTypes]="anonymousTypes"></app-model-attribute-tree-list>
      `,
    props: {
      schema: VERY_LARGE_SCHEMA,
      model: VERY_LARGE_SCHEMA.types.find(
        (t) =>
          t.name.fullyQualifiedName ===
          "com.ultumus.uatapi.indexComposition.definitions.Composition",
      ),
      anonymousTypes: [],
    },
  };
};

LargeModelDisplay.story = {
  name: "large model display",
};
