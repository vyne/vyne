import { TuiTextfieldControllerModule, TuiTextareaModule, TuiInputModule, TuiTagModule } from "@taiga-ui/legacy";
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SaveWithFilenameComponent} from '../filename-display/save-with-filename.component';
import {TypeLinkGraphModule} from './type-link-graph/type-link-graph.module';
import {AttributeTableModule} from './attribute-table/attribute-table.module';
import {ContentsTableModule} from './contents-table/contents-table.module';
import {DescriptionEditorModule} from './description-editor/description-editor.module';
import {EnumTableModule} from './enums-table/enum-table.module';
import {TypeViewerComponent} from './type-viewer.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {SearchModule} from '../search/search.module';
import {CodeViewerModule} from '../code-viewer/code-viewer.module';
import {TocHostDirective} from './toc-host.directive';
import {TypeViewerContainerComponent} from './type-viewer-container.component';
import {InheritanceGraphModule} from '../inheritence-graph/inheritance-graph.module';
import {HeaderBarModule} from '../header-bar/header-bar.module';
import {UsagesTableComponent} from './usages-table/usages-table.component';
import {OperationBadgeModule} from '../operation-badge/operation-badge.module';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {LineageGraphModule} from './lineage-graph/lineage-graph.module';
import {RouterModule} from '@angular/router';
import {InheritsFromComponent} from './inherits-from.component';
import {TuiLabel, TuiIcon, TuiLink, TuiButton, TuiLoader} from '@taiga-ui/core';
import {
  TuiTree,
  TuiBadge,
  TuiSwitch,
  TuiTabs,
  TuiProgress,
  TuiCheckbox, TuiChip,
} from '@taiga-ui/kit';
import {TypeSearchComponent} from './type-search/type-search.component';
import {TypeSearchContainerComponent} from './type-search/type-search-container.component';
import {TypeSearchResultComponent} from './type-search/type-search-result.component';
import {ModelAttributeTreeListComponent} from './model-attribute-tree-list/model-attribute-tree-list.component';
import {ModelMemberComponent} from './model-attribute-tree-list/model-member.component';
import {ModelMemberTreeNodeComponent} from './model-attribute-tree-list/model-member-tree-node.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TypedEditorModule} from '../type-editor/type-editor.module';
import {SchemaDiagramModule} from '../schema-diagram/schema-diagram.module';
import {ChangesetSelectorModule} from '../changeset-selector/changeset-selector.module';
import { MarkdownModule } from 'ngx-markdown';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';


@NgModule({
  imports: [
    SearchModule,
    TuiInputModule,
    MatToolbarModule,
    TuiTextfieldControllerModule,
    CommonModule,
    AttributeTableModule,
    ContentsTableModule,
    DescriptionEditorModule,
    EnumTableModule,
    TypeLinkGraphModule,
    CodeViewerModule,
    InheritanceGraphModule,
    HeaderBarModule,
    OperationBadgeModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    LineageGraphModule,
    RouterModule,
    TuiLink,
    TuiTextareaModule,
    ...TuiTree,
    TuiLabel,
    TuiTagModule,
    MatProgressSpinnerModule,
    TuiSwitch,
    ...TuiTabs,
    TypedEditorModule,
    SchemaDiagramModule,
    TuiButton,
    ChangesetSelectorModule,
    MarkdownModule,
    TuiIcon,
    ...TuiProgress,
    SaveWithFilenameComponent,
    TuiBadge,
    TuiCheckbox,
    TuiLoader,
    TuiChip,
  ],
    declarations: [
        TocHostDirective,
        TypeViewerComponent,
        TypeViewerContainerComponent,
        UsagesTableComponent,
        // TagsSectionComponent,
        // EditTagsPanelComponent,
        // EditTagsPanelContainerComponent,
        // EditOwnerPanelContainerComponent,
        // EditOwnerPanelComponent,
        InheritsFromComponent,
        ModelAttributeTreeListComponent, ModelMemberComponent, ModelMemberTreeNodeComponent,
        // These type search components are declared in type-viewer, otherwise we end up
        // with circular dependencies
        // (When editing a type in the type viewer, you can search for a new type,
        // which opens the type search.  When searching for a type, we show documentation
        // which needs the type viewer.
        // That's the circular dependency).
        TypeSearchComponent, TypeSearchContainerComponent, TypeSearchResultComponent,
    ],
    exports: [
        // TagsSectionComponent,
        TypeViewerComponent,
        // EditTagsPanelComponent,
        // EditOwnerPanelComponent,
        // EditTagsPanelContainerComponent,
        InheritsFromComponent,
        TypeSearchContainerComponent,
        TypeSearchComponent,
        ModelAttributeTreeListComponent,
    ],
    providers: [
      { provide: MAT_DIALOG_DATA, useValue: {} },
      { provide: MatDialogRef, useValue: {} }
    ]
})
export class TypeViewerModule {
}
