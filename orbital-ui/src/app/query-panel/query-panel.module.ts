import { TuiTextfieldControllerModule, TuiTextareaModule, TuiInputModule, TuiSelectModule } from "@taiga-ui/legacy";
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CopilotPanelComponent} from './query-editor/copilot-panel/copilot-panel.component';
import {DropdownComponent} from './query-editor/query-editor-toolbar/dropdown/dropdown.component';
import {PublishedEndpointInfoComponent} from './query-editor/query-editor-toolbar/published-endpoint-info.component';
import {QueryPanelComponent} from './query-panel.component';
import {SearchModule} from '../search/search.module';
import {TypeAutocompleteModule} from '../type-autocomplete/type-autocomplete.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {VyneQueryViewerComponent} from './taxi-viewer/vyne-query-viewer/vyne-query-viewer.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {CovalentDynamicFormsModule} from '@covalent/dynamic-forms';
import {ObjectViewModule} from '../object-view/object-view.module';
import {CovalentHighlightModule} from '@covalent/highlight';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {TypedInstancePanelModule} from '../typed-instance-panel/typed-instance-panel.module';
import {QueryEditorComponent} from './query-editor/query-editor.component';
import {MatTabsModule} from '@angular/material/tabs';
import {CodeViewerModule} from '../code-viewer/code-viewer.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {QueryEditorToolbar} from './query-editor/query-editor-toolbar/query-editor-toolbar.component';
import {CounterTimerComponent} from './query-editor/query-editor-toolbar/counter-timer.component';
import {CallExplorerModule} from './taxi-viewer/call-explorer/call-explorer.module';

import {AngularSplitModule} from 'angular-split';
import {ErrorPanelComponent} from './error-panel/error-panel.component';
import {HeaderBarModule} from '../header-bar/header-bar.module';
import {MatDialogModule} from '@angular/material/dialog';
import {CodeEditorModule} from '../code-editor/code-editor.module';
import {ResultsTableModule} from '../results-table/results-table.module';
import {TabbedResultsViewModule} from '../tabbed-results-view/tabbed-results-view.module';
import {RouterModule} from '@angular/router';
import {MatSortModule} from '@angular/material/sort';
import {CovalentFileModule} from '@covalent/core/file';
import {ExpandingPanelSetModule} from '../expanding-panelset/expanding-panel-set.module';
import {
  TuiNotification,
  TuiDataList,
  TuiError,
  TuiDropdown,
  TuiIcon,
  TuiButton,
  TuiHint,
  TuiAppearance,
  TuiLink,
} from '@taiga-ui/core';
import {ResultsDownloadModule} from 'src/app/results-download/results-download.module';
import {MatMenuModule} from '@angular/material/menu';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {QuerySnippetPanelModule} from 'src/app/query-snippet-panel/query-snippet-panel.module';
import { TUI_VALIDATION_ERRORS, TuiDataListWrapper, TuiStringifyContentPipe, TuiFilterByInputPipe, TuiFieldErrorPipe, TuiTabs } from '@taiga-ui/kit';
import {CatalogExplorerPanelModule} from "../catalog-explorer-panel/catalog-explorer-panel.module";
import {SaveQueryDialogComponent} from "./query-editor/query-editor-toolbar/save-query-dialog.component";
import {HeaderComponentLayoutModule} from "../header-component-layout/header-component-layout.module";
import {DisableControlModule} from "../disable-control/disable-control.module";
import {QueryHistoryPanelModule} from "../query-history-panel/query-history-panel.module";
import {SavedQueriesPanelModule} from "../saved-queries-panel/saved-queries-panel.module";
import {PublishEndpointDialogComponent} from "./query-editor/query-editor-toolbar/publish-endpoint-dialog.component";
import {ProjectSelectorModule} from "../project-selector/project-selector.module";
import { TuiActiveZone, TuiAutoFocus } from '@taiga-ui/cdk';

@NgModule({
  imports: [
    CommonModule,
    SearchModule,
    TypeAutocompleteModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    CovalentDynamicFormsModule,
    FormsModule,
    ReactiveFormsModule,
    CovalentFileModule,
    ObjectViewModule,
    CovalentHighlightModule,
    MatSidenavModule,
    TypedInstancePanelModule,
    MatTabsModule,
    CodeViewerModule,
    MatProgressSpinnerModule,
    CallExplorerModule,
    AngularSplitModule,
    HeaderBarModule,
    MatDialogModule,
    ResultsTableModule,
    CodeEditorModule,
    TabbedResultsViewModule,
    RouterModule,
    MatSortModule,
    ExpandingPanelSetModule,
    TuiButton,
    ResultsDownloadModule,
    MatMenuModule,
    ClipboardModule,
    ...TuiHint,
    QuerySnippetPanelModule,
    ...TuiTabs,
    TuiTextareaModule,
    TuiSelectModule,
    ...TuiDataListWrapper,
    TuiTextfieldControllerModule,
    CatalogExplorerPanelModule,
    HeaderComponentLayoutModule,
    TuiNotification,
    TuiFilterByInputPipe,
    TuiStringifyContentPipe,
    TuiInputModule,
    TuiError,
    TuiFieldErrorPipe,
    DisableControlModule,
    QueryHistoryPanelModule,
    SavedQueriesPanelModule,
    ProjectSelectorModule,
    ...TuiDropdown,
    TuiActiveZone,
    ...TuiDataList,
    TuiIcon,
    TuiAutoFocus,
    PublishedEndpointInfoComponent,
    DropdownComponent,
    CopilotPanelComponent,
    TuiAppearance,
    TuiLink,
    TuiFieldErrorPipe,
    TuiError,
  ],
  exports: [
    QueryPanelComponent,
    QueryEditorComponent,
    QueryEditorToolbar,
    ErrorPanelComponent,
    SaveQueryDialogComponent
  ],
  declarations: [
    QueryPanelComponent,
    VyneQueryViewerComponent,
    QueryEditorComponent,
    QueryEditorToolbar, CounterTimerComponent,
    PublishEndpointDialogComponent,
    ErrorPanelComponent,
    SaveQueryDialogComponent
  ],
  providers: [
    {
      provide: TUI_VALIDATION_ERRORS,
      useValue: {
        isExisting: 'Endpoint already exists',
      }
    }
  ]
})
export class QueryPanelModule {
}
