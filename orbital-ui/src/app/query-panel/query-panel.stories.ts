import { TuiRoot } from "@taiga-ui/core";
import { moduleMetadata } from "@storybook/angular";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { QueryPanelModule } from "./query-panel.module";
import { AngularSplitModule } from "angular-split";
import { RouterTestingModule } from "@angular/router/testing";
import { ExpandingPanelSetModule } from "../expanding-panelset/expanding-panel-set.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

export default {
  title: "Query panel",

  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        TuiRoot,
        QueryPanelModule,
        ExpandingPanelSetModule,
        AngularSplitModule,
        RouterTestingModule,
      ],
    }),
  ],
};

export const QueryEditor = () => {
  return {
    template: `<div style="padding: 40px; width: 100%; height: 250px" >
    <app-query-editor></app-query-editor>
    </div>`,
    props: {},
  };
};

QueryEditor.story = {
  name: "Query editor",
};

export const BottomBarStates = () => {
  return {
    template: `<div style="padding: 40px; width: 80%; height: 250px" >
      <app-panel-header title="Code">
        <app-query-editor-toolbar currentState="Editing"></app-query-editor-toolbar>
      </app-panel-header>
       <app-panel-header title="Code">
       <app-query-editor-toolbar currentState="Running" [queryStarted]="queryStartDate"></app-query-editor-toolbar>
      </app-panel-header>
       <app-panel-header title="Code">
        <app-query-editor-toolbar currentState="Running" [queryStarted]="aMinuteAgo"></app-query-editor-toolbar>
      </app-panel-header>
       <app-panel-header title="Code">
       <app-query-editor-toolbar currentState="Error" error="A query failed to execute."></app-query-editor-toolbar>
      </app-panel-header>
    </div>`,
    props: {
      queryStartDate: new Date(),
      aMinuteAgo: new Date(new Date().getTime() - 1000 * 60),
    },
  };
};

BottomBarStates.story = {
  name: "Bottom bar states",
};

export const SavePanel = () => {
  return {
    template: `
<tui-root>
<div style="padding: 40px; " >
<app-save-query-dialog [packages]="projects"></app-save-query-dialog>
</div>
</tui-root>
      `,
    props: {
      projects: [
        {
          identifier: {
            organisation: "io.vyne",
            name: "my-project",
            version: "1.0.0",
            unversionedId: "io.vyne/core-types",
            id: "io.vyne/core-types/1.0.0",
            uriSafeId: "io.vyne:core-types:1.0.0",
          },
          health: {
            status: "Healthy",
            message: null,
            timestamp: "2023-04-14T06:54:00.040411019Z",
          },
          sourceCount: 11,
          warningCount: 0,
          errorCount: 0,
          publisherType: "Pushed",
          editable: true,
          packageConfig: null,
          uriPath: "io.vyne:core-types:1.0.0",
        },
      ],
    },
  };
};

SavePanel.story = {
  name: "save panel",
};
