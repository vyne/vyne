import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TypeAutocompleteComponent} from './type-autocomplete.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import {MatInputModule} from '@angular/material/input';
import {SchemaMemberAutocompleteComponent} from './schema-member-autocomplete.component';
import {ConnectionNameAutocompleteComponent} from './connection-name-autocomplete.component';

@NgModule({
  declarations: [
    TypeAutocompleteComponent,
    SchemaMemberAutocompleteComponent,
    ConnectionNameAutocompleteComponent
  ],
  imports: [
    CommonModule,

    FormsModule,
    ReactiveFormsModule,

    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatIconModule,
    MatChipsModule
  ],
  exports: [TypeAutocompleteComponent, SchemaMemberAutocompleteComponent, ConnectionNameAutocompleteComponent]
})
export class TypeAutocompleteModule {
}
