import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {SourcePackageDescription} from "../package-viewer/packages.service";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'app-project-selector',
  template: `
    <tui-notification size="m" *ngIf="!hasEditablePackages && showErrorIfNoEditable"
                      appearance="error"
    ><div>
      You don't currently have any projects that are editable. Add or configure a project in the <a
        [routerLink]="['/schemas']">Schemas</a> view
    </div></tui-notification>

    <tui-select
      [stringify]="stringify"
      [ngModel]="selectedPackage"
      (ngModelChange)="setValue($event)"
      [tuiDropdownOpen]="startOpened"
    >
      {{ prompt }}
      <input
        tuiTextfieldLegacy [disableControl]="disabled"
      />
      <tui-data-list-wrapper
        *tuiDataList
        [items]="editablePackages | tuiFilterByInput"
        [itemContent]="stringify | tuiStringifyContent"
      ></tui-data-list-wrapper>
    </tui-select>
  `,
  styleUrls: ['./project-selector.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ProjectSelectorComponent
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectSelectorComponent implements ControlValueAccessor {
  @Input()
  disabled: boolean = false
  @Input()
  packages: SourcePackageDescription[];
  @Input()
  filterToEditableOnly: boolean = true;
  @Input()
  prompt: string
  @Input()
  selectedPackage: SourcePackageDescription | null;
  @Input()
  startOpened: boolean;

  @Input()
  showErrorIfNoEditable: boolean = false;

  get editablePackages(): SourcePackageDescription[] {
    if (!this.packages) {
      return [];
    } else if (!this.filterToEditableOnly) {
      return this.packages
    } else {
      return this.packages.filter(p => p.editable)
    }
  }

  get hasEditablePackages(): boolean {
    return this.editablePackages.length > 0;
  }

  readonly stringify = (item: SourcePackageDescription) => item.identifier.name;

  onChange = (value) => {
  };

  onTouched = () => {
  };

  registerOnChange(fn: any): void {
    this.onChange = fn
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: any): void {
    this.selectedPackage = obj
  }

  setValue(value: SourcePackageDescription) {
    this.selectedPackage = value;
    this.onChange(value);
    this.onTouched();
  }
}
