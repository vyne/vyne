import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter, HostBinding,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { BaseTypedInstanceViewer } from './BaseTypedInstanceViewer';
import { InstanceLike, Type } from '../services/schema';
import { Observable } from 'rxjs';
import { ResultsTableComponent } from '../results-table/results-table.component';
import { AppInfoService, AppConfig } from '../services/app-info.service';
import { TypesService } from '../services/types.service';
import { throttleTime } from 'rxjs/operators';
import { ExportFormat } from 'src/app/results-download/results-download.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-object-view-container',
  template: `
    <ng-container *ngIf="ready">
      <app-results-table *ngIf="displayMode==='table'"
                         [isStreamingQuery]="isStreamingQuery"
                         [instances$]="instances$"
                         [schema]="schema"
                         [selectable]="selectable"
                         [type]="type"
                         [anonymousTypes]="anonymousTypes"
                         (instanceClicked)="instanceClicked.emit($event)">
      </app-results-table>
      <app-object-view *ngIf="displayMode==='tree'"
                       [isStreamingQuery]="isStreamingQuery"
                       [instances$]="instances$"
                       [schema]="schema"
                       [selectable]="selectable"
                       [type]="type"
                       [anonymousTypes]="anonymousTypes"
                       (instanceClicked)="instanceClicked.emit($event)">
      </app-object-view>
      <app-json-results-view *ngIf="displayMode === 'json'"
                             [isStreamingQuery]="isStreamingQuery"
                             [instances$]="instances$"
                             [isResponseLarge]="isResponseLarge"
                             [schema]="schema"
                             >
      </app-json-results-view>
    </ng-container>
  `,
  styleUrls: ['./object-view-container.component.scss']
})
export class ObjectViewContainerComponent extends BaseTypedInstanceViewer implements AfterContentInit {

  // workaround for lack of enum support in templates
  downloadFileType = ExportFormat;

  config: AppConfig;

  constructor(
    private typesService: TypesService,
    appInfoService: AppInfoService,
    private changeDetector: ChangeDetectorRef) {
    super();
    appInfoService.getConfig()
      .subscribe(next => this.config = next);

    this.instancesChanged$
      .pipe(throttleTime(500))
      .subscribe(() => changeDetector.markForCheck())
  }

  @ViewChild(ResultsTableComponent)
  resultsTable: ResultsTableComponent;

  @HostBinding('class.display-contents') get classDisplayMode() { return this.displayMode === "json" }

  @Input()
  get displayMode(): DisplayMode {
    return this._displayMode;
  }

  set displayMode(value: DisplayMode) {
    this._displayMode = value;
  }

  private _displayMode: DisplayMode = 'table';
  private _instances$: Observable<InstanceLike>;
  @Input()
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  selectable: boolean = false;

  @Input()
  anonymousTypes: Type[];

  @Input()
  isStreamingQuery: boolean;

  private instancesChanged$: EventEmitter<void> = new EventEmitter<void>();

  get ready() {
    return this.instances$ && this.schema;
  }

  @Input()
  get instances$(): Observable<InstanceLike> {
    return this._instances$;
  }

  set instances$(value: Observable<InstanceLike>) {
    if (value === this._instances$) {
      return;
    }

    this._instances$ = value;
    this.unsubscribeOnClose(this._instances$.subscribe(next => {
      this.instancesChanged$.emit();
    }));

  }

  @Output()
  downloadClicked = new EventEmitter<DownloadClickedEvent>();

  @Input()
  downloadSupported = false;

  @Input()
  isResponseLarge: boolean;

  downloadRegressionPack: any;

  @Input()
  get type(): Type {
    return this._type;
  }

  set type(value: Type) {
    if (value === this._type) {
      return;
    }
    this._type = value;

  }


  ngAfterContentInit(): void {
    this.remeasureTable();
  }


  onDownloadClicked(format: ExportFormat) {
    if (this.config.analytics.persistResults) {
      this.downloadClicked.emit(new DownloadClickedEvent(format));
    } else {
      this.resultsTable.downloadAsCsvFromGrid();
    }
  }

  downloadAsCsvFromGrid() {
    this.resultsTable.downloadAsCsvFromGrid();
  }

  remeasureTable() {
    if (this.resultsTable) {
      console.log('Remeasuring resultsTable');
      this.resultsTable.remeasure();
    } else {
      console.warn('Called remeasureTable, but the resultsTable component isnt available yet');
    }
  }


}

export type DisplayMode = 'table' | 'tree' | 'json';

export class DownloadClickedEvent {
  constructor(public readonly format: ExportFormat) {
  }
}
