import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {Service} from '../services/schema';
import {TypesService} from '../services/types.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {flatMap, map} from 'rxjs/operators';
import { ServiceViewComponent } from './service-view.component';

@Component({
  selector: 'app-service-view-container',
  template: `
    <app-service-view [service]="service"></app-service-view>
  `,
  styleUrls: ['./service-view-container.component.scss'],
  imports: [
    ServiceViewComponent
  ],
  standalone: true
})
export class ServiceViewContainerComponent implements OnInit {

  @Input()
  service: Service;

  constructor(private typeService: TypesService,
              private activeRoute: ActivatedRoute,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.activeRoute.paramMap.pipe(
      map((params: ParamMap) => params.get('serviceName')),
      flatMap(serviceName => this.typeService.getService(serviceName))
    ).subscribe((service: Service) => {
      this.service = service;
      // Note: Strictly this shouldn't be required.
      // (As this view is not ViewDetection: OnPush)
      // But, in the data sources view, for some reason without this
      // we're not updating the view without this.
      this.changeDetectorRef.markForCheck();
    });
  }
}
