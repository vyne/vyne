import {Directive, Input} from '@angular/core';
import {SearchField, SearchResult} from '../search.service';

@Directive()
export class BaseSearchResultComponent {
  @Input()
  result: SearchResult;

  get name(): string {
    return this.getMatch('NAME') || this.result.qualifiedName.name;
  }

  get qualifiedName(): string {
    return this.getMatch('QUALIFIED_NAME') || this.result.qualifiedName.fullyQualifiedName;
  }

  get typeDoc(): string {
    return this.getMatch('TYPEDOC') || this.result.typeDoc;
  }

  private getMatch(searchField: SearchField): string | null {
    const match = this.result.matches.find(m => m.field === searchField);
    if (match) {
      return match.highlightedMatch;
    } else {
      return null;
    }
  }

  memberType(result: SearchResult): string {
    switch (result.memberType) {
      case 'OPERATION':
        return result.operationKind;
      case 'TYPE':
        return result.typeKind;
      case 'FIELD':
        return 'Field';
      case 'SERVICE':
        return result.serviceKind || 'Service';
      default:
        return '?';
    }
  }

  memberTypeForCSS(result: SearchResult): string {
    if (result.memberType === 'OPERATION') {
      return 'service';
    } else if ((result.memberType === 'TYPE' && result.typeKind === 'Model') || result.memberType === 'FIELD') {
      return 'model'
    }
    return result.memberType.toLowerCase();
  }
}
