import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {TypesService} from '../services/types.service';
import {InstanceSelectedEvent} from '../query-panel/instance-selected-event';
import {QueryService} from '../services/query.service';
import {BaseQueryResultWithSidebarComponent} from '../query-panel/BaseQueryResultWithSidebarComponent';
import {Observable} from 'rxjs/internal/Observable';
import {QueryResultInstanceSelectedEvent} from '../query-panel/result-display/BaseQueryResultComponent';

@Component({
  selector: 'app-typed-instance-panel-container',
  styleUrls: ['./typed-instance-panel-container.component.scss'],
  template: `
    <progress
      max="100"
      tuiProgressBar
      size='xs'
      new
      *ngIf='isLoading'
    ></progress>
    <app-panel-header [title]="panelTitle" *ngIf="showPanelHeader" [isSecondary]="true">
      <div class="spacer"></div>
      <button
        (click)="close.emit()"
        tuiIconButton
        type="button"
        appearance="icon"
        size="xs"
        iconStart="@tui.x"
      ></button>
    </app-panel-header>
    <app-typed-instance-panel
      [type]="selectedTypeInstanceType"
      [instance]="selectedTypeInstance"
      [inheritanceView]="inheritanceView"
      [dataSource]="selectedTypeInstanceDataSource"
      [discoverableTypes]="discoverableTypes"
      [instanceQueryCoordinates]="selectedInstanceQueryCoordinates"
    ></app-typed-instance-panel>
  `
})
export class TypedInstancePanelContainerComponent extends BaseQueryResultWithSidebarComponent {

  @Input()
  showPanelHeader = true
  @Input()
  panelTitle = 'Value details'

  @Output()
  close = new EventEmitter();

  private _queryResultSelectedEvent$: Observable<QueryResultInstanceSelectedEvent>

  @Input()
  get queryResultSelectedEvent$(): Observable<QueryResultInstanceSelectedEvent> {
    return this._queryResultSelectedEvent$;
  }

  set queryResultSelectedEvent$(value) {
    if (this._queryResultSelectedEvent$ === value) {
      return;
    }
    this._queryResultSelectedEvent$ = value;
    if (this.queryResultSelectedEvent$) {
      this.unsubscribeOnClose(this.queryResultSelectedEvent$.subscribe(event => {
        this.onQueryResultSelected(event);
      }))
    }
  }


  private _instanceSelected$: Observable<InstanceSelectedEvent>

  @Input()
  get instanceSelected$(): Observable<InstanceSelectedEvent> {
    return this._instanceSelected$;
  }

  set instanceSelected$(value) {
    if (this._instanceSelected$ === value) {
      return;
    }
    this._instanceSelected$ = value;
    if (this.instanceSelected$) {
      this.unsubscribeOnClose(this.instanceSelected$.subscribe(event => {
        this.onTypedInstanceSelected(event);
      }))
    }
  }

  constructor(protected queryService: QueryService, protected typeService: TypesService, changeDetector: ChangeDetectorRef) {
    super(queryService, typeService, changeDetector);
  }

  onCloseTypedInstanceDrawer($event: any) {
    this.close.emit();
  }

}
