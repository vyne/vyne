export const ordersSchema = {
  'types': [{
    'name': {
      'fullyQualifiedName': 'lang.taxi.Boolean',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Boolean',
      'name': 'Boolean',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Boolean',
      'shortDisplayName': 'Boolean'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'Represents a value which is either `true` or `false`.',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Boolean',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Boolean',
      'name': 'Boolean',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Boolean',
      'shortDisplayName': 'Boolean'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Boolean',
    'longDisplayName': 'lang.taxi.Boolean',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Boolean',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Boolean',
      'name': 'Boolean',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Boolean',
      'shortDisplayName': 'Boolean'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A collection of characters.',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.String',
    'longDisplayName': 'lang.taxi.String',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Int',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Int',
      'name': 'Int',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Int',
      'shortDisplayName': 'Int'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A signed integer - ie. a whole number (positive or negative), with no decimal places',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Int',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Int',
      'name': 'Int',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Int',
      'shortDisplayName': 'Int'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Int',
    'longDisplayName': 'lang.taxi.Int',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Int',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Int',
      'name': 'Int',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Int',
      'shortDisplayName': 'Int'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Decimal',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Decimal',
      'name': 'Decimal',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Decimal',
      'shortDisplayName': 'Decimal'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A signed decimal number - ie., a whole number with decimal places.',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Decimal',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Decimal',
      'name': 'Decimal',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Decimal',
      'shortDisplayName': 'Decimal'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Decimal',
    'longDisplayName': 'lang.taxi.Decimal',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Decimal',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Decimal',
      'name': 'Decimal',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Decimal',
      'shortDisplayName': 'Decimal'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Date',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Date',
      'name': 'Date',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Date',
      'shortDisplayName': 'Date'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A date, without a time or timezone.',
    'isTypeAlias': false,
    'offset': null,
    'format': ['yyyy-MM-dd'],
    'hasFormat': true,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Date',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Date',
      'name': 'Date',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Date',
      'shortDisplayName': 'Date'
    },
    'unformattedTypeName': {
      'fullyQualifiedName': 'lang.taxi.Date',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Date',
      'name': 'Date',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Date',
      'shortDisplayName': 'Date'
    },
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Date',
    'longDisplayName': 'lang.taxi.Date(yyyy-MM-dd)',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Date',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Date',
      'name': 'Date',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Date',
      'shortDisplayName': 'Date'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Time',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Time',
      'name': 'Time',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Time',
      'shortDisplayName': 'Time'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'Time only, excluding the date part',
    'isTypeAlias': false,
    'offset': null,
    'format': ['HH:mm:ss'],
    'hasFormat': true,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Time',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Time',
      'name': 'Time',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Time',
      'shortDisplayName': 'Time'
    },
    'unformattedTypeName': {
      'fullyQualifiedName': 'lang.taxi.Time',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Time',
      'name': 'Time',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Time',
      'shortDisplayName': 'Time'
    },
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Time',
    'longDisplayName': 'lang.taxi.Time(HH:mm:ss)',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Time',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Time',
      'name': 'Time',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Time',
      'shortDisplayName': 'Time'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.DateTime',
      'parameters': [],
      'parameterizedName': 'lang.taxi.DateTime',
      'name': 'DateTime',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.DateTime',
      'shortDisplayName': 'DateTime'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A date and time, without a timezone.  Generally, favour using Instant which represents a point-in-time, as it has a timezone attached',
    'isTypeAlias': false,
    'offset': null,
    'format': ['yyyy-MM-dd\'T\'HH:mm:ss.SSS'],
    'hasFormat': true,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.DateTime',
      'parameters': [],
      'parameterizedName': 'lang.taxi.DateTime',
      'name': 'DateTime',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.DateTime',
      'shortDisplayName': 'DateTime'
    },
    'unformattedTypeName': {
      'fullyQualifiedName': 'lang.taxi.DateTime',
      'parameters': [],
      'parameterizedName': 'lang.taxi.DateTime',
      'name': 'DateTime',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.DateTime',
      'shortDisplayName': 'DateTime'
    },
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.DateTime',
    'longDisplayName': 'lang.taxi.DateTime(yyyy-MM-dd\'T\'HH:mm:ss.SSS)',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.DateTime',
      'parameters': [],
      'parameterizedName': 'lang.taxi.DateTime',
      'name': 'DateTime',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.DateTime',
      'shortDisplayName': 'DateTime'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Instant',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Instant',
      'name': 'Instant',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Instant',
      'shortDisplayName': 'Instant'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A point in time, with date, time and timezone.  Follows ISO standard convention of yyyy-MM-dd\'T\'HH:mm:ss.SSSZ',
    'isTypeAlias': false,
    'offset': null,
    'format': ['yyyy-MM-dd\'T\'HH:mm:ss[.SSS]X'],
    'hasFormat': true,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Instant',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Instant',
      'name': 'Instant',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Instant',
      'shortDisplayName': 'Instant'
    },
    'unformattedTypeName': {
      'fullyQualifiedName': 'lang.taxi.Instant',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Instant',
      'name': 'Instant',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Instant',
      'shortDisplayName': 'Instant'
    },
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Instant',
    'longDisplayName': 'lang.taxi.Instant(yyyy-MM-dd\'T\'HH:mm:ss[.SSS]X)',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Instant',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Instant',
      'name': 'Instant',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Instant',
      'shortDisplayName': 'Instant'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Any',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Any',
      'name': 'Any',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Any',
      'shortDisplayName': 'Any'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'Can be anything.  Try to avoid using \'Any\' as it\'s not descriptive - favour using a strongly typed approach instead',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Any',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Any',
      'name': 'Any',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Any',
      'shortDisplayName': 'Any'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Any',
    'longDisplayName': 'lang.taxi.Any',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Any',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Any',
      'name': 'Any',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Any',
      'shortDisplayName': 'Any'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Double',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Double',
      'name': 'Double',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Double',
      'shortDisplayName': 'Double'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'Represents a double-precision 64-bit IEEE 754 floating point number.',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Double',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Double',
      'name': 'Double',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Double',
      'shortDisplayName': 'Double'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Double',
    'longDisplayName': 'lang.taxi.Double',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Double',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Double',
      'name': 'Double',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Double',
      'shortDisplayName': 'Double'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Void',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Void',
      'name': 'Void',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Void',
      'shortDisplayName': 'Void'
    },
    'attributes': {},
    'modifiers': ['PRIMITIVE'],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'Nothing.  Represents the return value of operations that don\'t return anything.',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Void',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Void',
      'name': 'Void',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Void',
      'shortDisplayName': 'Void'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': true,
    'fullyQualifiedName': 'lang.taxi.Void',
    'longDisplayName': 'lang.taxi.Void',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Void',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Void',
      'name': 'Void',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Void',
      'shortDisplayName': 'Void'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Array',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Array',
      'name': 'Array',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Array',
      'shortDisplayName': 'Array'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'A collection of things',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'lang.taxi.Array',
    'longDisplayName': 'lang.taxi.Array',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Array',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Array',
      'name': 'Array',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Array',
      'shortDisplayName': 'Array'
    },
    'underlyingTypeParameters': [],
    'isCollection': true,
    'isStream': false,
    'collectionType': {
      'fullyQualifiedName': 'lang.taxi.Any',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Any',
      'name': 'Any',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Any',
      'shortDisplayName': 'Any'
    },
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'lang.taxi.Stream',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Stream',
      'name': 'Stream',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Stream',
      'shortDisplayName': 'Stream'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': '<unknown>',
      'version': '0.0.0',
      'content': 'Native',
      'id': '<unknown>:0.0.0',
      'contentHash': 'd509e4'
    }],
    'typeParameters': [],
    'typeDoc': 'Result of a service publishing sequence of events',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'lang.taxi.Stream',
    'longDisplayName': 'lang.taxi.Stream',
    'memberQualifiedName': {
      'fullyQualifiedName': 'lang.taxi.Stream',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Stream',
      'name': 'Stream',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Stream',
      'shortDisplayName': 'Stream'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': true,
    'collectionType': {
      'fullyQualifiedName': 'lang.taxi.Any',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Any',
      'name': 'Any',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Any',
      'shortDisplayName': 'Any'
    },
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'ProductSku',
      'parameters': [],
      'parameterizedName': 'ProductSku',
      'name': 'ProductSku',
      'namespace': '',
      'longDisplayName': 'ProductSku',
      'shortDisplayName': 'ProductSku'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'type ProductSku inherits String',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '3c263d'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'ProductSku',
    'longDisplayName': 'ProductSku',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductSku',
      'parameters': [],
      'parameterizedName': 'ProductSku',
      'name': 'ProductSku',
      'namespace': '',
      'longDisplayName': 'ProductSku',
      'shortDisplayName': 'ProductSku'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'BaseSKU',
      'parameters': [],
      'parameterizedName': 'BaseSKU',
      'name': 'BaseSKU',
      'namespace': '',
      'longDisplayName': 'BaseSKU',
      'shortDisplayName': 'BaseSKU'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': '[[ The base SKU for the product.  Doesn\'t include things such as size or color, but is the basis for derived SKU\'s  ]]\ntype BaseSKU inherits String',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '681b9b'
    }],
    'typeParameters': [],
    'typeDoc': 'The base SKU for the product.  Doesn\'t include things such as size or color, but is the basis for derived SKU\'s',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'BaseSKU',
    'longDisplayName': 'BaseSKU',
    'memberQualifiedName': {
      'fullyQualifiedName': 'BaseSKU',
      'parameters': [],
      'parameterizedName': 'BaseSKU',
      'name': 'BaseSKU',
      'namespace': '',
      'longDisplayName': 'BaseSKU',
      'shortDisplayName': 'BaseSKU'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'Product',
      'parameters': [],
      'parameterizedName': 'Product',
      'name': 'Product',
      'namespace': '',
      'longDisplayName': 'Product',
      'shortDisplayName': 'Product'
    },
    'attributes': {
      'description': {
        'type': {
          'fullyQualifiedName': 'ProductDescription',
          'parameters': [],
          'parameterizedName': 'ProductDescription',
          'name': 'ProductDescription',
          'namespace': '',
          'longDisplayName': 'ProductDescription',
          'shortDisplayName': 'ProductDescription'
        },
        'modifiers': [],
        'typeDoc': 'The product decsription',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductDescription',
        'metadata': [],
        'sourcedBy': null
      },
      'baseSKU': {
        'type': {
          'fullyQualifiedName': 'BaseSKU',
          'parameters': [],
          'parameterizedName': 'BaseSKU',
          'name': 'BaseSKU',
          'namespace': '',
          'longDisplayName': 'BaseSKU',
          'shortDisplayName': 'BaseSKU'
        },
        'modifiers': [],
        'typeDoc': 'The base SKU for the product.  Doesn\'t include things such as size or color, but is the basis for derived SKU\'s',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'BaseSKU',
        'metadata': [],
        'sourcedBy': null
      },
      'color': {
        'type': {
          'fullyQualifiedName': 'ProductColor',
          'parameters': [],
          'parameterizedName': 'ProductColor',
          'name': 'ProductColor',
          'namespace': '',
          'longDisplayName': 'ProductColor',
          'shortDisplayName': 'ProductColor'
        },
        'modifiers': [],
        'typeDoc': 'The color of the item for this SKU',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductColor',
        'metadata': [],
        'sourcedBy': null
      },
      'size': {
        'type': {
          'fullyQualifiedName': 'ProductSize',
          'parameters': [],
          'parameterizedName': 'ProductSize',
          'name': 'ProductSize',
          'namespace': '',
          'longDisplayName': 'ProductSize',
          'shortDisplayName': 'ProductSize'
        },
        'modifiers': [],
        'typeDoc': 'The size of the item for this SKU',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductSize',
        'metadata': [],
        'sourcedBy': null
      },
      'sku': {
        'type': {
          'fullyQualifiedName': 'ProductSku',
          'parameters': [],
          'parameterizedName': 'ProductSku',
          'name': 'ProductSku',
          'namespace': '',
          'longDisplayName': 'ProductSku',
          'shortDisplayName': 'ProductSku'
        },
        'modifiers': [],
        'typeDoc': 'The SKU - the main way of identifying the product',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductSku',
        'metadata': [{
          'name': {
            'fullyQualifiedName': 'Id',
            'parameters': [],
            'parameterizedName': 'Id',
            'name': 'Id',
            'namespace': '',
            'longDisplayName': 'Id',
            'shortDisplayName': 'Id'
          }, 'params': {}
        }],
        'sourcedBy': null
      },
      'variantDescription': {
        'type': {
          'fullyQualifiedName': 'ProductVariantDescription',
          'parameters': [],
          'parameterizedName': 'ProductVariantDescription',
          'name': 'ProductVariantDescription',
          'namespace': '',
          'longDisplayName': 'ProductVariantDescription',
          'shortDisplayName': 'ProductVariantDescription'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductVariantDescription',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': '[[ A product, as defined in our Product service ]]\nmodel Product {\n    [[ The product decsription ]]\n    description: ProductDescription inherits String\n    [[ The base SKU for the product.  Doesn\'t include things such as size or color, but is the basis for derived SKU\'s  ]]\n    baseSKU : BaseSKU inherits String\n    [[ The color of the item for this SKU ]]\n    color : ProductColor inherits String\n    [[ The size of the item for this SKU ]]\n    size : ProductSize inherits String\n    [[ The SKU - the main way of identifying the product ]]\n    @Id\n    sku : ProductSku\n    variantDescription: ProductVariantDescription inherits String\n}',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '81dd65'
    }],
    'typeParameters': [],
    'typeDoc': 'A product, as defined in our Product service',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'Product',
    'longDisplayName': 'Product',
    'memberQualifiedName': {
      'fullyQualifiedName': 'Product',
      'parameters': [],
      'parameterizedName': 'Product',
      'name': 'Product',
      'namespace': '',
      'longDisplayName': 'Product',
      'shortDisplayName': 'Product'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'TransactionProductDescription',
      'parameters': [],
      'parameterizedName': 'TransactionProductDescription',
      'name': 'TransactionProductDescription',
      'namespace': '',
      'longDisplayName': 'TransactionProductDescription',
      'shortDisplayName': 'TransactionProductDescription'
    },
    'attributes': {
      'sku': {
        'type': {
          'fullyQualifiedName': 'TransactionProduct',
          'parameters': [],
          'parameterizedName': 'TransactionProduct',
          'name': 'TransactionProduct',
          'namespace': '',
          'longDisplayName': 'TransactionProduct',
          'shortDisplayName': 'TransactionProduct'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'TransactionProduct',
        'metadata': [],
        'sourcedBy': null
      },
      'description': {
        'type': {
          'fullyQualifiedName': 'ProductVariantDescription',
          'parameters': [],
          'parameterizedName': 'ProductVariantDescription',
          'name': 'ProductVariantDescription',
          'namespace': '',
          'longDisplayName': 'ProductVariantDescription',
          'shortDisplayName': 'ProductVariantDescription'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductVariantDescription',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'model TransactionProductDescription {\n    sku: TransactionProduct\n    description: ProductVariantDescription\n}',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'f84856'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'TransactionProductDescription',
    'longDisplayName': 'TransactionProductDescription',
    'memberQualifiedName': {
      'fullyQualifiedName': 'TransactionProductDescription',
      'parameters': [],
      'parameterizedName': 'TransactionProductDescription',
      'name': 'TransactionProductDescription',
      'namespace': '',
      'longDisplayName': 'TransactionProductDescription',
      'shortDisplayName': 'TransactionProductDescription'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'TransactionProduct',
      'parameters': [],
      'parameterizedName': 'TransactionProduct',
      'name': 'TransactionProduct',
      'namespace': '',
      'longDisplayName': 'TransactionProduct',
      'shortDisplayName': 'TransactionProduct'
    },
    'attributes': {
      'baseSku': {
        'type': {
          'fullyQualifiedName': 'BaseSKU',
          'parameters': [],
          'parameterizedName': 'BaseSKU',
          'name': 'BaseSKU',
          'namespace': '',
          'longDisplayName': 'BaseSKU',
          'shortDisplayName': 'BaseSKU'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'BaseSKU',
        'metadata': [],
        'sourcedBy': null
      },
      'description': {
        'type': {
          'fullyQualifiedName': 'ProductDescription',
          'parameters': [],
          'parameterizedName': 'ProductDescription',
          'name': 'ProductDescription',
          'namespace': '',
          'longDisplayName': 'ProductDescription',
          'shortDisplayName': 'ProductDescription'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductDescription',
        'metadata': [],
        'sourcedBy': null
      },
      'size': {
        'type': {
          'fullyQualifiedName': 'ProductSize',
          'parameters': [],
          'parameterizedName': 'ProductSize',
          'name': 'ProductSize',
          'namespace': '',
          'longDisplayName': 'ProductSize',
          'shortDisplayName': 'ProductSize'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductSize',
        'metadata': [],
        'sourcedBy': null
      },
      'color': {
        'type': {
          'fullyQualifiedName': 'ProductColor',
          'parameters': [],
          'parameterizedName': 'ProductColor',
          'name': 'ProductColor',
          'namespace': '',
          'longDisplayName': 'ProductColor',
          'shortDisplayName': 'ProductColor'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductColor',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'model TransactionProduct {\n    baseSku: BaseSKU\n        description: ProductDescription\n        size: ProductSize\n        color: ProductColor\n}',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '3e085b'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'TransactionProduct',
    'longDisplayName': 'TransactionProduct',
    'memberQualifiedName': {
      'fullyQualifiedName': 'TransactionProduct',
      'parameters': [],
      'parameterizedName': 'TransactionProduct',
      'name': 'TransactionProduct',
      'namespace': '',
      'longDisplayName': 'TransactionProduct',
      'shortDisplayName': 'TransactionProduct'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'OrderTransaction',
      'parameters': [],
      'parameterizedName': 'OrderTransaction',
      'name': 'OrderTransaction',
      'namespace': '',
      'longDisplayName': 'OrderTransaction',
      'shortDisplayName': 'OrderTransaction'
    },
    'attributes': {
      'products': {
        'type': {
          'fullyQualifiedName': 'lang.taxi.Array',
          'parameters': [{
            'fullyQualifiedName': 'TransactionProductDescription',
            'parameters': [],
            'parameterizedName': 'TransactionProductDescription',
            'name': 'TransactionProductDescription',
            'namespace': '',
            'longDisplayName': 'TransactionProductDescription',
            'shortDisplayName': 'TransactionProductDescription'
          }],
          'parameterizedName': 'lang.taxi.Array<TransactionProductDescription>',
          'name': 'Array',
          'namespace': 'lang.taxi',
          'longDisplayName': 'TransactionProductDescription[]',
          'shortDisplayName': 'TransactionProductDescription[]'
        },
        'modifiers': [],
        'typeDoc': 'The products included in this transcation',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'TransactionProductDescription[]',
        'metadata': [],
        'sourcedBy': null
      },
      'time': {
        'type': {
          'fullyQualifiedName': 'TransactionTime',
          'parameters': [],
          'parameterizedName': 'TransactionTime',
          'name': 'TransactionTime',
          'namespace': '',
          'longDisplayName': 'TransactionTime',
          'shortDisplayName': 'TransactionTime'
        },
        'modifiers': [],
        'typeDoc': 'The date and time of the transaction',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'TransactionTime',
        'metadata': [],
        'sourcedBy': null
      },
      'customer': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerEmailAddress',
          'parameters': [],
          'parameterizedName': 'demo.CustomerEmailAddress',
          'name': 'CustomerEmailAddress',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerEmailAddress',
          'shortDisplayName': 'CustomerEmailAddress'
        },
        'modifiers': [],
        'typeDoc': 'The email address of the purchasing customer',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerEmailAddress',
        'metadata': [],
        'sourcedBy': null
      },
      'value': {
        'type': {
          'fullyQualifiedName': 'TransactionPrice',
          'parameters': [],
          'parameterizedName': 'TransactionPrice',
          'name': 'TransactionPrice',
          'namespace': '',
          'longDisplayName': 'TransactionPrice',
          'shortDisplayName': 'TransactionPrice'
        },
        'modifiers': [],
        'typeDoc': 'The value of the transaction',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'TransactionPrice',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': '[[ Models transactions as they occur in our order tracking system ]]\nmodel OrderTransaction {\n    [[ The products included in this transcation  ]]\n    products: TransactionProductDescription[]\n    [[ The date and time of the transaction ]]\n    time : TransactionTime inherits Instant\n    [[ The email address of the purchasing customer ]]\n    customer : CustomerEmailAddress\n    [[ The value of the transaction ]]\n    value : TransactionPrice inherits Decimal\n}',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'c75550'
    }],
    'typeParameters': [],
    'typeDoc': 'Models transactions as they occur in our order tracking system',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'OrderTransaction',
    'longDisplayName': 'OrderTransaction',
    'memberQualifiedName': {
      'fullyQualifiedName': 'OrderTransaction',
      'parameters': [],
      'parameterizedName': 'OrderTransaction',
      'name': 'OrderTransaction',
      'namespace': '',
      'longDisplayName': 'OrderTransaction',
      'shortDisplayName': 'OrderTransaction'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'QueryResultType',
      'parameters': [],
      'parameterizedName': 'QueryResultType',
      'name': 'QueryResultType',
      'namespace': '',
      'longDisplayName': 'QueryResultType',
      'shortDisplayName': 'QueryResultType'
    },
    'attributes': {
      'customer': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerEmailAddress',
          'parameters': [],
          'parameterizedName': 'demo.CustomerEmailAddress',
          'name': 'CustomerEmailAddress',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerEmailAddress',
          'shortDisplayName': 'CustomerEmailAddress'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerEmailAddress',
        'metadata': [],
        'sourcedBy': null
      },
      'name': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerFirstName',
          'parameters': [],
          'parameterizedName': 'demo.CustomerFirstName',
          'name': 'CustomerFirstName',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerFirstName',
          'shortDisplayName': 'CustomerFirstName'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerFirstName',
        'metadata': [{
          'name': {
            'fullyQualifiedName': 'FirstNotEmpty',
            'parameters': [],
            'parameterizedName': 'FirstNotEmpty',
            'name': 'FirstNotEmpty',
            'namespace': '',
            'longDisplayName': 'FirstNotEmpty',
            'shortDisplayName': 'FirstNotEmpty'
          }, 'params': {}
        }],
        'sourcedBy': null
      },
      'products': {
        'type': {
          'fullyQualifiedName': 'lang.taxi.Array',
          'parameters': [{
            'fullyQualifiedName': 'QueryResultType$Products',
            'parameters': [],
            'parameterizedName': 'QueryResultType$Products',
            'name': 'QueryResultType$Products',
            'namespace': '',
            'longDisplayName': 'QueryResultType$Products',
            'shortDisplayName': 'QueryResultType$Products'
          }],
          'parameterizedName': 'lang.taxi.Array<QueryResultType$Products>',
          'name': 'Array',
          'namespace': 'lang.taxi',
          'longDisplayName': 'QueryResultType$Products[]',
          'shortDisplayName': 'QueryResultType$Products[]'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'QueryResultType$Products[]',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'model QueryResultType {\n    customer : CustomerEmailAddress\n    @FirstNotEmpty\n    name : CustomerFirstName\n    products: TransactionProductDescription -> {\n        sku: ProductSku\n        baseSku: BaseSKU\n        description: ProductDescription\n        size: ProductSize\n        color: ProductColor \n\n        nestedSku: String by concat(this.baseSku, \'-\', this.size, \'-\', this.color)\n    }[]\n}',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '9b5e55'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'QueryResultType',
    'longDisplayName': 'QueryResultType',
    'memberQualifiedName': {
      'fullyQualifiedName': 'QueryResultType',
      'parameters': [],
      'parameterizedName': 'QueryResultType',
      'name': 'QueryResultType',
      'namespace': '',
      'longDisplayName': 'QueryResultType',
      'shortDisplayName': 'QueryResultType'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'StockServiceOrderEvent',
      'parameters': [],
      'parameterizedName': 'StockServiceOrderEvent',
      'name': 'StockServiceOrderEvent',
      'namespace': '',
      'longDisplayName': 'StockServiceOrderEvent',
      'shortDisplayName': 'StockServiceOrderEvent'
    },
    'attributes': {
      'customer': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerEmailAddress',
          'parameters': [],
          'parameterizedName': 'demo.CustomerEmailAddress',
          'name': 'CustomerEmailAddress',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerEmailAddress',
          'shortDisplayName': 'CustomerEmailAddress'
        },
        'modifiers': [],
        'typeDoc': 'The customers email address, used for identification',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerEmailAddress',
        'metadata': [],
        'sourcedBy': null
      },
      'firstName': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerFirstName',
          'parameters': [],
          'parameterizedName': 'demo.CustomerFirstName',
          'name': 'CustomerFirstName',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerFirstName',
          'shortDisplayName': 'CustomerFirstName'
        },
        'modifiers': [],
        'typeDoc': 'Customers first name',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerFirstName',
        'metadata': [],
        'sourcedBy': null
      },
      'lastName': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerLastName',
          'parameters': [],
          'parameterizedName': 'demo.CustomerLastName',
          'name': 'CustomerLastName',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerLastName',
          'shortDisplayName': 'CustomerLastName'
        },
        'modifiers': [],
        'typeDoc': 'Customers last name',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerLastName',
        'metadata': [],
        'sourcedBy': null
      },
      'products': {
        'type': {
          'fullyQualifiedName': 'lang.taxi.Array',
          'parameters': [{
            'fullyQualifiedName': 'StockServiceOrderEvent$Products',
            'parameters': [],
            'parameterizedName': 'StockServiceOrderEvent$Products',
            'name': 'StockServiceOrderEvent$Products',
            'namespace': '',
            'longDisplayName': 'StockServiceOrderEvent$Products',
            'shortDisplayName': 'StockServiceOrderEvent$Products'
          }],
          'parameterizedName': 'lang.taxi.Array<StockServiceOrderEvent$Products>',
          'name': 'Array',
          'namespace': 'lang.taxi',
          'longDisplayName': 'StockServiceOrderEvent$Products[]',
          'shortDisplayName': 'StockServiceOrderEvent$Products[]'
        },
        'modifiers': [],
        'typeDoc': 'A collection of items included in this order',
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'StockServiceOrderEvent$Products[]',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': '[[ Describes an update of stock levels from an order, into our stock management service\n\n]]\nmodel StockServiceOrderEvent {\n    [[ The customers email address, used for identification ]]\n    customer : CustomerEmailAddress\n    [[ Customers first name]]\n    firstName : CustomerFirstName\n    [[ Customers last name]]\n    lastName : CustomerLastName\n\n    [[ A collection of items included in this order ]]\n    products: TransactionProductDescription -> {\n        baseSku: BaseSKU\n        size: ProductSize\n        color: ProductColor \n    }[]\n}',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '35c884'
    }],
    'typeParameters': [],
    'typeDoc': 'Describes an update of stock levels from an order, into our stock management service',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'StockServiceOrderEvent',
    'longDisplayName': 'StockServiceOrderEvent',
    'memberQualifiedName': {
      'fullyQualifiedName': 'StockServiceOrderEvent',
      'parameters': [],
      'parameterizedName': 'StockServiceOrderEvent',
      'name': 'StockServiceOrderEvent',
      'namespace': '',
      'longDisplayName': 'StockServiceOrderEvent',
      'shortDisplayName': 'StockServiceOrderEvent'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'io.vyne.Username',
      'parameters': [],
      'parameterizedName': 'io.vyne.Username',
      'name': 'Username',
      'namespace': 'io.vyne',
      'longDisplayName': 'io.vyne.Username',
      'shortDisplayName': 'Username'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'io.vyne.types',
      'version': '0.0.0',
      'content': 'type Username inherits String',
      'id': 'io.vyne.types:0.0.0',
      'contentHash': 'de78f4'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'io.vyne.Username',
    'longDisplayName': 'io.vyne.Username',
    'memberQualifiedName': {
      'fullyQualifiedName': 'io.vyne.Username',
      'parameters': [],
      'parameterizedName': 'io.vyne.Username',
      'name': 'Username',
      'namespace': 'io.vyne',
      'longDisplayName': 'io.vyne.Username',
      'shortDisplayName': 'Username'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'demo.CustomerId',
      'parameters': [],
      'parameterizedName': 'demo.CustomerId',
      'name': 'CustomerId',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerId',
      'shortDisplayName': 'CustomerId'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.Int',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Int',
      'name': 'Int',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Int',
      'shortDisplayName': 'Int'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Customers.taxi',
      'version': '0.0.0',
      'content': 'type CustomerId inherits lang.taxi.Int',
      'id': 'Customers.taxi:0.0.0',
      'contentHash': '0fee35'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Int',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Int',
      'name': 'Int',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Int',
      'shortDisplayName': 'Int'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'demo.CustomerId',
    'longDisplayName': 'demo.CustomerId',
    'memberQualifiedName': {
      'fullyQualifiedName': 'demo.CustomerId',
      'parameters': [],
      'parameterizedName': 'demo.CustomerId',
      'name': 'CustomerId',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerId',
      'shortDisplayName': 'CustomerId'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'demo.CustomerEmailAddress',
      'parameters': [],
      'parameterizedName': 'demo.CustomerEmailAddress',
      'name': 'CustomerEmailAddress',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerEmailAddress',
      'shortDisplayName': 'CustomerEmailAddress'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Customers.taxi',
      'version': '0.0.0',
      'content': 'type CustomerEmailAddress inherits String',
      'id': 'Customers.taxi:0.0.0',
      'contentHash': '8da0fa'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'demo.CustomerEmailAddress',
    'longDisplayName': 'demo.CustomerEmailAddress',
    'memberQualifiedName': {
      'fullyQualifiedName': 'demo.CustomerEmailAddress',
      'parameters': [],
      'parameterizedName': 'demo.CustomerEmailAddress',
      'name': 'CustomerEmailAddress',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerEmailAddress',
      'shortDisplayName': 'CustomerEmailAddress'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'demo.CustomerFirstName',
      'parameters': [],
      'parameterizedName': 'demo.CustomerFirstName',
      'name': 'CustomerFirstName',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerFirstName',
      'shortDisplayName': 'CustomerFirstName'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Customers.taxi',
      'version': '0.0.0',
      'content': 'type CustomerFirstName inherits lang.taxi.String',
      'id': 'Customers.taxi:0.0.0',
      'contentHash': 'a3fabb'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'demo.CustomerFirstName',
    'longDisplayName': 'demo.CustomerFirstName',
    'memberQualifiedName': {
      'fullyQualifiedName': 'demo.CustomerFirstName',
      'parameters': [],
      'parameterizedName': 'demo.CustomerFirstName',
      'name': 'CustomerFirstName',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerFirstName',
      'shortDisplayName': 'CustomerFirstName'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'demo.CustomerLastName',
      'parameters': [],
      'parameterizedName': 'demo.CustomerLastName',
      'name': 'CustomerLastName',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerLastName',
      'shortDisplayName': 'CustomerLastName'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Customers.taxi',
      'version': '0.0.0',
      'content': 'type CustomerLastName inherits lang.taxi.String',
      'id': 'Customers.taxi:0.0.0',
      'contentHash': 'edb9b7'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'demo.CustomerLastName',
    'longDisplayName': 'demo.CustomerLastName',
    'memberQualifiedName': {
      'fullyQualifiedName': 'demo.CustomerLastName',
      'parameters': [],
      'parameterizedName': 'demo.CustomerLastName',
      'name': 'CustomerLastName',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerLastName',
      'shortDisplayName': 'CustomerLastName'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'demo.Postcode',
      'parameters': [],
      'parameterizedName': 'demo.Postcode',
      'name': 'Postcode',
      'namespace': 'demo',
      'longDisplayName': 'demo.Postcode',
      'shortDisplayName': 'Postcode'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Customers.taxi',
      'version': '0.0.0',
      'content': 'type Postcode inherits lang.taxi.String',
      'id': 'Customers.taxi:0.0.0',
      'contentHash': 'e2b69e'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'demo.Postcode',
    'longDisplayName': 'demo.Postcode',
    'memberQualifiedName': {
      'fullyQualifiedName': 'demo.Postcode',
      'parameters': [],
      'parameterizedName': 'demo.Postcode',
      'name': 'Postcode',
      'namespace': 'demo',
      'longDisplayName': 'demo.Postcode',
      'shortDisplayName': 'Postcode'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'demo.CustomerUpdateTrigger',
      'parameters': [],
      'parameterizedName': 'demo.CustomerUpdateTrigger',
      'name': 'CustomerUpdateTrigger',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerUpdateTrigger',
      'shortDisplayName': 'CustomerUpdateTrigger'
    },
    'attributes': {
      'email': {
        'type': {
          'fullyQualifiedName': 'demo.CustomerEmailAddress',
          'parameters': [],
          'parameterizedName': 'demo.CustomerEmailAddress',
          'name': 'CustomerEmailAddress',
          'namespace': 'demo',
          'longDisplayName': 'demo.CustomerEmailAddress',
          'shortDisplayName': 'CustomerEmailAddress'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'demo.CustomerEmailAddress',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Customers.taxi',
      'version': '0.0.0',
      'content': 'model CustomerUpdateTrigger {\n    email : CustomerEmailAddress\n}',
      'id': 'Customers.taxi:0.0.0',
      'contentHash': 'ffa610'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'demo.CustomerUpdateTrigger',
    'longDisplayName': 'demo.CustomerUpdateTrigger',
    'memberQualifiedName': {
      'fullyQualifiedName': 'demo.CustomerUpdateTrigger',
      'parameters': [],
      'parameterizedName': 'demo.CustomerUpdateTrigger',
      'name': 'CustomerUpdateTrigger',
      'namespace': 'demo',
      'longDisplayName': 'demo.CustomerUpdateTrigger',
      'shortDisplayName': 'CustomerUpdateTrigger'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'ProductDescription',
      'parameters': [],
      'parameterizedName': 'ProductDescription',
      'name': 'ProductDescription',
      'namespace': '',
      'longDisplayName': 'ProductDescription',
      'shortDisplayName': 'ProductDescription'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'ProductDescription inherits String',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '9071ba'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'ProductDescription',
    'longDisplayName': 'ProductDescription',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductDescription',
      'parameters': [],
      'parameterizedName': 'ProductDescription',
      'name': 'ProductDescription',
      'namespace': '',
      'longDisplayName': 'ProductDescription',
      'shortDisplayName': 'ProductDescription'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'ProductColor',
      'parameters': [],
      'parameterizedName': 'ProductColor',
      'name': 'ProductColor',
      'namespace': '',
      'longDisplayName': 'ProductColor',
      'shortDisplayName': 'ProductColor'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'ProductColor inherits String',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'a7356a'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'ProductColor',
    'longDisplayName': 'ProductColor',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductColor',
      'parameters': [],
      'parameterizedName': 'ProductColor',
      'name': 'ProductColor',
      'namespace': '',
      'longDisplayName': 'ProductColor',
      'shortDisplayName': 'ProductColor'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'ProductSize',
      'parameters': [],
      'parameterizedName': 'ProductSize',
      'name': 'ProductSize',
      'namespace': '',
      'longDisplayName': 'ProductSize',
      'shortDisplayName': 'ProductSize'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'ProductSize inherits String',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'd5c2e8'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'ProductSize',
    'longDisplayName': 'ProductSize',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductSize',
      'parameters': [],
      'parameterizedName': 'ProductSize',
      'name': 'ProductSize',
      'namespace': '',
      'longDisplayName': 'ProductSize',
      'shortDisplayName': 'ProductSize'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'ProductVariantDescription',
      'parameters': [],
      'parameterizedName': 'ProductVariantDescription',
      'name': 'ProductVariantDescription',
      'namespace': '',
      'longDisplayName': 'ProductVariantDescription',
      'shortDisplayName': 'ProductVariantDescription'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'ProductVariantDescription inherits String',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '15d2a0'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.String',
      'parameters': [],
      'parameterizedName': 'lang.taxi.String',
      'name': 'String',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.String',
      'shortDisplayName': 'String'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'ProductVariantDescription',
    'longDisplayName': 'ProductVariantDescription',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductVariantDescription',
      'parameters': [],
      'parameterizedName': 'ProductVariantDescription',
      'name': 'ProductVariantDescription',
      'namespace': '',
      'longDisplayName': 'ProductVariantDescription',
      'shortDisplayName': 'ProductVariantDescription'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'TransactionTime',
      'parameters': [],
      'parameterizedName': 'TransactionTime',
      'name': 'TransactionTime',
      'namespace': '',
      'longDisplayName': 'TransactionTime',
      'shortDisplayName': 'TransactionTime'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.Instant',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Instant',
      'name': 'Instant',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Instant',
      'shortDisplayName': 'Instant'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'TransactionTime inherits Instant',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'd573ce'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': ['yyyy-MM-dd\'T\'HH:mm:ss[.SSS]X'],
    'hasFormat': true,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Instant',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Instant',
      'name': 'Instant',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Instant',
      'shortDisplayName': 'Instant'
    },
    'unformattedTypeName': {
      'fullyQualifiedName': 'TransactionTime',
      'parameters': [],
      'parameterizedName': 'TransactionTime',
      'name': 'TransactionTime',
      'namespace': '',
      'longDisplayName': 'TransactionTime',
      'shortDisplayName': 'TransactionTime'
    },
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'TransactionTime',
    'longDisplayName': 'TransactionTime(yyyy-MM-dd\'T\'HH:mm:ss[.SSS]X)',
    'memberQualifiedName': {
      'fullyQualifiedName': 'TransactionTime',
      'parameters': [],
      'parameterizedName': 'TransactionTime',
      'name': 'TransactionTime',
      'namespace': '',
      'longDisplayName': 'TransactionTime',
      'shortDisplayName': 'TransactionTime'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'TransactionPrice',
      'parameters': [],
      'parameterizedName': 'TransactionPrice',
      'name': 'TransactionPrice',
      'namespace': '',
      'longDisplayName': 'TransactionPrice',
      'shortDisplayName': 'TransactionPrice'
    },
    'attributes': {},
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [{
      'fullyQualifiedName': 'lang.taxi.Decimal',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Decimal',
      'name': 'Decimal',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Decimal',
      'shortDisplayName': 'Decimal'
    }],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': 'TransactionPrice inherits Decimal',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'ab7b92'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': {
      'fullyQualifiedName': 'lang.taxi.Decimal',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Decimal',
      'name': 'Decimal',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Decimal',
      'shortDisplayName': 'Decimal'
    },
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'TransactionPrice',
    'longDisplayName': 'TransactionPrice',
    'memberQualifiedName': {
      'fullyQualifiedName': 'TransactionPrice',
      'parameters': [],
      'parameterizedName': 'TransactionPrice',
      'name': 'TransactionPrice',
      'namespace': '',
      'longDisplayName': 'TransactionPrice',
      'shortDisplayName': 'TransactionPrice'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': true
  }, {
    'name': {
      'fullyQualifiedName': 'QueryResultType$Products',
      'parameters': [],
      'parameterizedName': 'QueryResultType$Products',
      'name': 'QueryResultType$Products',
      'namespace': '',
      'longDisplayName': 'QueryResultType$Products',
      'shortDisplayName': 'QueryResultType$Products'
    },
    'attributes': {
      'sku': {
        'type': {
          'fullyQualifiedName': 'ProductSku',
          'parameters': [],
          'parameterizedName': 'ProductSku',
          'name': 'ProductSku',
          'namespace': '',
          'longDisplayName': 'ProductSku',
          'shortDisplayName': 'ProductSku'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductSku',
        'metadata': [],
        'sourcedBy': null
      },
      'baseSku': {
        'type': {
          'fullyQualifiedName': 'BaseSKU',
          'parameters': [],
          'parameterizedName': 'BaseSKU',
          'name': 'BaseSKU',
          'namespace': '',
          'longDisplayName': 'BaseSKU',
          'shortDisplayName': 'BaseSKU'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'BaseSKU',
        'metadata': [],
        'sourcedBy': null
      },
      'description': {
        'type': {
          'fullyQualifiedName': 'ProductDescription',
          'parameters': [],
          'parameterizedName': 'ProductDescription',
          'name': 'ProductDescription',
          'namespace': '',
          'longDisplayName': 'ProductDescription',
          'shortDisplayName': 'ProductDescription'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductDescription',
        'metadata': [],
        'sourcedBy': null
      },
      'size': {
        'type': {
          'fullyQualifiedName': 'ProductSize',
          'parameters': [],
          'parameterizedName': 'ProductSize',
          'name': 'ProductSize',
          'namespace': '',
          'longDisplayName': 'ProductSize',
          'shortDisplayName': 'ProductSize'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductSize',
        'metadata': [],
        'sourcedBy': null
      },
      'color': {
        'type': {
          'fullyQualifiedName': 'ProductColor',
          'parameters': [],
          'parameterizedName': 'ProductColor',
          'name': 'ProductColor',
          'namespace': '',
          'longDisplayName': 'ProductColor',
          'shortDisplayName': 'ProductColor'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductColor',
        'metadata': [],
        'sourcedBy': null
      },
      'nestedSku': {
        'type': {
          'fullyQualifiedName': 'lang.taxi.String',
          'parameters': [],
          'parameterizedName': 'lang.taxi.String',
          'name': 'String',
          'namespace': 'lang.taxi',
          'longDisplayName': 'lang.taxi.String',
          'shortDisplayName': 'String'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'lang.taxi.String',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': '{\n        sku: ProductSku\n        baseSku: BaseSKU\n        description: ProductDescription\n        size: ProductSize\n        color: ProductColor \n\n        nestedSku: String by concat(this.baseSku, \'-\', this.size, \'-\', this.color)\n    }',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': 'a2209c'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'QueryResultType$Products',
    'longDisplayName': 'QueryResultType$Products',
    'memberQualifiedName': {
      'fullyQualifiedName': 'QueryResultType$Products',
      'parameters': [],
      'parameterizedName': 'QueryResultType$Products',
      'name': 'QueryResultType$Products',
      'namespace': '',
      'longDisplayName': 'QueryResultType$Products',
      'shortDisplayName': 'QueryResultType$Products'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }, {
    'name': {
      'fullyQualifiedName': 'StockServiceOrderEvent$Products',
      'parameters': [],
      'parameterizedName': 'StockServiceOrderEvent$Products',
      'name': 'StockServiceOrderEvent$Products',
      'namespace': '',
      'longDisplayName': 'StockServiceOrderEvent$Products',
      'shortDisplayName': 'StockServiceOrderEvent$Products'
    },
    'attributes': {
      'baseSku': {
        'type': {
          'fullyQualifiedName': 'BaseSKU',
          'parameters': [],
          'parameterizedName': 'BaseSKU',
          'name': 'BaseSKU',
          'namespace': '',
          'longDisplayName': 'BaseSKU',
          'shortDisplayName': 'BaseSKU'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'BaseSKU',
        'metadata': [],
        'sourcedBy': null
      },
      'size': {
        'type': {
          'fullyQualifiedName': 'ProductSize',
          'parameters': [],
          'parameterizedName': 'ProductSize',
          'name': 'ProductSize',
          'namespace': '',
          'longDisplayName': 'ProductSize',
          'shortDisplayName': 'ProductSize'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductSize',
        'metadata': [],
        'sourcedBy': null
      },
      'color': {
        'type': {
          'fullyQualifiedName': 'ProductColor',
          'parameters': [],
          'parameterizedName': 'ProductColor',
          'name': 'ProductColor',
          'namespace': '',
          'longDisplayName': 'ProductColor',
          'shortDisplayName': 'ProductColor'
        },
        'modifiers': [],
        'typeDoc': null,
        'defaultValue': null,
        'nullable': false,
        'typeDisplayName': 'ProductColor',
        'metadata': [],
        'sourcedBy': null
      }
    },
    'modifiers': [],
    'metadata': [],
    'aliasForType': null,
    'inheritsFrom': [],
    'enumValues': [],
    'sources': [{
      'name': 'Orders.taxi',
      'version': '0.0.0',
      'content': '{\n        baseSku: BaseSKU\n        size: ProductSize\n        color: ProductColor \n    }',
      'id': 'Orders.taxi:0.0.0',
      'contentHash': '3ee1a6'
    }],
    'typeParameters': [],
    'typeDoc': '',
    'isTypeAlias': false,
    'offset': null,
    'format': null,
    'hasFormat': false,
    'isCalculated': false,
    'basePrimitiveTypeName': null,
    'unformattedTypeName': null,
    'isParameterType': false,
    'isClosed': false,
    'isPrimitive': false,
    'fullyQualifiedName': 'StockServiceOrderEvent$Products',
    'longDisplayName': 'StockServiceOrderEvent$Products',
    'memberQualifiedName': {
      'fullyQualifiedName': 'StockServiceOrderEvent$Products',
      'parameters': [],
      'parameterizedName': 'StockServiceOrderEvent$Products',
      'name': 'StockServiceOrderEvent$Products',
      'namespace': '',
      'longDisplayName': 'StockServiceOrderEvent$Products',
      'shortDisplayName': 'StockServiceOrderEvent$Products'
    },
    'underlyingTypeParameters': [],
    'isCollection': false,
    'isStream': false,
    'collectionType': null,
    'isScalar': false
  }], 'services': [{
    'name': {
      'fullyQualifiedName': 'ProductService',
      'parameters': [],
      'parameterizedName': 'ProductService',
      'name': 'ProductService',
      'namespace': '',
      'longDisplayName': 'ProductService',
      'shortDisplayName': 'ProductService'
    },
    'operations': [{
      'qualifiedName': {
        'fullyQualifiedName': 'ProductService@@listAllProducts',
        'parameters': [],
        'parameterizedName': 'ProductService@@listAllProducts',
        'name': 'ProductService@@listAllProducts',
        'namespace': '',
        'longDisplayName': 'ProductService@@listAllProducts',
        'shortDisplayName': 'ProductService@@listAllProducts'
      },
      'parameters': [],
      'returnType': {
        'fullyQualifiedName': 'lang.taxi.Array',
        'parameters': [{
          'fullyQualifiedName': 'Product',
          'parameters': [],
          'parameterizedName': 'Product',
          'name': 'Product',
          'namespace': '',
          'longDisplayName': 'Product',
          'shortDisplayName': 'Product'
        }],
        'parameterizedName': 'lang.taxi.Array<Product>',
        'name': 'Array',
        'namespace': 'lang.taxi',
        'longDisplayName': 'Product[]',
        'shortDisplayName': 'Product[]'
      },
      'operationType': null,
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'HttpOperation',
          'parameters': [],
          'parameterizedName': 'HttpOperation',
          'name': 'HttpOperation',
          'namespace': '',
          'longDisplayName': 'HttpOperation',
          'shortDisplayName': 'HttpOperation'
        }, 'params': {'method': 'GET', 'url': 'http://localhost:9650/products'}
      }],
      'contract': {
        'returnType': {
          'fullyQualifiedName': 'lang.taxi.Array',
          'parameters': [{
            'fullyQualifiedName': 'Product',
            'parameters': [],
            'parameterizedName': 'Product',
            'name': 'Product',
            'namespace': '',
            'longDisplayName': 'Product',
            'shortDisplayName': 'Product'
          }],
          'parameterizedName': 'lang.taxi.Array<Product>',
          'name': 'Array',
          'namespace': 'lang.taxi',
          'longDisplayName': 'Product[]',
          'shortDisplayName': 'Product[]'
        }, 'constraints': []
      },
      'typeDoc': null,
      'name': 'listAllProducts',
      'memberQualifiedName': {
        'fullyQualifiedName': 'ProductService@@listAllProducts',
        'parameters': [],
        'parameterizedName': 'ProductService@@listAllProducts',
        'name': 'ProductService@@listAllProducts',
        'namespace': '',
        'longDisplayName': 'ProductService@@listAllProducts',
        'shortDisplayName': 'ProductService@@listAllProducts'
      }
    }, {
      'qualifiedName': {
        'fullyQualifiedName': 'ProductService@@findProduct',
        'parameters': [],
        'parameterizedName': 'ProductService@@findProduct',
        'name': 'ProductService@@findProduct',
        'namespace': '',
        'longDisplayName': 'ProductService@@findProduct',
        'shortDisplayName': 'ProductService@@findProduct'
      },
      'parameters': [{
        'type': {
          'fullyQualifiedName': 'ProductSku',
          'parameters': [],
          'parameterizedName': 'ProductSku',
          'name': 'ProductSku',
          'namespace': '',
          'longDisplayName': 'ProductSku',
          'shortDisplayName': 'ProductSku'
        },
        'name': 'sku',
        'metadata': [{
          'name': {
            'fullyQualifiedName': 'PathVariable',
            'parameters': [],
            'parameterizedName': 'PathVariable',
            'name': 'PathVariable',
            'namespace': '',
            'longDisplayName': 'PathVariable',
            'shortDisplayName': 'PathVariable'
          }, 'params': {'name': 'sku'}
        }],
        'constraints': []
      }],
      'returnType': {
        'fullyQualifiedName': 'Product',
        'parameters': [],
        'parameterizedName': 'Product',
        'name': 'Product',
        'namespace': '',
        'longDisplayName': 'Product',
        'shortDisplayName': 'Product'
      },
      'operationType': null,
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'HttpOperation',
          'parameters': [],
          'parameterizedName': 'HttpOperation',
          'name': 'HttpOperation',
          'namespace': '',
          'longDisplayName': 'HttpOperation',
          'shortDisplayName': 'HttpOperation'
        }, 'params': {'method': 'GET', 'url': 'http://localhost:9650/products/{sku}'}
      }],
      'contract': {
        'returnType': {
          'fullyQualifiedName': 'Product',
          'parameters': [],
          'parameterizedName': 'Product',
          'name': 'Product',
          'namespace': '',
          'longDisplayName': 'Product',
          'shortDisplayName': 'Product'
        }, 'constraints': []
      },
      'typeDoc': null,
      'name': 'findProduct',
      'memberQualifiedName': {
        'fullyQualifiedName': 'ProductService@@findProduct',
        'parameters': [],
        'parameterizedName': 'ProductService@@findProduct',
        'name': 'ProductService@@findProduct',
        'namespace': '',
        'longDisplayName': 'ProductService@@findProduct',
        'shortDisplayName': 'ProductService@@findProduct'
      }
    }],
    'queryOperations': [],
    'metadata': [],
    'typeDoc': null,
    'qualifiedName': 'ProductService',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductService',
      'parameters': [],
      'parameterizedName': 'ProductService',
      'name': 'ProductService',
      'namespace': '',
      'longDisplayName': 'ProductService',
      'shortDisplayName': 'ProductService'
    }
  }, {
    'name': {
      'fullyQualifiedName': 'OrderService',
      'parameters': [],
      'parameterizedName': 'OrderService',
      'name': 'OrderService',
      'namespace': '',
      'longDisplayName': 'OrderService',
      'shortDisplayName': 'OrderService'
    },
    'operations': [{
      'qualifiedName': {
        'fullyQualifiedName': 'OrderService@@listOrders',
        'parameters': [],
        'parameterizedName': 'OrderService@@listOrders',
        'name': 'OrderService@@listOrders',
        'namespace': '',
        'longDisplayName': 'OrderService@@listOrders',
        'shortDisplayName': 'OrderService@@listOrders'
      },
      'parameters': [{
        'type': {
          'fullyQualifiedName': 'lang.taxi.Instant',
          'parameters': [],
          'parameterizedName': 'lang.taxi.Instant',
          'name': 'Instant',
          'namespace': 'lang.taxi',
          'longDisplayName': 'lang.taxi.Instant',
          'shortDisplayName': 'Instant'
        },
        'name': 'since',
        'metadata': [{
          'name': {
            'fullyQualifiedName': 'PathVariable',
            'parameters': [],
            'parameterizedName': 'PathVariable',
            'name': 'PathVariable',
            'namespace': '',
            'longDisplayName': 'PathVariable',
            'shortDisplayName': 'PathVariable'
          }, 'params': {'name': 'since'}
        }],
        'constraints': []
      }],
      'returnType': {
        'fullyQualifiedName': 'lang.taxi.Array',
        'parameters': [{
          'fullyQualifiedName': 'OrderTransaction',
          'parameters': [],
          'parameterizedName': 'OrderTransaction',
          'name': 'OrderTransaction',
          'namespace': '',
          'longDisplayName': 'OrderTransaction',
          'shortDisplayName': 'OrderTransaction'
        }],
        'parameterizedName': 'lang.taxi.Array<OrderTransaction>',
        'name': 'Array',
        'namespace': 'lang.taxi',
        'longDisplayName': 'OrderTransaction[]',
        'shortDisplayName': 'OrderTransaction[]'
      },
      'operationType': null,
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'HttpOperation',
          'parameters': [],
          'parameterizedName': 'HttpOperation',
          'name': 'HttpOperation',
          'namespace': '',
          'longDisplayName': 'HttpOperation',
          'shortDisplayName': 'HttpOperation'
        }, 'params': {'method': 'GET', 'url': 'http://localhost:9650/orders/since/{since}'}
      }],
      'contract': {
        'returnType': {
          'fullyQualifiedName': 'lang.taxi.Array',
          'parameters': [{
            'fullyQualifiedName': 'OrderTransaction',
            'parameters': [],
            'parameterizedName': 'OrderTransaction',
            'name': 'OrderTransaction',
            'namespace': '',
            'longDisplayName': 'OrderTransaction',
            'shortDisplayName': 'OrderTransaction'
          }],
          'parameterizedName': 'lang.taxi.Array<OrderTransaction>',
          'name': 'Array',
          'namespace': 'lang.taxi',
          'longDisplayName': 'OrderTransaction[]',
          'shortDisplayName': 'OrderTransaction[]'
        },
        'constraints': [{
          'propertyIdentifier': {
            'type': {
              'namespace': '',
              'typeName': 'TransactionTime',
              'parameters': [],
              'parameterizedName': 'TransactionTime',
              'fullyQualifiedName': 'TransactionTime',
              'firstTypeParameterOrSelf': 'TransactionTime'
            }, 'description': 'type TransactionTime', 'taxi': 'TransactionTime'
          },
          'operator': 'GREATER_THAN',
          'expectedValue': {'path': {'parts': ['since'], 'path': 'since'}, 'taxi': 'since'},
          'compilationUnits': []
        }]
      },
      'typeDoc': null,
      'name': 'listOrders',
      'memberQualifiedName': {
        'fullyQualifiedName': 'OrderService@@listOrders',
        'parameters': [],
        'parameterizedName': 'OrderService@@listOrders',
        'name': 'OrderService@@listOrders',
        'namespace': '',
        'longDisplayName': 'OrderService@@listOrders',
        'shortDisplayName': 'OrderService@@listOrders'
      }
    }],
    'queryOperations': [],
    'metadata': [],
    'typeDoc': null,
    'qualifiedName': 'OrderService',
    'memberQualifiedName': {
      'fullyQualifiedName': 'OrderService',
      'parameters': [],
      'parameterizedName': 'OrderService',
      'name': 'OrderService',
      'namespace': '',
      'longDisplayName': 'OrderService',
      'shortDisplayName': 'OrderService'
    }
  }, {
    'name': {
      'fullyQualifiedName': 'StockService',
      'parameters': [],
      'parameterizedName': 'StockService',
      'name': 'StockService',
      'namespace': '',
      'longDisplayName': 'StockService',
      'shortDisplayName': 'StockService'
    },
    'operations': [{
      'qualifiedName': {
        'fullyQualifiedName': 'StockService@@submitOrders',
        'parameters': [],
        'parameterizedName': 'StockService@@submitOrders',
        'name': 'StockService@@submitOrders',
        'namespace': '',
        'longDisplayName': 'StockService@@submitOrders',
        'shortDisplayName': 'StockService@@submitOrders'
      },
      'parameters': [{
        'type': {
          'fullyQualifiedName': 'lang.taxi.Array',
          'parameters': [{
            'fullyQualifiedName': 'StockServiceOrderEvent',
            'parameters': [],
            'parameterizedName': 'StockServiceOrderEvent',
            'name': 'StockServiceOrderEvent',
            'namespace': '',
            'longDisplayName': 'StockServiceOrderEvent',
            'shortDisplayName': 'StockServiceOrderEvent'
          }],
          'parameterizedName': 'lang.taxi.Array<StockServiceOrderEvent>',
          'name': 'Array',
          'namespace': 'lang.taxi',
          'longDisplayName': 'StockServiceOrderEvent[]',
          'shortDisplayName': 'StockServiceOrderEvent[]'
        },
        'name': 'orders',
        'metadata': [{
          'name': {
            'fullyQualifiedName': 'RequestBody',
            'parameters': [],
            'parameterizedName': 'RequestBody',
            'name': 'RequestBody',
            'namespace': '',
            'longDisplayName': 'RequestBody',
            'shortDisplayName': 'RequestBody'
          }, 'params': {}
        }],
        'constraints': []
      }],
      'returnType': {
        'fullyQualifiedName': 'lang.taxi.Void',
        'parameters': [],
        'parameterizedName': 'lang.taxi.Void',
        'name': 'Void',
        'namespace': 'lang.taxi',
        'longDisplayName': 'lang.taxi.Void',
        'shortDisplayName': 'Void'
      },
      'operationType': null,
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'HttpOperation',
          'parameters': [],
          'parameterizedName': 'HttpOperation',
          'name': 'HttpOperation',
          'namespace': '',
          'longDisplayName': 'HttpOperation',
          'shortDisplayName': 'HttpOperation'
        }, 'params': {'method': 'POST', 'url': 'http://localhost:9650/stock/orders'}
      }],
      'contract': {
        'returnType': {
          'fullyQualifiedName': 'lang.taxi.Void',
          'parameters': [],
          'parameterizedName': 'lang.taxi.Void',
          'name': 'Void',
          'namespace': 'lang.taxi',
          'longDisplayName': 'lang.taxi.Void',
          'shortDisplayName': 'Void'
        }, 'constraints': []
      },
      'typeDoc': null,
      'name': 'submitOrders',
      'memberQualifiedName': {
        'fullyQualifiedName': 'StockService@@submitOrders',
        'parameters': [],
        'parameterizedName': 'StockService@@submitOrders',
        'name': 'StockService@@submitOrders',
        'namespace': '',
        'longDisplayName': 'StockService@@submitOrders',
        'shortDisplayName': 'StockService@@submitOrders'
      }
    }],
    'queryOperations': [],
    'metadata': [],
    'typeDoc': null,
    'qualifiedName': 'StockService',
    'memberQualifiedName': {
      'fullyQualifiedName': 'StockService',
      'parameters': [],
      'parameterizedName': 'StockService',
      'name': 'StockService',
      'namespace': '',
      'longDisplayName': 'StockService',
      'shortDisplayName': 'StockService'
    }
  }], 'operations': [{
    'qualifiedName': {
      'fullyQualifiedName': 'ProductService@@listAllProducts',
      'parameters': [],
      'parameterizedName': 'ProductService@@listAllProducts',
      'name': 'ProductService@@listAllProducts',
      'namespace': '',
      'longDisplayName': 'ProductService@@listAllProducts',
      'shortDisplayName': 'ProductService@@listAllProducts'
    },
    'parameters': [],
    'returnType': {
      'fullyQualifiedName': 'lang.taxi.Array',
      'parameters': [{
        'fullyQualifiedName': 'Product',
        'parameters': [],
        'parameterizedName': 'Product',
        'name': 'Product',
        'namespace': '',
        'longDisplayName': 'Product',
        'shortDisplayName': 'Product'
      }],
      'parameterizedName': 'lang.taxi.Array<Product>',
      'name': 'Array',
      'namespace': 'lang.taxi',
      'longDisplayName': 'Product[]',
      'shortDisplayName': 'Product[]'
    },
    'operationType': null,
    'metadata': [{
      'name': {
        'fullyQualifiedName': 'HttpOperation',
        'parameters': [],
        'parameterizedName': 'HttpOperation',
        'name': 'HttpOperation',
        'namespace': '',
        'longDisplayName': 'HttpOperation',
        'shortDisplayName': 'HttpOperation'
      }, 'params': {'method': 'GET', 'url': 'http://localhost:9650/products'}
    }],
    'contract': {
      'returnType': {
        'fullyQualifiedName': 'lang.taxi.Array',
        'parameters': [{
          'fullyQualifiedName': 'Product',
          'parameters': [],
          'parameterizedName': 'Product',
          'name': 'Product',
          'namespace': '',
          'longDisplayName': 'Product',
          'shortDisplayName': 'Product'
        }],
        'parameterizedName': 'lang.taxi.Array<Product>',
        'name': 'Array',
        'namespace': 'lang.taxi',
        'longDisplayName': 'Product[]',
        'shortDisplayName': 'Product[]'
      }, 'constraints': []
    },
    'typeDoc': null,
    'name': 'listAllProducts',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductService@@listAllProducts',
      'parameters': [],
      'parameterizedName': 'ProductService@@listAllProducts',
      'name': 'ProductService@@listAllProducts',
      'namespace': '',
      'longDisplayName': 'ProductService@@listAllProducts',
      'shortDisplayName': 'ProductService@@listAllProducts'
    }
  }, {
    'qualifiedName': {
      'fullyQualifiedName': 'ProductService@@findProduct',
      'parameters': [],
      'parameterizedName': 'ProductService@@findProduct',
      'name': 'ProductService@@findProduct',
      'namespace': '',
      'longDisplayName': 'ProductService@@findProduct',
      'shortDisplayName': 'ProductService@@findProduct'
    },
    'parameters': [{
      'type': {
        'fullyQualifiedName': 'ProductSku',
        'parameters': [],
        'parameterizedName': 'ProductSku',
        'name': 'ProductSku',
        'namespace': '',
        'longDisplayName': 'ProductSku',
        'shortDisplayName': 'ProductSku'
      },
      'name': 'sku',
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'PathVariable',
          'parameters': [],
          'parameterizedName': 'PathVariable',
          'name': 'PathVariable',
          'namespace': '',
          'longDisplayName': 'PathVariable',
          'shortDisplayName': 'PathVariable'
        }, 'params': {'name': 'sku'}
      }],
      'constraints': []
    }],
    'returnType': {
      'fullyQualifiedName': 'Product',
      'parameters': [],
      'parameterizedName': 'Product',
      'name': 'Product',
      'namespace': '',
      'longDisplayName': 'Product',
      'shortDisplayName': 'Product'
    },
    'operationType': null,
    'metadata': [{
      'name': {
        'fullyQualifiedName': 'HttpOperation',
        'parameters': [],
        'parameterizedName': 'HttpOperation',
        'name': 'HttpOperation',
        'namespace': '',
        'longDisplayName': 'HttpOperation',
        'shortDisplayName': 'HttpOperation'
      }, 'params': {'method': 'GET', 'url': 'http://localhost:9650/products/{sku}'}
    }],
    'contract': {
      'returnType': {
        'fullyQualifiedName': 'Product',
        'parameters': [],
        'parameterizedName': 'Product',
        'name': 'Product',
        'namespace': '',
        'longDisplayName': 'Product',
        'shortDisplayName': 'Product'
      }, 'constraints': []
    },
    'typeDoc': null,
    'name': 'findProduct',
    'memberQualifiedName': {
      'fullyQualifiedName': 'ProductService@@findProduct',
      'parameters': [],
      'parameterizedName': 'ProductService@@findProduct',
      'name': 'ProductService@@findProduct',
      'namespace': '',
      'longDisplayName': 'ProductService@@findProduct',
      'shortDisplayName': 'ProductService@@findProduct'
    }
  }, {
    'qualifiedName': {
      'fullyQualifiedName': 'OrderService@@listOrders',
      'parameters': [],
      'parameterizedName': 'OrderService@@listOrders',
      'name': 'OrderService@@listOrders',
      'namespace': '',
      'longDisplayName': 'OrderService@@listOrders',
      'shortDisplayName': 'OrderService@@listOrders'
    },
    'parameters': [{
      'type': {
        'fullyQualifiedName': 'lang.taxi.Instant',
        'parameters': [],
        'parameterizedName': 'lang.taxi.Instant',
        'name': 'Instant',
        'namespace': 'lang.taxi',
        'longDisplayName': 'lang.taxi.Instant',
        'shortDisplayName': 'Instant'
      },
      'name': 'since',
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'PathVariable',
          'parameters': [],
          'parameterizedName': 'PathVariable',
          'name': 'PathVariable',
          'namespace': '',
          'longDisplayName': 'PathVariable',
          'shortDisplayName': 'PathVariable'
        }, 'params': {'name': 'since'}
      }],
      'constraints': []
    }],
    'returnType': {
      'fullyQualifiedName': 'lang.taxi.Array',
      'parameters': [{
        'fullyQualifiedName': 'OrderTransaction',
        'parameters': [],
        'parameterizedName': 'OrderTransaction',
        'name': 'OrderTransaction',
        'namespace': '',
        'longDisplayName': 'OrderTransaction',
        'shortDisplayName': 'OrderTransaction'
      }],
      'parameterizedName': 'lang.taxi.Array<OrderTransaction>',
      'name': 'Array',
      'namespace': 'lang.taxi',
      'longDisplayName': 'OrderTransaction[]',
      'shortDisplayName': 'OrderTransaction[]'
    },
    'operationType': null,
    'metadata': [{
      'name': {
        'fullyQualifiedName': 'HttpOperation',
        'parameters': [],
        'parameterizedName': 'HttpOperation',
        'name': 'HttpOperation',
        'namespace': '',
        'longDisplayName': 'HttpOperation',
        'shortDisplayName': 'HttpOperation'
      }, 'params': {'method': 'GET', 'url': 'http://localhost:9650/orders/since/{since}'}
    }],
    'contract': {
      'returnType': {
        'fullyQualifiedName': 'lang.taxi.Array',
        'parameters': [{
          'fullyQualifiedName': 'OrderTransaction',
          'parameters': [],
          'parameterizedName': 'OrderTransaction',
          'name': 'OrderTransaction',
          'namespace': '',
          'longDisplayName': 'OrderTransaction',
          'shortDisplayName': 'OrderTransaction'
        }],
        'parameterizedName': 'lang.taxi.Array<OrderTransaction>',
        'name': 'Array',
        'namespace': 'lang.taxi',
        'longDisplayName': 'OrderTransaction[]',
        'shortDisplayName': 'OrderTransaction[]'
      },
      'constraints': [{
        'propertyIdentifier': {
          'type': {
            'namespace': '',
            'typeName': 'TransactionTime',
            'parameters': [],
            'parameterizedName': 'TransactionTime',
            'fullyQualifiedName': 'TransactionTime',
            'firstTypeParameterOrSelf': 'TransactionTime'
          }, 'description': 'type TransactionTime', 'taxi': 'TransactionTime'
        },
        'operator': 'GREATER_THAN',
        'expectedValue': {'path': {'parts': ['since'], 'path': 'since'}, 'taxi': 'since'},
        'compilationUnits': []
      }]
    },
    'typeDoc': null,
    'name': 'listOrders',
    'memberQualifiedName': {
      'fullyQualifiedName': 'OrderService@@listOrders',
      'parameters': [],
      'parameterizedName': 'OrderService@@listOrders',
      'name': 'OrderService@@listOrders',
      'namespace': '',
      'longDisplayName': 'OrderService@@listOrders',
      'shortDisplayName': 'OrderService@@listOrders'
    }
  }, {
    'qualifiedName': {
      'fullyQualifiedName': 'StockService@@submitOrders',
      'parameters': [],
      'parameterizedName': 'StockService@@submitOrders',
      'name': 'StockService@@submitOrders',
      'namespace': '',
      'longDisplayName': 'StockService@@submitOrders',
      'shortDisplayName': 'StockService@@submitOrders'
    },
    'parameters': [{
      'type': {
        'fullyQualifiedName': 'lang.taxi.Array',
        'parameters': [{
          'fullyQualifiedName': 'StockServiceOrderEvent',
          'parameters': [],
          'parameterizedName': 'StockServiceOrderEvent',
          'name': 'StockServiceOrderEvent',
          'namespace': '',
          'longDisplayName': 'StockServiceOrderEvent',
          'shortDisplayName': 'StockServiceOrderEvent'
        }],
        'parameterizedName': 'lang.taxi.Array<StockServiceOrderEvent>',
        'name': 'Array',
        'namespace': 'lang.taxi',
        'longDisplayName': 'StockServiceOrderEvent[]',
        'shortDisplayName': 'StockServiceOrderEvent[]'
      },
      'name': 'orders',
      'metadata': [{
        'name': {
          'fullyQualifiedName': 'RequestBody',
          'parameters': [],
          'parameterizedName': 'RequestBody',
          'name': 'RequestBody',
          'namespace': '',
          'longDisplayName': 'RequestBody',
          'shortDisplayName': 'RequestBody'
        }, 'params': {}
      }],
      'constraints': []
    }],
    'returnType': {
      'fullyQualifiedName': 'lang.taxi.Void',
      'parameters': [],
      'parameterizedName': 'lang.taxi.Void',
      'name': 'Void',
      'namespace': 'lang.taxi',
      'longDisplayName': 'lang.taxi.Void',
      'shortDisplayName': 'Void'
    },
    'operationType': null,
    'metadata': [{
      'name': {
        'fullyQualifiedName': 'HttpOperation',
        'parameters': [],
        'parameterizedName': 'HttpOperation',
        'name': 'HttpOperation',
        'namespace': '',
        'longDisplayName': 'HttpOperation',
        'shortDisplayName': 'HttpOperation'
      }, 'params': {'method': 'POST', 'url': 'http://localhost:9650/stock/orders'}
    }],
    'contract': {
      'returnType': {
        'fullyQualifiedName': 'lang.taxi.Void',
        'parameters': [],
        'parameterizedName': 'lang.taxi.Void',
        'name': 'Void',
        'namespace': 'lang.taxi',
        'longDisplayName': 'lang.taxi.Void',
        'shortDisplayName': 'Void'
      }, 'constraints': []
    },
    'typeDoc': null,
    'name': 'submitOrders',
    'memberQualifiedName': {
      'fullyQualifiedName': 'StockService@@submitOrders',
      'parameters': [],
      'parameterizedName': 'StockService@@submitOrders',
      'name': 'StockService@@submitOrders',
      'namespace': '',
      'longDisplayName': 'StockService@@submitOrders',
      'shortDisplayName': 'StockService@@submitOrders'
    }
  }]
}
