import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { isNullOrUndefined } from 'src/app/utils/utils';


// Start with a letter, then any collection of letters or digits.
const pattern = new RegExp('^[a-zA-Z][a-zA-Z0-9._-]*$');

@Directive({
  selector: '[validIdentifier]',
  standalone: true,
  providers: [{ provide: NG_VALIDATORS, useExisting: ValidIdentifierDirective, multi: true }]
})
export class ValidIdentifierDirective implements Validator {


  validate(control: AbstractControl): ValidationErrors | null {
    const value = control.value;
    if (isNullOrUndefined(value)) {
      return {
        required: value
      }
    }
    const testResult = pattern.test(control.value);
    if (!testResult) {
      return {
        invalidSemver: value
      }
    }
  }

}
