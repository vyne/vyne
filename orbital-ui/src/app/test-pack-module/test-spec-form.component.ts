import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { UiCustomisations } from "../../environments/ui-customisations";

@Component({
  selector: 'app-test-spec-form',
  template: `
    <h2>
      Download a test spec
    </h2>
    <p>
      This lets you download the output of your parsed content as a test case than can by run automatically using {{ UiCustomisations.productName }}'s
      testing tools.
    </p>
    <mat-form-field appearance="outline">
      <mat-label>Test case name</mat-label>
      <input matInput placeholder="Placeholder" [(ngModel)]="testSpecName">
      <mat-hint>Giving the test case a meaningful name helps explain what the test is asserting</mat-hint>
    </mat-form-field>
    <div class="button-row">
      <button mat-stroked-button (click)="onCancelClicked()">Cancel</button>
      <div class="spacer"></div>
      <button mat-raised-button color="primary" [disabled]="!hasName" (click)="onDownloadClicked()">Download</button>
    </div>
  `,
  styleUrls: ['./test-spec-form.component.scss']
})
export class TestSpecFormComponent {
  protected readonly UiCustomisations = UiCustomisations;

  constructor(public dialogRef: MatDialogRef<TestSpecFormComponent>) {
  }

  testSpecName: string;

  get hasName(): boolean {
    return this.testSpecName && this.testSpecName.length > 0;
  }

  onDownloadClicked() {
    this.dialogRef.close(this.testSpecName);
  }

  onCancelClicked() {
    this.dialogRef.close(null);
  }
}
