package com.orbitalhq.history

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class AnalyticsConfig {
}


