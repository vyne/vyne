package com.orbitalhq.history.chronicle.domain;

public interface WrappedValue<T> {
   T value();
}
