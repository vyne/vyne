package com.orbitalhq.connectors.jdbc.drivers.postgres

import com.orbitalhq.connectors.config.jdbc.DatabaseDriverName
import com.orbitalhq.connectors.config.jdbc.JdbcMetadataParams
import com.orbitalhq.connectors.config.jdbc.JdbcUrlBuilder
import com.orbitalhq.connectors.jdbc.UpsertVerb
import com.orbitalhq.connectors.jdbc.drivers.DatabaseSupport
import com.orbitalhq.connectors.jdbc.sql.dml.SqlOperation
import com.orbitalhq.connectors.jdbc.sql.dml.SqlQuery
import com.orbitalhq.schemas.AttributeName
import mu.KotlinLogging
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.RowN
import org.jooq.impl.DSL

object PostgresDbSupport : DatabaseSupport {
   override val driverName: DatabaseDriverName = "POSTGRES"
   private val logger = KotlinLogging.logger {}
   override val jdbcDriverMetadata: JdbcMetadataParams = JdbcMetadataParams()
   override fun jdbcUrlBuilder(): JdbcUrlBuilder = PostgresJdbcUrlBuilder()

   override fun buildUpsertStatement(
      sql: DSLContext,
      actualTableName: String,
      sqlFields: List<Field<out Any>>,
      rows: List<RowN>,
      valuesAsMaps: List<Map<AttributeName, Any?>>,
      verb: UpsertVerb,
      primaryKeyFields: List<Field<out Any>>,
      generatedFields: List<Field<out Any>>
   ): SqlOperation {
      val statement = sql.insertInto(DSL.table(DSL.name(actualTableName)), *sqlFields.toTypedArray())
         .valuesOfRows(*rows.toTypedArray())
         .let { insert ->
            if (primaryKeyFields.isNotEmpty() && verb != UpsertVerb.Insert) {
               insert.onConflict(primaryKeyFields)
                  .doUpdate().setAllToExcluded()
                  .returningResult(generatedFields)
            } else {
               insert.returningResult(generatedFields)
            }
         }
      return SqlQuery(statement, returnsValues = generatedFields.isNotEmpty())
   }

}
