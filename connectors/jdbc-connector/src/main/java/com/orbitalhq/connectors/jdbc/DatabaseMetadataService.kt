package com.orbitalhq.connectors.jdbc

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.orbitalhq.connectors.ConnectionSucceeded
import com.orbitalhq.connectors.config.jdbc.JdbcConnectionConfiguration
import com.orbitalhq.connectors.jdbc.drivers.databaseSupport
import com.orbitalhq.connectors.jdbc.schema.JdbcTaxiSchemaGenerator
import com.orbitalhq.schemas.QualifiedName
import com.orbitalhq.schemas.QualifiedNameAsStringDeserializer
import com.orbitalhq.schemas.Schema
import com.orbitalhq.utils.orElse
import lang.taxi.generators.GeneratedTaxiCode
import mu.KotlinLogging
import org.springframework.core.NestedRuntimeException
import org.springframework.jdbc.core.JdbcTemplate
import schemacrawler.inclusionrule.RegularExpressionInclusionRule
import schemacrawler.schema.Catalog
import schemacrawler.schemacrawler.LimitOptionsBuilder
import schemacrawler.schemacrawler.LoadOptionsBuilder
import schemacrawler.schemacrawler.SchemaCrawlerOptionsBuilder
import schemacrawler.schemacrawler.SchemaInfoLevelBuilder
import schemacrawler.tools.utility.SchemaCrawlerUtility
import java.net.UnknownHostException
import java.sql.Connection
import java.util.*


/**
 * Class which fetches metadata (tables, columns, datatypes)
 * from a database.
 *
 * Primarily used in UI tooling to help users build connections
 */
class DatabaseMetadataService(
   val template: JdbcTemplate,
   val connectionConfig: JdbcConnectionConfiguration
) {
   private val logger = KotlinLogging.logger {}
   fun testConnection(query: String): Either<String, ConnectionSucceeded> {
      return try {
         val map = template.queryForMap(query)
         if (map.isNotEmpty()) {
            ConnectionSucceeded.right()
         } else {
            logger.info { "Test query did not return any rows - treating as a failure" }
            "Connection succeeded, but test query returned no rows.  Possibly a bug in the adaptor?".left()
         }
      } catch (e: Exception) {
         getUserFriendlyConnectionError(e).left()
      }
   }

   private fun getUserFriendlyConnectionError(exception: Throwable): String {
      return when (exception) {
         is NestedRuntimeException -> getUserFriendlyConnectionError(exception.mostSpecificCause)
         is UnknownHostException -> "Unknown host: ${exception.message}"
         else -> "Could not connect to the database: ${exception::class.simpleName} : ${exception.message?.orElse("")}"
      }
   }

   fun tableExists(schemaName: String?, tableName: String): Boolean {
      // Don't scan full catalog, as that can take ages
      template.dataSource!!.connection.use { connection ->
         val (dbSchemaName, dbTableName) = connection.toCorrectCasing(schemaName, tableName)
         val catalogPattern: String? = null
         try {
            val rs = connection.metaData.getTables(catalogPattern, dbSchemaName, dbTableName, arrayOf("TABLE"))
            // If this returns true, then the table exists
            return rs.next()
         } catch (e: Exception) {
            return false
         }
      }
   }

   fun listTables(): List<JdbcTable> {
      val catalog = buildCatalog()
      val tables = catalog.tables.map { table ->
         val indexes = table.indexes.map {
            JdbcIndex(it.name,
               it.columns.map { indexColumn ->
                  JdbcColumn(
                     indexColumn.name,
                     indexColumn.type.name,
                     indexColumn.size,
                     indexColumn.decimalDigits,
                     indexColumn.isNullable
                  )
               })
         }
         val constraintColumns = table.primaryKey?.constrainedColumns?.map {
            JdbcColumn(
               it.name,
               it.type.name,
               it.size,
               it.decimalDigits,
               it.isNullable
            )
         } ?: emptyList()
         // MySQL returns null for schema.name, but a value for schema.catalogName
         JdbcTable(table.schema.name ?: table.schema.catalogName, table.name, constraintColumns, indexes)
      }
      return tables
   }

   private fun Connection.toCorrectCasing(schemaName: String?, tableName: String): Pair<String?, String> {
      return if (this.metaData.storesUpperCaseIdentifiers()) {
         schemaName?.uppercase(Locale.getDefault()) to tableName.uppercase(Locale.getDefault())
      } else {
         schemaName to tableName
      }
   }

   fun listColumns(schemaName: String, tableName: String): List<JdbcColumn> {
      template.dataSource!!.connection.use { safeConnection ->
         val catalogPattern = null
         val (schemaPattern, tableNamePattern) = safeConnection.toCorrectCasing(schemaName, tableName)
         val columnNamePattern = null
         val resultSet = safeConnection.metaData.getColumns(
            catalogPattern, schemaPattern, tableNamePattern, columnNamePattern
         )
         val columns = mutableListOf<JdbcColumn>()
         while (resultSet.next()) {
            val columnName = resultSet.getString("COLUMN_NAME")
            val dataType = resultSet.getString("TYPE_NAME")
            val columnSize = resultSet.getInt("COLUMN_SIZE")
            val decimalDigits = resultSet.getInt("DECIMAL_DIGITS")
            val nullable = resultSet.getString("IS_NULLABLE").yesNoToBoolean()
            columns.add(
               JdbcColumn(
                  columnName,
                  dataType,
                  columnSize,
                  decimalDigits,
                  nullable
               )
            )
         }
         return columns

      }

   }

   fun generateTaxi(
      tables: List<TableTaxiGenerationRequest>,
      schema: Schema,
      connectionName: String
   ): GeneratedTaxiCode {
      val catalog = buildCatalog()
      return JdbcTaxiSchemaGenerator(catalog).buildSchema(tables, schema, connectionName)
   }

   private fun buildCatalog(): Catalog {
      val options = SchemaCrawlerOptionsBuilder.newSchemaCrawlerOptions()
         .withLoadOptions(
            LoadOptionsBuilder.builder()
               .withSchemaInfoLevel(SchemaInfoLevelBuilder.standard())
               .toOptions()
         ).let {options ->
            val schemaName = connectionConfig.databaseSupport.schemaName(connectionConfig)
            if (schemaName != null) {
               options.withLimitOptions(
                  LimitOptionsBuilder.builder()
                     .includeSchemas(RegularExpressionInclusionRule(schemaName))
                     .toOptions()
               )
            } else options
         }
      return SchemaCrawlerUtility.getCatalog(
         com.orbitalhq.connectors.jdbc.schemacrawler.DataSourceConnectionSource(template.dataSource!!),
         options
      )
   }
}

data class TableTaxiGenerationRequest(
   val table: JdbcTable,
   // Leave null to allow the API to generate a type
   val typeName: NewOrExistingTypeName? = null,
   val defaultNamespace: String? = null
)

data class NewOrExistingTypeName(
   @JsonDeserialize(using = QualifiedNameAsStringDeserializer::class)
   val typeName: QualifiedName,
   val exists: Boolean
)

private fun String.yesNoToBoolean(): Boolean {
   return when (this.lowercase(Locale.getDefault())) {
      "yes" -> true
      "no" -> false
      else -> error("$this is not a valid boolean value, expected yes/no")
   }
}

data class JdbcTable(
   val schemaName: String,
   val tableName: String,
   val constrainedColumns: List<JdbcColumn> = emptyList(),
   val indexes: List<JdbcIndex> = emptyList()
)

data class JdbcColumn(
   val columnName: String, val dataType: String, val columnSize: Int,
   val decimalDigits: Int, val nullable: Boolean
)

data class JdbcIndex(val name: String, val columns: List<JdbcColumn>)
