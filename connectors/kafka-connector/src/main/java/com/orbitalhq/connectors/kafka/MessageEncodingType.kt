package com.orbitalhq.connectors.kafka

import com.orbitalhq.schemas.Type
import com.orbitalhq.schemas.fqn
import lang.taxi.generators.avro.AvroMessageAnnotation
import lang.taxi.generators.protobuf.ProtobufMessageAnnotation

enum class MessageEncodingType {
   BYTE_ARRAY,
   STRING;

   companion object {
      fun forType(messageType: Type): MessageEncodingType {
         val declaredEncoding = declaredEncodingOrNull(messageType)
         if (declaredEncoding != null) {
            return declaredEncoding
         }
         return declaredEncodingOrNull(messageType) ?: STRING
      }

      private fun declaredEncodingOrNull(messageType: Type): MessageEncodingType? {
         return when {
            messageType.hasMetadata(ProtobufMessageAnnotation.NAME.fqn()) -> MessageEncodingType.BYTE_ARRAY
            messageType.hasMetadata(AvroMessageAnnotation.NAME.fqn()) -> MessageEncodingType.BYTE_ARRAY
            messageType.hasMetadata(AvroMessageAnnotation.AVRO_MESSAGE_WRAPPER_NAME.fqn()) -> MessageEncodingType.BYTE_ARRAY
            // TODO : Other binary types (eg, avro) go here
            else -> null
         }
      }
   }
}

