package com.orbitalhq.connectors.registry

import com.typesafe.config.ConfigFactory
import io.github.config4k.toConfig
import com.orbitalhq.PackageIdentifier
import com.orbitalhq.ResultWithMessage
import com.orbitalhq.config.UpdatableConfigRepository
import com.orbitalhq.connectors.config.ConnectionsConfig
import com.orbitalhq.connectors.config.SourceLoaderConnectorsRegistry
import reactor.core.publisher.Flux
import kotlin.reflect.KProperty1

interface ConnectionRegistry<T : ConnectorConfiguration> {
   fun hasConnection(name: String): Boolean
   fun getConnection(name: String): T
   fun listAll(): List<T>
}

interface MutableConnectionRegistry<T : ConnectorConfiguration> : ConnectionRegistry<T> {
   fun register(targetPackage: PackageIdentifier, connectionConfiguration: T):ResultWithMessage
   fun remove(targetPackage: PackageIdentifier, connectionConfiguration: T):ResultWithMessage {
      return remove(targetPackage, connectionConfiguration.connectionName)
   }

   fun remove(targetPackage: PackageIdentifier, connectionName: String):ResultWithMessage
}

/**
 * An adaptor between the new ConfigFileConnectorsRegistry / Loader approach,
 * and the legacy XxxConnectionRegistry approach.
 *
 * SourceLoaderConnectorsRegistry supports reloading from sources like Schemas, and
 * is the preferred way for loading config.  However, ConnectionRegistry<> are everywhere.
 *
 * This lets us quickly adapt
 */
abstract class SourceLoaderConnectionRegistryAdapter<T : ConnectorConfiguration>(
   private val sourceLoaderConnectorsRegistry: SourceLoaderConnectorsRegistry,
   private val property: KProperty1<ConnectionsConfig, Map<String, T>>
) : MutableConnectionRegistry<T>, UpdatableConfigRepository<Map<String,T>> {
   private fun getCurrent(): Map<String, T> {
      return property.invoke(sourceLoaderConnectorsRegistry.load())
   }

   override val configUpdated: Flux<Map<String,T>> = sourceLoaderConnectorsRegistry.configUpdated
      .map { getCurrent() }

   override fun getConnection(name: String): T {
      return getCurrent().get(name) ?: error("No connection named $name is defined")
   }

   override fun hasConnection(name: String): Boolean {
      return getCurrent().containsKey(name)
   }

   override fun listAll(): List<T> {
      return getCurrent().values.toList()
   }

   /**
    * Registers the config into the target map.
    * Will also invoke a writer, so the config is updated, and
    * the local cache in invalidated
    */
   override fun register(targetPackage: PackageIdentifier, connectionConfiguration: T):ResultWithMessage {
      // The full config, including all different types of connectors
      // but with env variables unresolved
      val currentConnectionsConfig = sourceLoaderConnectorsRegistry.loadUnresolvedConfig(targetPackage)

      val connectionAsHocon =
         connectionConfiguration.toConfig(name = configPath(connectionConfiguration.connectionName))
      val updated = ConfigFactory.empty()
         .withFallback(connectionAsHocon)
         .withFallback(currentConnectionsConfig)

      return sourceLoaderConnectorsRegistry.saveConfig(targetPackage, updated)
   }

   private fun configPath(connectionName: String) = "${property.name}.$connectionName"

   override fun remove(targetPackage: PackageIdentifier, connectionName: String):ResultWithMessage {
      val currentConnectionsConfig = sourceLoaderConnectorsRegistry.loadUnresolvedConfig(targetPackage)
      val updated = currentConnectionsConfig.withoutPath(configPath(connectionName))

      return sourceLoaderConnectorsRegistry.saveConfig(targetPackage, updated)

   }
}
