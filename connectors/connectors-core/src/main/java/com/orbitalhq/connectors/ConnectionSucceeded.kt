package com.orbitalhq.connectors

/**
 * Marker object for returning when a test of a connector has succeeded
 */
object ConnectionSucceeded
