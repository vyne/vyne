package com.orbitalhq.connectors.config

import com.google.common.io.Resources
import io.kotest.matchers.maps.shouldHaveKeys
import io.kotest.matchers.shouldBe
import com.orbitalhq.config.FileConfigSourceLoader
import com.orbitalhq.connectors.config.jdbc.DefaultJdbcConnectionConfiguration
import com.orbitalhq.connectors.config.kafka.KafkaConnectionConfiguration
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.cbor.Cbor
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import org.junit.jupiter.api.Test
import java.nio.file.Paths

class SourceLoaderConnectorsRegistryTest {

   @OptIn(ExperimentalSerializationApi::class)
   @Test
   fun `can serde to cbor`() {
      val path = Paths.get(
         Resources.getResource("mixed-connections.conf")
            .toURI()
      )
       val config = SourceLoaderConnectorsRegistry.forPath(path).load()
      val bytes = Cbor.encodeToByteArray(config)
      val fromBytes = Cbor.decodeFromByteArray<ConnectionsConfig>(bytes)
      fromBytes.shouldBe(config)
   }

   @Test
   fun `will merge multiple configs`() {
      val path1 = Paths.get(
         Resources.getResource("mixed-connections.conf")
            .toURI()
      )
      val path2 = Paths.get(
         Resources.getResource("connections-2.conf")
            .toURI()
      )
       val config = SourceLoaderConnectorsRegistry(
         listOf(
            FileConfigSourceLoader(path1, packageIdentifier = FileConfigSourceLoader.LOCAL_PACKAGE_IDENTIFIER),
            FileConfigSourceLoader(path2, packageIdentifier = FileConfigSourceLoader.LOCAL_PACKAGE_IDENTIFIER),
         ),
          writerProviders = emptyList()
      ).load()
      config.jdbc.shouldHaveKeys("another-connection", "connection-2", "connection-3", "connection-4")
      config.kafka.shouldHaveKeys("kafka-connection", "kafka-connection-2")
   }

   @Test
   fun `can read from disk`() {
      val path = Paths.get(
         Resources.getResource("mixed-connections.conf")
            .toURI()
      )
       val config = SourceLoaderConnectorsRegistry.forPath(path).load()
      config.shouldBe(
         ConnectionsConfig(
            jdbc = mapOf(
               "another-connection" to DefaultJdbcConnectionConfiguration(
                  connectionName = "another-connection",
                  connectionParameters = mapOf(
                     "database" to "transactions",
                     "host" to "our-db-server",
                     "password" to "super-secret",
                     "port" to "2003",
                     "username" to "jack"
                  ),
                  jdbcDriver = "POSTGRES",
               ),
               "connection-2" to DefaultJdbcConnectionConfiguration(
                  connectionName = "connection-2",
                  connectionParameters = mapOf(
                     "database" to "transactions",
                     "host" to "our-db-server",
                     "password" to "super-secret",
                     "port" to "2003",
                     "username" to "jack"
                  ),
                  jdbcDriver = "POSTGRES",
               )
            ),
            kafka = mapOf(
               "kafka-connection" to KafkaConnectionConfiguration(
                  connectionName = "kafka-connection",
                  connectionParameters = mapOf(
                     "brokers" to "localhost:29092,localhost:39092",
                     "offset" to "earliest",
                     "topic" to "oldmovies"
                  )
               )
            )
         )

      )
   }

}
