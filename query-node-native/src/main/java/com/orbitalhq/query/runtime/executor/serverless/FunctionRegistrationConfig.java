package com.orbitalhq.query.runtime.executor.serverless;

import com.orbitalhq.query.runtime.CompressedQueryResultWrapper;
import com.orbitalhq.query.runtime.QueryMessageCborWrapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

import java.util.function.Function;

/**
 * Exposes serverless function mappings.  Used when running the node
 * as a lambda executor.
 * <p>
 * Spring isn't detecting these functions when defined as Kotlin functions.
 * Hence this class is written in Java.
 */
@Configuration
// Can't use ConditionalOnProperty when compiling to a Native image.
// It's fairly harmless exposing these functions even if nothing is invoking them.
//@ConditionalOnProperty(value = "vyne.consumer.serverless.enabled", havingValue = "true", matchIfMissing = false)
public class FunctionRegistrationConfig {

   @Bean
   public Function<QueryMessageCborWrapper, Mono<CompressedQueryResultWrapper>> queryFunction(
      ServerlessQueryExecutor queryExecutor
   ) {
      return queryExecutor::executeQueryReactive;
   }
}

