package com.orbitalhq.pipelines.jet.pipelines

import com.orbitalhq.PackageIdentifier
import com.orbitalhq.UriSafePackageIdentifier
import com.orbitalhq.pipelines.jet.api.*
import com.orbitalhq.pipelines.jet.api.streams.StreamStatus
import com.orbitalhq.pipelines.jet.api.streams.StreamStatusUpdateRequest
import com.orbitalhq.pipelines.jet.api.transport.PipelineSpec
import com.orbitalhq.pipelines.jet.streams.StreamStateManager
import com.orbitalhq.schema.consumer.SchemaStore
import com.orbitalhq.schemas.fqn
import com.orbitalhq.schemas.taxi.TaxiSchema
import com.orbitalhq.security.VynePrivileges
import com.orbitalhq.spring.http.NotFoundException
import jakarta.annotation.PostConstruct
import mu.KotlinLogging
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
class PipelineService(
   private val pipelineManager: PipelineManager,
   private val pipelineRepository: PipelineConfigRepository,
   private val schemaStore: SchemaStore,
   private val stateManager: StreamStateManager
) : PipelineApi {

   private val logger = KotlinLogging.logger {}

   init {
      pipelineRepository.configUpdated.subscribe {
         logger.info { "Pipeline sources have changed, resubmitting pipelines" }
         loadAndSubmitPipelines()
      }

      logger.info { "Triggering load of pipelines on startup" }
      loadAndSubmitPipelines()

   }

   private fun loadAndSubmitPipelines() {
      val pipelines = pipelineRepository.loadPipelines()
      val schema = schemaStore.schema().asTaxiSchema()
      checkReceivedTypesForPipelinesAndStartAppropriateOnes(
         pipelines,
         schema
      )
   }


   private fun checkReceivedTypesForPipelinesAndStartAppropriateOnes(
      pipelinesToBeSubmitted: List<PipelineSpec<*, *>>,
      schema: TaxiSchema
   ): List<PipelineSpec<*, *>> {
      return pipelinesToBeSubmitted.filter { pipelineSpec ->
         val typesMissingForInput = pipelineSpec.input.requiredSchemaTypes.filter { !schema.hasType(it) }
         val typesMissingForOutputs =
            pipelineSpec.outputs.flatMap { output -> output.requiredSchemaTypes.filter { !schema.hasType(it) } }
         val typesMissing = typesMissingForInput + typesMissingForOutputs
         if (typesMissing.isNotEmpty()) {
            logger.error(
               "The following types are missing for the pipeline \"${pipelineSpec.name}\": ${
                  typesMissing.joinToString(
                     ", "
                  )
               }. Pipeline will be started as soon as they become available in the schema."
            )
            return@filter true
         }
         try {
            logger.info("Pipeline \"${pipelineSpec.name}\" has all the required types available.")
            pipelineManager.startPipeline(pipelineSpec)
            false
         } catch (e: Exception) {
            logger.error(e) { "Loaded pipeline \"${pipelineSpec.name}\" failed to start. Retrying on the next schema update event." }
            true
         }
      }
   }

   @PreAuthorize("hasAuthority('${VynePrivileges.EditPipelines}')")
   @PostMapping("/api/pipelines/scheduled")
   fun triggerScheduledPipeline(@RequestBody triggerScheduledPipelineRequest: TriggerScheduledPipelineRequest): Mono<TriggerScheduledPipelineResponse> {
      logger.info { "Received request to trigger a schedule pipeline manually => $triggerScheduledPipelineRequest" }
      return Mono.just(
         TriggerScheduledPipelineResponse(
            pipelineManager.triggerScheduledPipeline(
               triggerScheduledPipelineRequest.pipelineSpecId
            )
         )
      )
   }

   @PreAuthorize("hasAuthority('${VynePrivileges.EditPipelines}')")
   @PostMapping("/api/pipelines/{packageIdentifier}")
   override fun submitPipeline(
      @PathVariable("packageIdentifier") packageUri: UriSafePackageIdentifier,
      @RequestBody pipelineSpec: PipelineSpec<*, *>
   ): Mono<SubmittedPipeline> {
      val packageIdentifier = PackageIdentifier.fromUriSafeId(packageUri)
      logger.info { "Received new pipelineSpec: \n${pipelineSpec}" }
      pipelineRepository.save(packageIdentifier, pipelineSpec)
      val (submittedPipeline) = pipelineManager.startPipeline(pipelineSpec)

      return Mono.just(submittedPipeline)
   }

   @PreAuthorize("hasAuthority('${VynePrivileges.ViewPipelines}')")
   @GetMapping("/api/pipelines")
   override fun getPipelines(): Mono<List<RunningPipelineSummary>> {
      return Mono.just(pipelineManager.getPipelines())
   }

   @PreAuthorize("hasAuthority('${VynePrivileges.ViewPipelines}')")
   @GetMapping("/api/pipelines/{pipelineSpecId}")
   override fun getPipeline(@PathVariable("pipelineSpecId") pipelineSpecId: String): Mono<RunningPipelineSummary> {
      return Mono.just(pipelineManager.getPipeline(pipelineSpecId))
   }


   @PreAuthorize("hasAuthority('${VynePrivileges.EditPipelines}')")
   @DeleteMapping("/api/pipelines/{pipelineId}")
   override fun deletePipeline(@PathVariable("pipelineId") pipelineSpecId: String): Mono<PipelineStatus> {
      val status = pipelineManager.terminatePipeline(pipelineSpecId)
      TODO("Deleting pipelines not supported whilst we migrate to using schema loaders")
//      if (status.status != JobStatus.RUNNING && status.status != JobStatus.SCHEDULED) {
//         val pipeline = pipelineManager.getPipeline(pipelineSpecId)
//         pipelineRepository.deletePipeline(pipeline.pipeline!!.spec)
//      }
//      return Mono.just(status)
   }


   @PreAuthorize("hasAuthority('${VynePrivileges.EditPipelines}')")
   @PostMapping("/api/streams/{streamName}/status")
   override fun updateStreamStatus(
      @PathVariable("streamName") streamName: String,
      @RequestBody request: StreamStatusUpdateRequest
   ): Mono<StreamStatus> {
      return Mono.fromCallable {
         stateManager.setStreamState(streamName, request.state)
      }
   }

   @PreAuthorize("hasAuthority('${VynePrivileges.EditPipelines}')")
   @PostMapping("/api/streams/{streamName}/restart")
   fun restartFailedStream(
      @PathVariable("streamName") streamName: String,
   ): Mono<StreamStatus> {
      return Mono.fromCallable {
         pipelineManager.startPipelineByName(streamName.fqn())
         stateManager.getStreamStatusIfExists(streamName)
      }
   }

   @PreAuthorize("hasAuthority('${VynePrivileges.ViewPipelines}')")
   @GetMapping("/api/streams/{streamName}/status")
   override fun getStreamStatus(
      @PathVariable("streamName") streamName: String,
   ): Mono<StreamStatus> {
      val status = stateManager.getStreamStatusIfExists(streamName)
         ?: throw NotFoundException("No stream named $streamName was found")
      return Mono.just(status)
   }

}

data class TriggerScheduledPipelineRequest(val pipelineSpecId: String)
data class TriggerScheduledPipelineResponse(val success: Boolean)
