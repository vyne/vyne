package com.orbitalhq.pipelines.jet

/**
 * This class does nothing, but is used by spring config to
 * indicate to scan all packages under here.
 */
object StreamEngineConfigMarker
