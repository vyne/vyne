package com.orbitalhq.pipelines.jet.streams

import com.google.common.annotations.VisibleForTesting
import com.google.common.collect.Maps
import com.orbitalhq.annotations.streaming.StreamingQueryAnnotations
import com.orbitalhq.pipelines.jet.api.RunningPipelineSummary
import com.orbitalhq.pipelines.jet.api.transport.query.StreamingQueryInputSpec
import com.orbitalhq.pipelines.jet.pipelines.PipelineManager
import com.orbitalhq.schema.consumer.SchemaChangedEventProvider
import com.orbitalhq.schemas.QualifiedName
import com.orbitalhq.schemas.Schema
import com.orbitalhq.schemas.fqn
import com.orbitalhq.schemas.toVyneQualifiedName
import lang.taxi.query.QueryMode
import lang.taxi.query.TaxiQLQueryString
import lang.taxi.query.TaxiQlQuery
import lang.taxi.types.annotation
import mu.KotlinLogging
import org.springframework.stereotype.Component
import reactor.kotlin.core.publisher.toFlux
import java.util.concurrent.ConcurrentHashMap

/**
 * Watches the schema, and creates / destroys persistent streams
 * based on those saved in the schema
 */
@Component
class PersistentStreamManager(
   private val schemaStore: SchemaChangedEventProvider,
   private val pipelineManager: PipelineManager,
   @VisibleForTesting
   val streamStateManager: StreamStateManager
) {
   private val managedStreams = ConcurrentHashMap<QualifiedName, ManagedStream>()

   companion object {
      private val logger = KotlinLogging.logger {}
   }

   init {
      logger.info { "Stream manager is active, monitoring schema for persistent streams" }
      schemaStore.schemaChanged
         .toFlux()
         .subscribe { schemaSetChangedEvent ->
            val newContentHash = schemaSetChangedEvent.newSchemaSet.allSourcesContentHashes
            val oldContentHash = schemaSetChangedEvent.oldSchemaSet?.allSourcesContentHashes
            /**
             * Consider moving this check up to the point where we fire SchemaSetChangedEvent.
             */
            if (newContentHash != oldContentHash) {
               if (this.pipelineManager.canStartPipelines()) {
                  logger.info { "Schema has updated - updating streams" }
                  updateStreams(schemaSetChangedEvent.newSchemaSet.schema)
               } else {
                  logger.info { "Schema has updated - however this node is not the leader - no action taken" }
               }
            } else {
               logger.warn { "Schema is updated but source content hash are the same, skipping stream updates!" }
            }
         }
   }

   fun listCurrentStreams(): List<ManagedStream> {
      return managedStreams.values.toList()
   }

   private fun updateStreams(schema: Schema) {
      val updatedStreamState = getStreams(schema)
      val updatedStreamQueries: Map<QualifiedName, TaxiQLQueryString> = updatedStreamState.mapValues { (_,taxiQlQuery) -> taxiQlQuery.source }

      val currentManagedStreams = pipelineManager.getManagedStreams(includeCancelled = false)
      val currentStreamQueries: Map<QualifiedName, TaxiQLQueryString> = currentManagedStreams
         .filter { it.pipeline != null }.associate { runningPipeline ->
            val pipelineSpec = runningPipeline.pipeline!!.spec
            val querySpec = pipelineSpec.input as StreamingQueryInputSpec
            pipelineSpec.name.fqn() to querySpec.query
         }


      val difference = Maps.difference(updatedStreamQueries, currentStreamQueries)

      val addedStreams = difference.entriesOnlyOnLeft()
      handleStreamsAdded(schema.hash,  addedStreams.mapValues { entry -> updatedStreamState[entry.key]!! })

      val removedStreams = difference.entriesOnlyOnRight()
      handleStreamsRemoved(removedStreams.keys, currentManagedStreams, removeCurrentState = true)

      val updatedStreams = difference.entriesDiffering()
      handleStreamsUpdated(schema.hash, updatedStreams.mapValues { entry -> updatedStreamState[entry.key]!! }, currentManagedStreams)

   }

   private fun handleStreamsUpdated(
      schemaHash: Int,
      updatedStreams: Map<QualifiedName, TaxiQlQuery>,
      currentManagedStreams: List<RunningPipelineSummary>
   ) {
      if (updatedStreams.isEmpty()) return
      logger.info { "${updatedStreams.size} streams modified, will stop and restart them" }
      handleStreamsRemoved(updatedStreams.keys, currentManagedStreams, removeCurrentState = false)
      handleStreamsAdded(schemaHash, updatedStreams)
   }

   private fun handleStreamsRemoved(
      removedStreams: Set<QualifiedName>,
      currentManagedStreams: List<RunningPipelineSummary>,
      removeCurrentState: Boolean
   ) {
      removedStreams.map { removedStreamName ->
         currentManagedStreams.single {
            runningPipeline -> runningPipeline.pipeline!!.name ==  removedStreamName.parameterizedName }
      }
         .forEach { runningPipelineSummary ->
            logger.info { "Managed stream ${runningPipelineSummary.pipeline!!.name} has been removed from the schema, so terminating associated stream" }
            pipelineManager.terminatePipeline(runningPipelineSummary.pipeline!!.pipelineSpecId)
            if (removeCurrentState) {
               streamStateManager.removeStreamState(runningPipelineSummary.pipeline!!.name)
            }
         }

   }

   private fun handleStreamsAdded(schemaHash: Int, addedStreams: Map<QualifiedName, TaxiQlQuery>) {
      addedStreams
         .forEach {
            logger.info { "Creating a persistent stream for ${it.key}" }
            streamStateManager.submitStream(ManagedStream.from(schemaHash, it.value))
         }
   }

   private fun getStreams(schema: Schema): Map<QualifiedName, TaxiQlQuery> {
      return schema.taxi.queries
         .filter { it.queryMode == QueryMode.STREAM }
         .associateBy { it.name.toVyneQualifiedName() }
   }
}

data class ManagedStream(
   val schemaHash: Int,
   val name: QualifiedName,
   val query: TaxiQlQuery,
   val streamType: StreamType,
   val enabled: Boolean = true,
   val state: StreamState = StreamState.NOT_STARTED
) {
   companion object {
      fun from(schemaHash: Int, query: TaxiQlQuery, enabled: Boolean = true): ManagedStream {
         val streamType: ManagedStream.StreamType = detectStreamType(query)
         return ManagedStream(schemaHash, query.name.toVyneQualifiedName() , query, streamType, enabled)
      }

      // TODO. Placeholder.
      private fun detectStreamType(value: TaxiQlQuery): ManagedStream.StreamType = ManagedStream.StreamType.CONTINUOUS
   }

   /**
    * Returns the distributed count defined by the @Distributed() annotation - if present
    */
   val parallelism:Int?
      get() {
         val annotation = query.annotation(StreamingQueryAnnotations.ParallelAnnotationName)
            ?: return null
         val count = annotation.parameters["count"] as Int
         return count
      }

   // Capturing design thoughts, might not be fully implemented, will
   // start with contious streams first.
   enum class StreamType {
      /**
       * A stream that is always running.
       * These are streams that do not have an @HttpOperation annotation associated with them
       * Executed by our pipeline infrastrcture, which should ensure there's only ever a single instance running.
       */
      CONTINUOUS,

      /**
       * A new stream is created per request, and terminated when the requestor goes away.
       * This is for streams that contain an @HttpOperation on them.
       */
      PER_REQUEST
   }

   enum class StreamState {
      NOT_STARTED,
      SCHEDULED,
      STARTED,
      STOPPING,
      STOPPED
   }
}
