package com.orbitalhq.query.runtime.core.dispatcher.local

import com.orbitalhq.query.ResultMode
import com.orbitalhq.query.runtime.StreamResultStreamProvider
import com.orbitalhq.query.runtime.core.QueryService
import com.orbitalhq.query.runtime.core.dispatcher.StreamingQueryDispatcher
import com.orbitalhq.query.runtime.core.gateway.RoutedQueryResponse
import kotlinx.coroutines.runBlocking
import lang.taxi.types.QualifiedName
import mu.KotlinLogging
import org.springframework.security.core.Authentication
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.security.Principal


/**
 * Configured by the RoutedQueryDispatcherAdaptor if no query dispatcher was provided.
 *
 * A StreamingQueryDispatcher is responsible for taking requests to saved queries
 * with @Http endpoints, and sending it somewhere to be executed (eg., a serverless function somewhere).
 *
 * If one isn't provided, then we use this, to execute the queries locally.
 * Useful for quick-start projects, but not as scalable as query offloading.
 */
class LocalQueryDispatcher(
   private val queryService: QueryService,
   private val streamResultSubscriptionManager: StreamResultStreamProvider,
) : StreamingQueryDispatcher {

   companion object {
      private val logger = KotlinLogging.logger {}
   }

   override fun dispatchQuery(
      query: String,
      clientQueryId: String,
      mediaType: String,
      resultMode: ResultMode,
      arguments: Map<String, Any?>,
      principal: Principal?
   ): RoutedQueryResponse {
      // Note: This isn't actually a suspend function.
      // All the work happens in the returned Flux<> / Flow<>,
      // we just need to fix the underling signatures.
      logger.info { "Received inbound call to saved query with request id $clientQueryId. Will execute locally" }
      val auth: Authentication? = if (principal is Authentication) {
         principal
      } else null
      val responseEntity = runBlocking {
         try {
            queryService.submitVyneQlQuery(
               query,
               resultMode,
               mediaType,
               auth,
               clientQueryId,
               arguments = arguments,
            )
         } catch (e:Exception) {
            throw e
         }

      }
      val responseHeaders = responseEntity.headers.map { it.key to it.value}.toMap()
      val response =  when (responseEntity.body) {
         is Flux<*> -> {
            (responseEntity.body!! as Flux<Any>).doOnError { error ->
               Flux.error<Any>(error)
            }
         }
         is Mono<*> -> (responseEntity.body!! as Mono<Any>) /*.doOnError {
            TODO()
         }*/
         else -> error("Unhandled usecase: ${responseEntity.body::class.simpleName}")
      }

      return RoutedQueryResponse(response, responseHeaders)
   }

   override fun publishResultStream(name: QualifiedName, principal: Principal?): Flux<Any> {
      return streamResultSubscriptionManager.getResultStream(name.parameterizedName,principal)
   }
}
