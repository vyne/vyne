package com.orbitalhq.query.runtime.core

/**
 * Marker object for component scanning on the query node base packages
 */
object QueryNodeConfig
