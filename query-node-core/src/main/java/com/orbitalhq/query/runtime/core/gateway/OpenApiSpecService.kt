package com.orbitalhq.query.runtime.core.gateway

import com.orbitalhq.schema.api.SchemaProvider
import com.orbitalhq.schemas.QualifiedName
import com.orbitalhq.schemas.Schema
import com.orbitalhq.security.VynePrivileges
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.taxilang.openapi.OpenApiGenerator

@RestController
class OpenApiSpecService(
   private val schemaProvider: SchemaProvider,
   private val queryRouteService: QueryRouteService
) {

   @PreAuthorize("hasAuthority('${VynePrivileges.BrowseSchema}')")
   @GetMapping("/api/q/meta/{queryName}/oas", produces = ["text/yaml"])
   suspend fun getApiSpecForSavedQuery(@PathVariable("queryName") queryName: String): String {
      val routableQuery = queryRouteService.routes.firstOrNull { query ->
         query.query.name.fullyQualifiedName == queryName
      } ?: error("No query matched the provided route")

      val taxiDocument = schemaProvider.schema.taxi

      val openApiGenerator = OpenApiGenerator()
      val generatedSpecs = openApiGenerator
         .generateOpenApiSpec(taxiDocument, listOf(routableQuery.query.name.fullyQualifiedName))
      val yaml = openApiGenerator.generateYaml(generatedSpecs)
      return yaml.joinToString("\n") { it.content }
   }

   fun getApiSpecForQuery(schema:Schema, queryName: QualifiedName): String {
      val openApiGenerator = OpenApiGenerator()
      val generatedSpecs = openApiGenerator
         .generateOpenApiSpec(schema.taxi, listOf(queryName.parameterizedName))
      val yaml = openApiGenerator.generateYaml(generatedSpecs)
      return yaml.joinToString("\n") { it.content }
   }
}
