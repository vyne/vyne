package com.orbitalhq.search.embedded

import com.orbitalhq.query.graph.Algorithms
import com.orbitalhq.query.graph.OperationQueryResult
import com.orbitalhq.query.graph.OperationQueryResultItemRole
import com.orbitalhq.schemas.*
import com.orbitalhq.utils.log
import lang.taxi.types.TypeKind
import mu.KotlinLogging
import org.apache.lucene.document.Document
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.Term
import org.apache.lucene.search.BooleanClause
import org.apache.lucene.search.BooleanQuery
import org.apache.lucene.search.BoostQuery
import org.apache.lucene.search.FuzzyQuery
import org.apache.lucene.search.PrefixQuery
import org.apache.lucene.search.SearcherManager
import org.apache.lucene.search.TermQuery
import org.apache.lucene.search.highlight.Highlighter
import org.springframework.stereotype.Component

@Component
class SearchIndexRepository(
   private val indexWriter: IndexWriter,
   private val searchManager: SearcherManager,
   private val configFactory: ConfigFactory
) {

   fun destroyAndInitialize() {
      log().info("Destroying existing search indices")
      indexWriter.deleteAll()
      indexWriter.commit()
   }

   fun writeAll(documents: List<Document>) {
      indexWriter.addDocuments(documents)
      indexWriter.commit()
   }

   private fun createAnnotationSearchQuery(term: String): BooleanQuery {
      val annotationSearchTerm = annotationSearchTerm(term)
      val queryBuilder = BooleanQuery.Builder()
      // Decreasing the score of fuzzy search as it can produce a match for 'time' when we search for 'fixe'
      val field = SearchField.QUALIFIED_NAME
      queryBuilder.add(
         BoostQuery(
            FuzzyQuery(Term(field.fieldName, "${annotationSearchTerm.toLowerCase()}*"), 2),
            field.boostFactor * 0.11F
         ), BooleanClause.Occur.SHOULD
      )
      queryBuilder.add(
         BoostQuery(
            PrefixQuery(Term(field.fieldName, annotationSearchTerm.toLowerCase())),
            field.boostFactor
         ), BooleanClause.Occur.SHOULD
      )
      SearchEntryType.values().forEach { searchEntryType ->
         if (searchEntryType != SearchEntryType.ANNOTATION) {
            queryBuilder.add(
               TermQuery(Term(SearchField.MEMBER_TYPE.fieldName, searchEntryType.name)),
               BooleanClause.Occur.MUST_NOT
            )
         }
      }

      return queryBuilder.build()
   }

   private fun mapSearchResultForAnnotations(
      schema: Schema,
      term: String
   ): List<SearchResult> {
      return Algorithms
         .findAllFunctionsWithArgumentOrReturnValueForAnnotationDetailed(schema, term)
         .toSet()
         .map { (annotationSearchResult, operationQueryResult) ->
            val vyneType = schema.type(operationQueryResult.typeName)
            SearchResult(
               vyneType.qualifiedName,
               vyneType.typeDoc,
               annotationSearchResult.containingTypeAndFieldName(),
               listOf(SearchMatch(SearchField.QUALIFIED_NAME, vyneType.fullyQualifiedName)),
               SearchEntryType.ANNOTATION,
               1.0F,
               consumers = operationQueryResult.results.filter { it.role == OperationQueryResultItemRole.Input && it.operationName != null }
                  .map { it.operationName!! },
               producers = operationQueryResult.results.filter { it.role == OperationQueryResultItemRole.Output && it.operationName != null }
                  .map { it.operationName!! },
               metadata = vyneType.metadata
            )
         }
   }

   private fun findAttributeType(searchResultFullyQualifiedName: QualifiedName, matchedFieldName: String?, schema:Schema):Type? {
      if (matchedFieldName == null) return null
      return schema.typeOrNull(searchResultFullyQualifiedName)?.attribute(matchedFieldName)
         ?.let { field -> schema.type(field.type) }
   }

   fun searchForTypesAndOperations(term: String, schema: Schema): List<SearchResult> {
      searchManager.maybeRefresh()
      val queryBuilder = BooleanQuery.Builder()
      SearchField.values().forEach { field ->
         // Decreasing the score of fuzzy search as it can produce a match for 'time' when we search for 'fixe'
         queryBuilder.add(
            BoostQuery(
               FuzzyQuery(Term(field.fieldName, "${term.toLowerCase()}*"), 2),
               field.boostFactor * 0.11F
            ), BooleanClause.Occur.SHOULD
         )
         queryBuilder.add(
            BoostQuery(PrefixQuery(Term(field.fieldName, term.toLowerCase())), field.boostFactor),
            BooleanClause.Occur.SHOULD
         )
      }
      val query = queryBuilder.build()
      val searcher = searchManager.acquire()

      val result = searcher.search(query, 1000)
      val highlighter = SearchHighlighter.newHighlighter(query)
      val searchResults = result.scoreDocs.mapNotNull scoredDoc@{ hit ->
         val doc = searcher.doc(hit.doc)
         val searchMatches = SearchField.values().mapNotNull mapSearchField@{ searchField ->

            val fieldContents = doc.getField(searchField.fieldName)?.stringValue() ?: return@mapSearchField null
            when (searchField.highlightMethod) {
               SearchField.HighlightMethod.HIGHLIGHTER -> highlightResult(highlighter, searchField, fieldContents)
               SearchField.HighlightMethod.SUBSTRING -> highlightResultWithSubstring(fieldContents, term, searchField)
               SearchField.HighlightMethod.NONE -> null
            }
         }.distinct()
         val searchResultFullyQualifiedName = doc.getField(SearchField.QUALIFIED_NAME.fieldName).stringValue().fqn()
         val searchEntryType = SearchEntryType.fromName(doc.getField(SearchField.MEMBER_TYPE.fieldName)?.stringValue())
         val (metadata: List<Metadata>, operationQueryResult: com.orbitalhq.query.graph.OperationQueryResult) = if (searchEntryType == SearchEntryType.TYPE) {
            if (schema.hasType(searchResultFullyQualifiedName.parameterizedName)) {
               val vyneType = schema.type(searchResultFullyQualifiedName)
               vyneType.metadata to Algorithms.findAllFunctionsWithArgumentOrReturnValueForType(
                  schema,
                  searchResultFullyQualifiedName.fullyQualifiedName
               )
            } else {
               emptyList<Metadata>() to OperationQueryResult.empty(searchResultFullyQualifiedName.fullyQualifiedName)
            }
         } else {
            emptyList<Metadata>() to OperationQueryResult.empty(searchResultFullyQualifiedName.fullyQualifiedName)
         }

         val matchedFieldName =
            if (searchEntryType == SearchEntryType.FIELD) doc.getField(SearchField.NAME.fieldName)
               ?.stringValue() else null

         val (typeKind: TypeKind?, serviceKind: ServiceKind?, operationKind: OperationKind?) = when (searchEntryType) {
            SearchEntryType.TYPE -> Triple(schema.typeOrNull(searchResultFullyQualifiedName)?.taxiType?.typeKind, null, null)
            SearchEntryType.SERVICE -> Triple(null, schema.serviceOrNull(searchResultFullyQualifiedName)?.serviceKind, null)
            SearchEntryType.OPERATION -> Triple(null, null, schema.remoteOperationOrNull(searchResultFullyQualifiedName)?.operationKind)
            SearchEntryType.FIELD -> {
               val typeKind = findAttributeType(searchResultFullyQualifiedName, matchedFieldName, schema)?.taxiType?.typeKind
               Triple(typeKind, null, null)
            }
            else -> Triple(null, null, null)
         }

         val primitiveType = when (searchEntryType) {
            SearchEntryType.TYPE -> schema.typeOrNull(searchResultFullyQualifiedName)?.basePrimitiveTypeName
            SearchEntryType.FIELD -> findAttributeType(searchResultFullyQualifiedName,matchedFieldName, schema)?.basePrimitiveTypeName
            else -> null
         }


         SearchResult(
            qualifiedName = searchResultFullyQualifiedName,
            typeDoc = doc.getField(SearchField.TYPEDOC.fieldName)?.stringValue(),
            matchedFieldName = matchedFieldName,
            matches = searchMatches,
            memberType = searchEntryType,
            score = hit.score,
            consumers = operationQueryResult.results.filter { it.role == OperationQueryResultItemRole.Input && it.operationName != null }
               .map { it.operationName!! },
            producers = operationQueryResult.results.filter { it.role == OperationQueryResultItemRole.Output && it.operationName != null }
               .map { it.operationName!! },
            metadata = metadata,
            typeKind, serviceKind, operationKind,
            primitiveType
         )
      }
      return distinctSearchResults(searchResults)
   }

   fun search(term: String, schema: Schema): List<SearchResult> {
      searchManager.maybeRefresh()
      val isAnnotationSearch = isAnnotationSearch(term)
      val searchResults = if (isAnnotationSearch) {
         searchAnnotations(term, schema)
      } else {
         searchForTypesAndOperations(term, schema)
      }
      return distinctSearchResults(searchResults)
   }

   private fun searchAnnotations(term: String, schema: Schema): List<SearchResult> {
      return mapSearchResultForAnnotations(schema, term)
   }

   private fun distinctSearchResults(searchResults: List<SearchResult>): List<SearchResult> {
      // Merge the searchResults,
      // as a match can appear on multiple fields, which seems to return
      // separate documents.  Odd.
      val distinctSearchResults = searchResults.groupBy { it.qualifiedName }
         .map { (_, results) ->
            results.reduce { acc, searchResult ->
               acc.copy(typeDoc = acc.typeDoc ?: searchResult.typeDoc, matches = acc.matches + searchResult.matches)
            }
         }

      return distinctSearchResults.sortedByDescending { it.score }
   }

   private fun highlightResultWithSubstring(
      fieldContents: String,
      term: String,
      searchField: SearchField
   ): SearchMatch? {
      // We use this approach (substring) over highlighting, as for String indexed fields (vs Text indexed fields)
      // with a prefix match, lucene's highlighter matches the entire result, which isn't what
      // the user wants to see.
      if (!fieldContents.toLowerCase().contains(term.toLowerCase())) {
         return null
      }

      val index = fieldContents.indexOf(term, ignoreCase = true)
      val highlightedMatch = fieldContents.substring(0, index) +
         SearchHighlighter.PREFIX +
         fieldContents.substring(index, index + term.length) +
         SearchHighlighter.SUFFIX +
         fieldContents.substring(index + term.length)
      return SearchMatch(
         searchField,
         highlightedMatch
      )

   }

   private fun highlightResult(
      highlighter: Highlighter,
      searchField: SearchField,
      fieldContents: String
   ): SearchMatch? {
      return try {
         highlighter.getBestFragment(configFactory.config().analyzer, searchField.fieldName, fieldContents)
            ?.let { highlight ->
               SearchMatch(searchField, highlight)
            }
      } catch (e:Exception) {
         logger.warn { "Search highlighting failed with exception: ${e.message}" }
         null
      }
   }

   companion object {
      fun isAnnotationSearch(term: String) = term.startsWith("#") || term.startsWith("@")
      fun annotationSearchTerm(term: String) = term.drop(1)
      private val logger = KotlinLogging.logger {}
   }
}

data class SearchResult(
   val qualifiedName: QualifiedName,
   val typeDoc: String?,
   val matchedFieldName: String?,
   val matches: List<SearchMatch>,
   val memberType: SearchEntryType,
   val score: Float,
   val consumers: List<QualifiedName> = emptyList(),
   val producers: List<QualifiedName> = emptyList(),
   val metadata: List<com.orbitalhq.schemas.Metadata> = emptyList(),
   val typeKind: TypeKind? = null,
   val serviceKind: ServiceKind? = null,
   val operationKind: OperationKind? = null,
   val primitiveType: QualifiedName? = null
)

data class SearchMatch(val field: SearchField, val highlightedMatch: String)

