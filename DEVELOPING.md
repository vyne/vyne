# Developing Orbital

Orbital is currently developed on Gitlab (for historic reasons), and mirrored
to Github.

This makes it difficult for us to accept contributions on Github.
Please reach out on [Slack](https://join.slack.com/t/orbitalapi/shared_invite/zt-697laanr-DHGXXak5slqsY9DqwrkzHg) if you'd like to contribute.

## Naming
Orbital used to be called Vyne, so you'll see that in many places throughout the code.
We're progressively moving this from Vyne to Orbital.

## Commit messages

Commit messages follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification that
allows them to be readable both by humans and machines easily.

The available types and scopes have been listed out in [.commitlintrc.json](.commitlintrc.json) and a plugin
called [Commitlint Conventional Commit](https://plugins.jetbrains.com/plugin/14046-commitlint-conventional-commit) for
IntelliJ IDEA adds a nice UI and checks the usage of the proper types and scopes when committing.
