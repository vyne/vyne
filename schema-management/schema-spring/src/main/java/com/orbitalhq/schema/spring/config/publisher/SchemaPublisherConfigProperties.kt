package com.orbitalhq.schema.spring.config.publisher

import com.orbitalhq.schema.api.VyneSchemaInteractionMethod
import com.orbitalhq.schema.spring.config.SchemaConfigProperties.Companion.SCHEMA_CONFIG
import com.orbitalhq.schema.spring.config.SchemaTransportConfigProperties
import com.orbitalhq.schema.spring.config.publisher.SchemaPublisherConfigProperties.Companion.PUBLISHER_CONFIG
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.NestedConfigurationProperty

//@ConstructorBinding
@ConfigurationProperties(prefix = PUBLISHER_CONFIG)
data class SchemaPublisherConfigProperties(
   override var method: VyneSchemaInteractionMethod = VyneSchemaInteractionMethod.RSocket,
   @NestedConfigurationProperty
   val http: HttpPublisherConfigParams = HttpPublisherConfigParams(),
) : SchemaTransportConfigProperties {
   companion object {
      const val PUBLISHER_CONFIG = "$SCHEMA_CONFIG.publisher"
      const val PUBLISHER_METHOD = "$PUBLISHER_CONFIG.method"
   }
}
