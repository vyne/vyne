package com.orbitalhq.schema.publisher

enum class PublisherType {
   GitRepo,
   FileSystem,
   Pushed
}
