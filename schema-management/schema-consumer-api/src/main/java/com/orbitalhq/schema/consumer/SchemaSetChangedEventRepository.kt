package com.orbitalhq.schema.consumer

import com.orbitalhq.schema.api.SchemaSet
import com.orbitalhq.schemas.SchemaSetChangedEvent
import mu.KotlinLogging
import org.reactivestreams.Publisher
import reactor.core.publisher.Sinks
import java.time.Duration


abstract class SchemaSetChangedEventRepository : SchemaChangedEventProvider, SchemaStore {
   private val schemaSetSink = Sinks.many().replay().latest<SchemaSetChangedEvent>()
   override val schemaChanged: Publisher<SchemaSetChangedEvent> = schemaSetSink.asFlux()

   private val logger = KotlinLogging.logger {}

   private var lastSchemaSet: SchemaSet = SchemaSet.EMPTY

    override val schemaSet: SchemaSet
        get() {
            return lastSchemaSet
        }

   override val generation: Int
      get() {
         return schemaSet.generation
      }

   override fun forceSchemaChangedEvent() {
      schemaSetSink.emitNext(SchemaSetChangedEvent(schemaSet,schemaSet), Sinks.EmitFailureHandler.busyLooping(Duration.ofSeconds(10)))
   }


   /**
    * Generates and emits SchemaSetChangedEvent if the schema is considered different.
    * If the schema is considered different, the internal state is updated.
    *
    * The event (which has already been emitted) is returned for convenience.
    */
   fun emitNewSchemaIfDifferent(newSchemaSet: SchemaSet): SchemaSetChangedEvent? {
      return SchemaSetChangedEvent.generateFor(lastSchemaSet, newSchemaSet)?.let { event ->
         logger.info("SchemaSet has been updated / created: $newSchemaSet - dispatching event.")
         lastSchemaSet = newSchemaSet

         // If there are concurrency issues, just retry.
         // We can't drop this event, as it indicates a changed schema
         schemaSetSink.emitNext(event, Sinks.EmitFailureHandler.busyLooping(Duration.ofSeconds(10)))
         event
      }
   }

}
